/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Simone Sorgon
 * @generated
 */
@ProviderType
public class ContenutoPaginaSoap implements Serializable {

	public static ContenutoPaginaSoap toSoapModel(ContenutoPagina model) {
		ContenutoPaginaSoap soapModel = new ContenutoPaginaSoap();

		soapModel.setContenutoPaginaId(model.getContenutoPaginaId());
		soapModel.setCodicePagina(model.getCodicePagina());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setPlaceholder(model.getPlaceholder());
		soapModel.setDocument_blob(model.getDocument_blob());
		soapModel.setDescrizioneFile(model.getDescrizioneFile());
		soapModel.setTipoFile(model.getTipoFile());
		soapModel.setNomeFile(model.getNomeFile());
		soapModel.setDataAllegato(model.getDataAllegato());
		soapModel.setDataEtichettaPrincipale(
			model.getDataEtichettaPrincipale());
		soapModel.setDataAltreProp(model.getDataAltreProp());
		soapModel.setEtichettaPrincipale(model.getEtichettaPrincipale());

		return soapModel;
	}

	public static ContenutoPaginaSoap[] toSoapModels(ContenutoPagina[] models) {
		ContenutoPaginaSoap[] soapModels =
			new ContenutoPaginaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ContenutoPaginaSoap[][] toSoapModels(
		ContenutoPagina[][] models) {

		ContenutoPaginaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new ContenutoPaginaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ContenutoPaginaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ContenutoPaginaSoap[] toSoapModels(
		List<ContenutoPagina> models) {

		List<ContenutoPaginaSoap> soapModels =
			new ArrayList<ContenutoPaginaSoap>(models.size());

		for (ContenutoPagina model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ContenutoPaginaSoap[soapModels.size()]);
	}

	public ContenutoPaginaSoap() {
	}

	public long getPrimaryKey() {
		return _contenutoPaginaId;
	}

	public void setPrimaryKey(long pk) {
		setContenutoPaginaId(pk);
	}

	public long getContenutoPaginaId() {
		return _contenutoPaginaId;
	}

	public void setContenutoPaginaId(long contenutoPaginaId) {
		_contenutoPaginaId = contenutoPaginaId;
	}

	public long getCodicePagina() {
		return _codicePagina;
	}

	public void setCodicePagina(long codicePagina) {
		_codicePagina = codicePagina;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public String getPlaceholder() {
		return _placeholder;
	}

	public void setPlaceholder(String placeholder) {
		_placeholder = placeholder;
	}

	public String getDocument_blob() {
		return _document_blob;
	}

	public void setDocument_blob(String document_blob) {
		_document_blob = document_blob;
	}

	public String getDescrizioneFile() {
		return _descrizioneFile;
	}

	public void setDescrizioneFile(String descrizioneFile) {
		_descrizioneFile = descrizioneFile;
	}

	public String getTipoFile() {
		return _tipoFile;
	}

	public void setTipoFile(String tipoFile) {
		_tipoFile = tipoFile;
	}

	public String getNomeFile() {
		return _nomeFile;
	}

	public void setNomeFile(String nomeFile) {
		_nomeFile = nomeFile;
	}

	public Date getDataAllegato() {
		return _dataAllegato;
	}

	public void setDataAllegato(Date dataAllegato) {
		_dataAllegato = dataAllegato;
	}

	public Date getDataEtichettaPrincipale() {
		return _dataEtichettaPrincipale;
	}

	public void setDataEtichettaPrincipale(Date dataEtichettaPrincipale) {
		_dataEtichettaPrincipale = dataEtichettaPrincipale;
	}

	public Date getDataAltreProp() {
		return _dataAltreProp;
	}

	public void setDataAltreProp(Date dataAltreProp) {
		_dataAltreProp = dataAltreProp;
	}

	public String getEtichettaPrincipale() {
		return _etichettaPrincipale;
	}

	public void setEtichettaPrincipale(String etichettaPrincipale) {
		_etichettaPrincipale = etichettaPrincipale;
	}

	private long _contenutoPaginaId;
	private long _codicePagina;
	private String _descrizione;
	private String _placeholder;
	private String _document_blob;
	private String _descrizioneFile;
	private String _tipoFile;
	private String _nomeFile;
	private Date _dataAllegato;
	private Date _dataEtichettaPrincipale;
	private Date _dataAltreProp;
	private String _etichettaPrincipale;

}