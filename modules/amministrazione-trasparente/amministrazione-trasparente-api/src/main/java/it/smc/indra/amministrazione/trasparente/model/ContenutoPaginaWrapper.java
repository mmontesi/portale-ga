/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ContenutoPagina}.
 * </p>
 *
 * @author Simone Sorgon
 * @see ContenutoPagina
 * @generated
 */
@ProviderType
public class ContenutoPaginaWrapper
	implements ContenutoPagina, ModelWrapper<ContenutoPagina> {

	public ContenutoPaginaWrapper(ContenutoPagina contenutoPagina) {
		_contenutoPagina = contenutoPagina;
	}

	@Override
	public Class<?> getModelClass() {
		return ContenutoPagina.class;
	}

	@Override
	public String getModelClassName() {
		return ContenutoPagina.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("contenutoPaginaId", getContenutoPaginaId());
		attributes.put("codicePagina", getCodicePagina());
		attributes.put("descrizione", getDescrizione());
		attributes.put("placeholder", getPlaceholder());
		attributes.put("document_blob", getDocument_blob());
		attributes.put("descrizioneFile", getDescrizioneFile());
		attributes.put("tipoFile", getTipoFile());
		attributes.put("nomeFile", getNomeFile());
		attributes.put("dataAllegato", getDataAllegato());
		attributes.put("dataEtichettaPrincipale", getDataEtichettaPrincipale());
		attributes.put("dataAltreProp", getDataAltreProp());
		attributes.put("etichettaPrincipale", getEtichettaPrincipale());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long contenutoPaginaId = (Long)attributes.get("contenutoPaginaId");

		if (contenutoPaginaId != null) {
			setContenutoPaginaId(contenutoPaginaId);
		}

		Long codicePagina = (Long)attributes.get("codicePagina");

		if (codicePagina != null) {
			setCodicePagina(codicePagina);
		}

		String descrizione = (String)attributes.get("descrizione");

		if (descrizione != null) {
			setDescrizione(descrizione);
		}

		String placeholder = (String)attributes.get("placeholder");

		if (placeholder != null) {
			setPlaceholder(placeholder);
		}

		String document_blob = (String)attributes.get("document_blob");

		if (document_blob != null) {
			setDocument_blob(document_blob);
		}

		String descrizioneFile = (String)attributes.get("descrizioneFile");

		if (descrizioneFile != null) {
			setDescrizioneFile(descrizioneFile);
		}

		String tipoFile = (String)attributes.get("tipoFile");

		if (tipoFile != null) {
			setTipoFile(tipoFile);
		}

		String nomeFile = (String)attributes.get("nomeFile");

		if (nomeFile != null) {
			setNomeFile(nomeFile);
		}

		Date dataAllegato = (Date)attributes.get("dataAllegato");

		if (dataAllegato != null) {
			setDataAllegato(dataAllegato);
		}

		Date dataEtichettaPrincipale = (Date)attributes.get(
			"dataEtichettaPrincipale");

		if (dataEtichettaPrincipale != null) {
			setDataEtichettaPrincipale(dataEtichettaPrincipale);
		}

		Date dataAltreProp = (Date)attributes.get("dataAltreProp");

		if (dataAltreProp != null) {
			setDataAltreProp(dataAltreProp);
		}

		String etichettaPrincipale = (String)attributes.get(
			"etichettaPrincipale");

		if (etichettaPrincipale != null) {
			setEtichettaPrincipale(etichettaPrincipale);
		}
	}

	@Override
	public Object clone() {
		return new ContenutoPaginaWrapper(
			(ContenutoPagina)_contenutoPagina.clone());
	}

	@Override
	public int compareTo(
		it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
			contenutoPagina) {

		return _contenutoPagina.compareTo(contenutoPagina);
	}

	/**
	 * Returns the codice pagina of this contenuto pagina.
	 *
	 * @return the codice pagina of this contenuto pagina
	 */
	@Override
	public long getCodicePagina() {
		return _contenutoPagina.getCodicePagina();
	}

	/**
	 * Returns the contenuto pagina ID of this contenuto pagina.
	 *
	 * @return the contenuto pagina ID of this contenuto pagina
	 */
	@Override
	public long getContenutoPaginaId() {
		return _contenutoPagina.getContenutoPaginaId();
	}

	/**
	 * Returns the data allegato of this contenuto pagina.
	 *
	 * @return the data allegato of this contenuto pagina
	 */
	@Override
	public Date getDataAllegato() {
		return _contenutoPagina.getDataAllegato();
	}

	/**
	 * Returns the data altre prop of this contenuto pagina.
	 *
	 * @return the data altre prop of this contenuto pagina
	 */
	@Override
	public Date getDataAltreProp() {
		return _contenutoPagina.getDataAltreProp();
	}

	/**
	 * Returns the data etichetta principale of this contenuto pagina.
	 *
	 * @return the data etichetta principale of this contenuto pagina
	 */
	@Override
	public Date getDataEtichettaPrincipale() {
		return _contenutoPagina.getDataEtichettaPrincipale();
	}

	/**
	 * Returns the descrizione of this contenuto pagina.
	 *
	 * @return the descrizione of this contenuto pagina
	 */
	@Override
	public String getDescrizione() {
		return _contenutoPagina.getDescrizione();
	}

	/**
	 * Returns the descrizione file of this contenuto pagina.
	 *
	 * @return the descrizione file of this contenuto pagina
	 */
	@Override
	public String getDescrizioneFile() {
		return _contenutoPagina.getDescrizioneFile();
	}

	/**
	 * Returns the document_blob of this contenuto pagina.
	 *
	 * @return the document_blob of this contenuto pagina
	 */
	@Override
	public String getDocument_blob() {
		return _contenutoPagina.getDocument_blob();
	}

	/**
	 * Returns the etichetta principale of this contenuto pagina.
	 *
	 * @return the etichetta principale of this contenuto pagina
	 */
	@Override
	public String getEtichettaPrincipale() {
		return _contenutoPagina.getEtichettaPrincipale();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _contenutoPagina.getExpandoBridge();
	}

	/**
	 * Returns the nome file of this contenuto pagina.
	 *
	 * @return the nome file of this contenuto pagina
	 */
	@Override
	public String getNomeFile() {
		return _contenutoPagina.getNomeFile();
	}

	/**
	 * Returns the placeholder of this contenuto pagina.
	 *
	 * @return the placeholder of this contenuto pagina
	 */
	@Override
	public String getPlaceholder() {
		return _contenutoPagina.getPlaceholder();
	}

	/**
	 * Returns the primary key of this contenuto pagina.
	 *
	 * @return the primary key of this contenuto pagina
	 */
	@Override
	public long getPrimaryKey() {
		return _contenutoPagina.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _contenutoPagina.getPrimaryKeyObj();
	}

	/**
	 * Returns the tipo file of this contenuto pagina.
	 *
	 * @return the tipo file of this contenuto pagina
	 */
	@Override
	public String getTipoFile() {
		return _contenutoPagina.getTipoFile();
	}

	@Override
	public int hashCode() {
		return _contenutoPagina.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _contenutoPagina.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _contenutoPagina.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _contenutoPagina.isNew();
	}

	@Override
	public void persist() {
		_contenutoPagina.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_contenutoPagina.setCachedModel(cachedModel);
	}

	/**
	 * Sets the codice pagina of this contenuto pagina.
	 *
	 * @param codicePagina the codice pagina of this contenuto pagina
	 */
	@Override
	public void setCodicePagina(long codicePagina) {
		_contenutoPagina.setCodicePagina(codicePagina);
	}

	/**
	 * Sets the contenuto pagina ID of this contenuto pagina.
	 *
	 * @param contenutoPaginaId the contenuto pagina ID of this contenuto pagina
	 */
	@Override
	public void setContenutoPaginaId(long contenutoPaginaId) {
		_contenutoPagina.setContenutoPaginaId(contenutoPaginaId);
	}

	/**
	 * Sets the data allegato of this contenuto pagina.
	 *
	 * @param dataAllegato the data allegato of this contenuto pagina
	 */
	@Override
	public void setDataAllegato(Date dataAllegato) {
		_contenutoPagina.setDataAllegato(dataAllegato);
	}

	/**
	 * Sets the data altre prop of this contenuto pagina.
	 *
	 * @param dataAltreProp the data altre prop of this contenuto pagina
	 */
	@Override
	public void setDataAltreProp(Date dataAltreProp) {
		_contenutoPagina.setDataAltreProp(dataAltreProp);
	}

	/**
	 * Sets the data etichetta principale of this contenuto pagina.
	 *
	 * @param dataEtichettaPrincipale the data etichetta principale of this contenuto pagina
	 */
	@Override
	public void setDataEtichettaPrincipale(Date dataEtichettaPrincipale) {
		_contenutoPagina.setDataEtichettaPrincipale(dataEtichettaPrincipale);
	}

	/**
	 * Sets the descrizione of this contenuto pagina.
	 *
	 * @param descrizione the descrizione of this contenuto pagina
	 */
	@Override
	public void setDescrizione(String descrizione) {
		_contenutoPagina.setDescrizione(descrizione);
	}

	/**
	 * Sets the descrizione file of this contenuto pagina.
	 *
	 * @param descrizioneFile the descrizione file of this contenuto pagina
	 */
	@Override
	public void setDescrizioneFile(String descrizioneFile) {
		_contenutoPagina.setDescrizioneFile(descrizioneFile);
	}

	/**
	 * Sets the document_blob of this contenuto pagina.
	 *
	 * @param document_blob the document_blob of this contenuto pagina
	 */
	@Override
	public void setDocument_blob(String document_blob) {
		_contenutoPagina.setDocument_blob(document_blob);
	}

	/**
	 * Sets the etichetta principale of this contenuto pagina.
	 *
	 * @param etichettaPrincipale the etichetta principale of this contenuto pagina
	 */
	@Override
	public void setEtichettaPrincipale(String etichettaPrincipale) {
		_contenutoPagina.setEtichettaPrincipale(etichettaPrincipale);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_contenutoPagina.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_contenutoPagina.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_contenutoPagina.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_contenutoPagina.setNew(n);
	}

	/**
	 * Sets the nome file of this contenuto pagina.
	 *
	 * @param nomeFile the nome file of this contenuto pagina
	 */
	@Override
	public void setNomeFile(String nomeFile) {
		_contenutoPagina.setNomeFile(nomeFile);
	}

	/**
	 * Sets the placeholder of this contenuto pagina.
	 *
	 * @param placeholder the placeholder of this contenuto pagina
	 */
	@Override
	public void setPlaceholder(String placeholder) {
		_contenutoPagina.setPlaceholder(placeholder);
	}

	/**
	 * Sets the primary key of this contenuto pagina.
	 *
	 * @param primaryKey the primary key of this contenuto pagina
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_contenutoPagina.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_contenutoPagina.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the tipo file of this contenuto pagina.
	 *
	 * @param tipoFile the tipo file of this contenuto pagina
	 */
	@Override
	public void setTipoFile(String tipoFile) {
		_contenutoPagina.setTipoFile(tipoFile);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel
		<it.smc.indra.amministrazione.trasparente.model.ContenutoPagina>
			toCacheModel() {

		return _contenutoPagina.toCacheModel();
	}

	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		toEscapedModel() {

		return new ContenutoPaginaWrapper(_contenutoPagina.toEscapedModel());
	}

	@Override
	public String toString() {
		return _contenutoPagina.toString();
	}

	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		toUnescapedModel() {

		return new ContenutoPaginaWrapper(_contenutoPagina.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _contenutoPagina.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ContenutoPaginaWrapper)) {
			return false;
		}

		ContenutoPaginaWrapper contenutoPaginaWrapper =
			(ContenutoPaginaWrapper)obj;

		if (Objects.equals(
				_contenutoPagina, contenutoPaginaWrapper._contenutoPagina)) {

			return true;
		}

		return false;
	}

	@Override
	public ContenutoPagina getWrappedModel() {
		return _contenutoPagina;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _contenutoPagina.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _contenutoPagina.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_contenutoPagina.resetOriginalValues();
	}

	private final ContenutoPagina _contenutoPagina;

}