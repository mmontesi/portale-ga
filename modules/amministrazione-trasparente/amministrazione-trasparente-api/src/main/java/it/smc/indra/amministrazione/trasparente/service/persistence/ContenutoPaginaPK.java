/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */

package it.smc.indra.amministrazione.trasparente.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Serializable;

/**
 * @author Simone Sorgon
 * @generated
 */
@ProviderType
public class ContenutoPaginaPK
	implements Comparable<ContenutoPaginaPK>, Serializable {

	public String dataAllegato;
	public long codicePagina;

	public ContenutoPaginaPK() {
	}

	public ContenutoPaginaPK(String dataAllegato, long codicePagina) {
		this.dataAllegato = dataAllegato;
		this.codicePagina = codicePagina;
	}

	public String getDataAllegato() {
		return dataAllegato;
	}

	public void setDataAllegato(String dataAllegato) {
		this.dataAllegato = dataAllegato;
	}

	public long getCodicePagina() {
		return codicePagina;
	}

	public void setCodicePagina(long codicePagina) {
		this.codicePagina = codicePagina;
	}

	@Override
	public int compareTo(ContenutoPaginaPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		value = dataAllegato.compareTo(pk.dataAllegato);

		if (value != 0) {
			return value;
		}

		if (codicePagina < pk.codicePagina) {
			value = -1;
		}
		else if (codicePagina > pk.codicePagina) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ContenutoPaginaPK)) {
			return false;
		}

		ContenutoPaginaPK pk = (ContenutoPaginaPK)obj;

		if (dataAllegato.equals(pk.dataAllegato) &&
			(codicePagina == pk.codicePagina)) {

			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, dataAllegato);
		hashCode = HashUtil.hash(hashCode, codicePagina);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(6);

		sb.append("{");

		sb.append("dataAllegato=");

		sb.append(dataAllegato);
		sb.append(", codicePagina=");

		sb.append(codicePagina);

		sb.append("}");

		return sb.toString();
	}

}