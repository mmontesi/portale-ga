/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import it.smc.indra.amministrazione.trasparente.exception.NoSuchAlberaturaTrasparenzaException;
import it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza;

import java.io.Serializable;

import java.util.Map;
import java.util.Set;

/**
 * The persistence interface for the alberatura trasparenza service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Simone Sorgon
 * @see AlberaturaTrasparenzaUtil
 * @generated
 */
@ProviderType
public interface AlberaturaTrasparenzaPersistence
	extends BasePersistence<AlberaturaTrasparenza> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AlberaturaTrasparenzaUtil} to access the alberatura trasparenza persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */
	@Override
	public Map<Serializable, AlberaturaTrasparenza> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys);

	/**
	 * Caches the alberatura trasparenza in the entity cache if it is enabled.
	 *
	 * @param alberaturaTrasparenza the alberatura trasparenza
	 */
	public void cacheResult(AlberaturaTrasparenza alberaturaTrasparenza);

	/**
	 * Caches the alberatura trasparenzas in the entity cache if it is enabled.
	 *
	 * @param alberaturaTrasparenzas the alberatura trasparenzas
	 */
	public void cacheResult(
		java.util.List<AlberaturaTrasparenza> alberaturaTrasparenzas);

	/**
	 * Creates a new alberatura trasparenza with the primary key. Does not add the alberatura trasparenza to the database.
	 *
	 * @param codicePagina the primary key for the new alberatura trasparenza
	 * @return the new alberatura trasparenza
	 */
	public AlberaturaTrasparenza create(long codicePagina);

	/**
	 * Removes the alberatura trasparenza with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza that was removed
	 * @throws NoSuchAlberaturaTrasparenzaException if a alberatura trasparenza with the primary key could not be found
	 */
	public AlberaturaTrasparenza remove(long codicePagina)
		throws NoSuchAlberaturaTrasparenzaException;

	public AlberaturaTrasparenza updateImpl(
		AlberaturaTrasparenza alberaturaTrasparenza);

	/**
	 * Returns the alberatura trasparenza with the primary key or throws a <code>NoSuchAlberaturaTrasparenzaException</code> if it could not be found.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza
	 * @throws NoSuchAlberaturaTrasparenzaException if a alberatura trasparenza with the primary key could not be found
	 */
	public AlberaturaTrasparenza findByPrimaryKey(long codicePagina)
		throws NoSuchAlberaturaTrasparenzaException;

	/**
	 * Returns the alberatura trasparenza with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza, or <code>null</code> if a alberatura trasparenza with the primary key could not be found
	 */
	public AlberaturaTrasparenza fetchByPrimaryKey(long codicePagina);

	/**
	 * Returns all the alberatura trasparenzas.
	 *
	 * @return the alberatura trasparenzas
	 */
	public java.util.List<AlberaturaTrasparenza> findAll();

	/**
	 * Returns a range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @return the range of alberatura trasparenzas
	 */
	public java.util.List<AlberaturaTrasparenza> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of alberatura trasparenzas
	 */
	public java.util.List<AlberaturaTrasparenza> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AlberaturaTrasparenza>
			orderByComparator);

	/**
	 * Returns an ordered range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of alberatura trasparenzas
	 */
	public java.util.List<AlberaturaTrasparenza> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AlberaturaTrasparenza>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Removes all the alberatura trasparenzas from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of alberatura trasparenzas.
	 *
	 * @return the number of alberatura trasparenzas
	 */
	public int countAll();

}