/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */

package it.smc.indra.amministrazione.trasparente.model;

import aQute.bnd.annotation.ProviderType;

import java.sql.Blob;

/**
 * The Blob model class for lazy loading the document_blob column in ContenutoPagina.
 *
 * @author Simone Sorgon
 * @see ContenutoPagina
 * @generated
 */
@ProviderType
public class ContenutoPaginaDocument_blobBlobModel {

	public ContenutoPaginaDocument_blobBlobModel() {
	}

	public ContenutoPaginaDocument_blobBlobModel(long contenutoPaginaId) {
		_contenutoPaginaId = contenutoPaginaId;
	}

	public ContenutoPaginaDocument_blobBlobModel(
		long contenutoPaginaId, Blob document_blobBlob) {

		_contenutoPaginaId = contenutoPaginaId;
		_document_blobBlob = document_blobBlob;
	}

	public long getContenutoPaginaId() {
		return _contenutoPaginaId;
	}

	public void setContenutoPaginaId(long contenutoPaginaId) {
		_contenutoPaginaId = contenutoPaginaId;
	}

	public Blob getDocument_blobBlob() {
		return _document_blobBlob;
	}

	public void setDocument_blobBlob(Blob document_blobBlob) {
		_document_blobBlob = document_blobBlob;
	}

	private long _contenutoPaginaId;
	private Blob _document_blobBlob;

}