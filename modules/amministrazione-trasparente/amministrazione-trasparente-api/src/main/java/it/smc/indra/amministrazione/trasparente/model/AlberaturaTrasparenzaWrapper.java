/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link AlberaturaTrasparenza}.
 * </p>
 *
 * @author Simone Sorgon
 * @see AlberaturaTrasparenza
 * @generated
 */
@ProviderType
public class AlberaturaTrasparenzaWrapper
	implements AlberaturaTrasparenza, ModelWrapper<AlberaturaTrasparenza> {

	public AlberaturaTrasparenzaWrapper(
		AlberaturaTrasparenza alberaturaTrasparenza) {

		_alberaturaTrasparenza = alberaturaTrasparenza;
	}

	@Override
	public Class<?> getModelClass() {
		return AlberaturaTrasparenza.class;
	}

	@Override
	public String getModelClassName() {
		return AlberaturaTrasparenza.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("codicePagina", getCodicePagina());
		attributes.put("codicePaginaPadre", getCodicePaginaPadre());
		attributes.put("descrizionePagina", getDescrizionePagina());
		attributes.put("titoloPagina", getTitoloPagina());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long codicePagina = (Long)attributes.get("codicePagina");

		if (codicePagina != null) {
			setCodicePagina(codicePagina);
		}

		Long codicePaginaPadre = (Long)attributes.get("codicePaginaPadre");

		if (codicePaginaPadre != null) {
			setCodicePaginaPadre(codicePaginaPadre);
		}

		String descrizionePagina = (String)attributes.get("descrizionePagina");

		if (descrizionePagina != null) {
			setDescrizionePagina(descrizionePagina);
		}

		String titoloPagina = (String)attributes.get("titoloPagina");

		if (titoloPagina != null) {
			setTitoloPagina(titoloPagina);
		}
	}

	@Override
	public Object clone() {
		return new AlberaturaTrasparenzaWrapper(
			(AlberaturaTrasparenza)_alberaturaTrasparenza.clone());
	}

	@Override
	public int compareTo(
		it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
			alberaturaTrasparenza) {

		return _alberaturaTrasparenza.compareTo(alberaturaTrasparenza);
	}

	/**
	 * Returns the codice pagina of this alberatura trasparenza.
	 *
	 * @return the codice pagina of this alberatura trasparenza
	 */
	@Override
	public long getCodicePagina() {
		return _alberaturaTrasparenza.getCodicePagina();
	}

	/**
	 * Returns the codice pagina padre of this alberatura trasparenza.
	 *
	 * @return the codice pagina padre of this alberatura trasparenza
	 */
	@Override
	public long getCodicePaginaPadre() {
		return _alberaturaTrasparenza.getCodicePaginaPadre();
	}

	/**
	 * Returns the descrizione pagina of this alberatura trasparenza.
	 *
	 * @return the descrizione pagina of this alberatura trasparenza
	 */
	@Override
	public String getDescrizionePagina() {
		return _alberaturaTrasparenza.getDescrizionePagina();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _alberaturaTrasparenza.getExpandoBridge();
	}

	/**
	 * Returns the primary key of this alberatura trasparenza.
	 *
	 * @return the primary key of this alberatura trasparenza
	 */
	@Override
	public long getPrimaryKey() {
		return _alberaturaTrasparenza.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _alberaturaTrasparenza.getPrimaryKeyObj();
	}

	/**
	 * Returns the titolo pagina of this alberatura trasparenza.
	 *
	 * @return the titolo pagina of this alberatura trasparenza
	 */
	@Override
	public String getTitoloPagina() {
		return _alberaturaTrasparenza.getTitoloPagina();
	}

	@Override
	public int hashCode() {
		return _alberaturaTrasparenza.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _alberaturaTrasparenza.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _alberaturaTrasparenza.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _alberaturaTrasparenza.isNew();
	}

	@Override
	public void persist() {
		_alberaturaTrasparenza.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_alberaturaTrasparenza.setCachedModel(cachedModel);
	}

	/**
	 * Sets the codice pagina of this alberatura trasparenza.
	 *
	 * @param codicePagina the codice pagina of this alberatura trasparenza
	 */
	@Override
	public void setCodicePagina(long codicePagina) {
		_alberaturaTrasparenza.setCodicePagina(codicePagina);
	}

	/**
	 * Sets the codice pagina padre of this alberatura trasparenza.
	 *
	 * @param codicePaginaPadre the codice pagina padre of this alberatura trasparenza
	 */
	@Override
	public void setCodicePaginaPadre(long codicePaginaPadre) {
		_alberaturaTrasparenza.setCodicePaginaPadre(codicePaginaPadre);
	}

	/**
	 * Sets the descrizione pagina of this alberatura trasparenza.
	 *
	 * @param descrizionePagina the descrizione pagina of this alberatura trasparenza
	 */
	@Override
	public void setDescrizionePagina(String descrizionePagina) {
		_alberaturaTrasparenza.setDescrizionePagina(descrizionePagina);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_alberaturaTrasparenza.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_alberaturaTrasparenza.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_alberaturaTrasparenza.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_alberaturaTrasparenza.setNew(n);
	}

	/**
	 * Sets the primary key of this alberatura trasparenza.
	 *
	 * @param primaryKey the primary key of this alberatura trasparenza
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_alberaturaTrasparenza.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_alberaturaTrasparenza.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the titolo pagina of this alberatura trasparenza.
	 *
	 * @param titoloPagina the titolo pagina of this alberatura trasparenza
	 */
	@Override
	public void setTitoloPagina(String titoloPagina) {
		_alberaturaTrasparenza.setTitoloPagina(titoloPagina);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel
		<it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza>
			toCacheModel() {

		return _alberaturaTrasparenza.toCacheModel();
	}

	@Override
	public it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
		toEscapedModel() {

		return new AlberaturaTrasparenzaWrapper(
			_alberaturaTrasparenza.toEscapedModel());
	}

	@Override
	public String toString() {
		return _alberaturaTrasparenza.toString();
	}

	@Override
	public it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
		toUnescapedModel() {

		return new AlberaturaTrasparenzaWrapper(
			_alberaturaTrasparenza.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _alberaturaTrasparenza.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AlberaturaTrasparenzaWrapper)) {
			return false;
		}

		AlberaturaTrasparenzaWrapper alberaturaTrasparenzaWrapper =
			(AlberaturaTrasparenzaWrapper)obj;

		if (Objects.equals(
				_alberaturaTrasparenza,
				alberaturaTrasparenzaWrapper._alberaturaTrasparenza)) {

			return true;
		}

		return false;
	}

	@Override
	public AlberaturaTrasparenza getWrappedModel() {
		return _alberaturaTrasparenza;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _alberaturaTrasparenza.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _alberaturaTrasparenza.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_alberaturaTrasparenza.resetOriginalValues();
	}

	private final AlberaturaTrasparenza _alberaturaTrasparenza;

}