/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ContenutoPaginaLocalService}.
 *
 * @author Simone Sorgon
 * @see ContenutoPaginaLocalService
 * @generated
 */
@ProviderType
public class ContenutoPaginaLocalServiceWrapper
	implements ContenutoPaginaLocalService,
			   ServiceWrapper<ContenutoPaginaLocalService> {

	public ContenutoPaginaLocalServiceWrapper(
		ContenutoPaginaLocalService contenutoPaginaLocalService) {

		_contenutoPaginaLocalService = contenutoPaginaLocalService;
	}

	/**
	 * Adds the contenuto pagina to the database. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPagina the contenuto pagina
	 * @return the contenuto pagina that was added
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		addContenutoPagina(
			it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
				contenutoPagina) {

		return _contenutoPaginaLocalService.addContenutoPagina(contenutoPagina);
	}

	/**
	 * Creates a new contenuto pagina with the primary key. Does not add the contenuto pagina to the database.
	 *
	 * @param contenutoPaginaId the primary key for the new contenuto pagina
	 * @return the new contenuto pagina
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		createContenutoPagina(long contenutoPaginaId) {

		return _contenutoPaginaLocalService.createContenutoPagina(
			contenutoPaginaId);
	}

	/**
	 * Deletes the contenuto pagina from the database. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPagina the contenuto pagina
	 * @return the contenuto pagina that was removed
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		deleteContenutoPagina(
			it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
				contenutoPagina) {

		return _contenutoPaginaLocalService.deleteContenutoPagina(
			contenutoPagina);
	}

	/**
	 * Deletes the contenuto pagina with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina that was removed
	 * @throws PortalException if a contenuto pagina with the primary key could not be found
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
			deleteContenutoPagina(long contenutoPaginaId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contenutoPaginaLocalService.deleteContenutoPagina(
			contenutoPaginaId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contenutoPaginaLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _contenutoPaginaLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _contenutoPaginaLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _contenutoPaginaLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _contenutoPaginaLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _contenutoPaginaLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _contenutoPaginaLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		fetchContenutoPagina(long contenutoPaginaId) {

		return _contenutoPaginaLocalService.fetchContenutoPagina(
			contenutoPaginaId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _contenutoPaginaLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the contenuto pagina with the primary key.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina
	 * @throws PortalException if a contenuto pagina with the primary key could not be found
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
			getContenutoPagina(long contenutoPaginaId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contenutoPaginaLocalService.getContenutoPagina(
			contenutoPaginaId);
	}

	/**
	 * Returns a range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @return the range of contenuto paginas
	 */
	@Override
	public java.util.List
		<it.smc.indra.amministrazione.trasparente.model.ContenutoPagina>
			getContenutoPaginas(int start, int end) {

		return _contenutoPaginaLocalService.getContenutoPaginas(start, end);
	}

	/**
	 * Returns the number of contenuto paginas.
	 *
	 * @return the number of contenuto paginas
	 */
	@Override
	public int getContenutoPaginasCount() {
		return _contenutoPaginaLocalService.getContenutoPaginasCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _contenutoPaginaLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _contenutoPaginaLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contenutoPaginaLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the contenuto pagina in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPagina the contenuto pagina
	 * @return the contenuto pagina that was updated
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		updateContenutoPagina(
			it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
				contenutoPagina) {

		return _contenutoPaginaLocalService.updateContenutoPagina(
			contenutoPagina);
	}

	@Override
	public ContenutoPaginaLocalService getWrappedService() {
		return _contenutoPaginaLocalService;
	}

	@Override
	public void setWrappedService(
		ContenutoPaginaLocalService contenutoPaginaLocalService) {

		_contenutoPaginaLocalService = contenutoPaginaLocalService;
	}

	private ContenutoPaginaLocalService _contenutoPaginaLocalService;

}