/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the alberatura trasparenza service. This utility wraps <code>it.smc.indra.amministrazione.trasparente.service.persistence.impl.AlberaturaTrasparenzaPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Simone Sorgon
 * @see AlberaturaTrasparenzaPersistence
 * @generated
 */
@ProviderType
public class AlberaturaTrasparenzaUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(AlberaturaTrasparenza alberaturaTrasparenza) {
		getPersistence().clearCache(alberaturaTrasparenza);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, AlberaturaTrasparenza> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AlberaturaTrasparenza> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AlberaturaTrasparenza> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AlberaturaTrasparenza> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<AlberaturaTrasparenza> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static AlberaturaTrasparenza update(
		AlberaturaTrasparenza alberaturaTrasparenza) {

		return getPersistence().update(alberaturaTrasparenza);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static AlberaturaTrasparenza update(
		AlberaturaTrasparenza alberaturaTrasparenza,
		ServiceContext serviceContext) {

		return getPersistence().update(alberaturaTrasparenza, serviceContext);
	}

	/**
	 * Caches the alberatura trasparenza in the entity cache if it is enabled.
	 *
	 * @param alberaturaTrasparenza the alberatura trasparenza
	 */
	public static void cacheResult(
		AlberaturaTrasparenza alberaturaTrasparenza) {

		getPersistence().cacheResult(alberaturaTrasparenza);
	}

	/**
	 * Caches the alberatura trasparenzas in the entity cache if it is enabled.
	 *
	 * @param alberaturaTrasparenzas the alberatura trasparenzas
	 */
	public static void cacheResult(
		List<AlberaturaTrasparenza> alberaturaTrasparenzas) {

		getPersistence().cacheResult(alberaturaTrasparenzas);
	}

	/**
	 * Creates a new alberatura trasparenza with the primary key. Does not add the alberatura trasparenza to the database.
	 *
	 * @param codicePagina the primary key for the new alberatura trasparenza
	 * @return the new alberatura trasparenza
	 */
	public static AlberaturaTrasparenza create(long codicePagina) {
		return getPersistence().create(codicePagina);
	}

	/**
	 * Removes the alberatura trasparenza with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza that was removed
	 * @throws NoSuchAlberaturaTrasparenzaException if a alberatura trasparenza with the primary key could not be found
	 */
	public static AlberaturaTrasparenza remove(long codicePagina)
		throws it.smc.indra.amministrazione.trasparente.exception.
			NoSuchAlberaturaTrasparenzaException {

		return getPersistence().remove(codicePagina);
	}

	public static AlberaturaTrasparenza updateImpl(
		AlberaturaTrasparenza alberaturaTrasparenza) {

		return getPersistence().updateImpl(alberaturaTrasparenza);
	}

	/**
	 * Returns the alberatura trasparenza with the primary key or throws a <code>NoSuchAlberaturaTrasparenzaException</code> if it could not be found.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza
	 * @throws NoSuchAlberaturaTrasparenzaException if a alberatura trasparenza with the primary key could not be found
	 */
	public static AlberaturaTrasparenza findByPrimaryKey(long codicePagina)
		throws it.smc.indra.amministrazione.trasparente.exception.
			NoSuchAlberaturaTrasparenzaException {

		return getPersistence().findByPrimaryKey(codicePagina);
	}

	/**
	 * Returns the alberatura trasparenza with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza, or <code>null</code> if a alberatura trasparenza with the primary key could not be found
	 */
	public static AlberaturaTrasparenza fetchByPrimaryKey(long codicePagina) {
		return getPersistence().fetchByPrimaryKey(codicePagina);
	}

	/**
	 * Returns all the alberatura trasparenzas.
	 *
	 * @return the alberatura trasparenzas
	 */
	public static List<AlberaturaTrasparenza> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @return the range of alberatura trasparenzas
	 */
	public static List<AlberaturaTrasparenza> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of alberatura trasparenzas
	 */
	public static List<AlberaturaTrasparenza> findAll(
		int start, int end,
		OrderByComparator<AlberaturaTrasparenza> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of alberatura trasparenzas
	 */
	public static List<AlberaturaTrasparenza> findAll(
		int start, int end,
		OrderByComparator<AlberaturaTrasparenza> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Removes all the alberatura trasparenzas from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of alberatura trasparenzas.
	 *
	 * @return the number of alberatura trasparenzas
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static AlberaturaTrasparenzaPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<AlberaturaTrasparenzaPersistence, AlberaturaTrasparenzaPersistence>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			AlberaturaTrasparenzaPersistence.class);

		ServiceTracker
			<AlberaturaTrasparenzaPersistence, AlberaturaTrasparenzaPersistence>
				serviceTracker =
					new ServiceTracker
						<AlberaturaTrasparenzaPersistence,
						 AlberaturaTrasparenzaPersistence>(
							 bundle.getBundleContext(),
							 AlberaturaTrasparenzaPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}