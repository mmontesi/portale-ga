/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Simone Sorgon
 * @generated
 */
@ProviderType
public class AlberaturaTrasparenzaSoap implements Serializable {

	public static AlberaturaTrasparenzaSoap toSoapModel(
		AlberaturaTrasparenza model) {

		AlberaturaTrasparenzaSoap soapModel = new AlberaturaTrasparenzaSoap();

		soapModel.setCodicePagina(model.getCodicePagina());
		soapModel.setCodicePaginaPadre(model.getCodicePaginaPadre());
		soapModel.setDescrizionePagina(model.getDescrizionePagina());
		soapModel.setTitoloPagina(model.getTitoloPagina());

		return soapModel;
	}

	public static AlberaturaTrasparenzaSoap[] toSoapModels(
		AlberaturaTrasparenza[] models) {

		AlberaturaTrasparenzaSoap[] soapModels =
			new AlberaturaTrasparenzaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AlberaturaTrasparenzaSoap[][] toSoapModels(
		AlberaturaTrasparenza[][] models) {

		AlberaturaTrasparenzaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new AlberaturaTrasparenzaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AlberaturaTrasparenzaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AlberaturaTrasparenzaSoap[] toSoapModels(
		List<AlberaturaTrasparenza> models) {

		List<AlberaturaTrasparenzaSoap> soapModels =
			new ArrayList<AlberaturaTrasparenzaSoap>(models.size());

		for (AlberaturaTrasparenza model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(
			new AlberaturaTrasparenzaSoap[soapModels.size()]);
	}

	public AlberaturaTrasparenzaSoap() {
	}

	public long getPrimaryKey() {
		return _codicePagina;
	}

	public void setPrimaryKey(long pk) {
		setCodicePagina(pk);
	}

	public long getCodicePagina() {
		return _codicePagina;
	}

	public void setCodicePagina(long codicePagina) {
		_codicePagina = codicePagina;
	}

	public long getCodicePaginaPadre() {
		return _codicePaginaPadre;
	}

	public void setCodicePaginaPadre(long codicePaginaPadre) {
		_codicePaginaPadre = codicePaginaPadre;
	}

	public String getDescrizionePagina() {
		return _descrizionePagina;
	}

	public void setDescrizionePagina(String descrizionePagina) {
		_descrizionePagina = descrizionePagina;
	}

	public String getTitoloPagina() {
		return _titoloPagina;
	}

	public void setTitoloPagina(String titoloPagina) {
		_titoloPagina = titoloPagina;
	}

	private long _codicePagina;
	private long _codicePaginaPadre;
	private String _descrizionePagina;
	private String _titoloPagina;

}