/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.smc.indra.amministrazione.trasparente.model.ContenutoPagina;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the contenuto pagina service. This utility wraps <code>it.smc.indra.amministrazione.trasparente.service.persistence.impl.ContenutoPaginaPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Simone Sorgon
 * @see ContenutoPaginaPersistence
 * @generated
 */
@ProviderType
public class ContenutoPaginaUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ContenutoPagina contenutoPagina) {
		getPersistence().clearCache(contenutoPagina);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, ContenutoPagina> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ContenutoPagina> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ContenutoPagina> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ContenutoPagina> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ContenutoPagina> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ContenutoPagina update(ContenutoPagina contenutoPagina) {
		return getPersistence().update(contenutoPagina);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ContenutoPagina update(
		ContenutoPagina contenutoPagina, ServiceContext serviceContext) {

		return getPersistence().update(contenutoPagina, serviceContext);
	}

	/**
	 * Caches the contenuto pagina in the entity cache if it is enabled.
	 *
	 * @param contenutoPagina the contenuto pagina
	 */
	public static void cacheResult(ContenutoPagina contenutoPagina) {
		getPersistence().cacheResult(contenutoPagina);
	}

	/**
	 * Caches the contenuto paginas in the entity cache if it is enabled.
	 *
	 * @param contenutoPaginas the contenuto paginas
	 */
	public static void cacheResult(List<ContenutoPagina> contenutoPaginas) {
		getPersistence().cacheResult(contenutoPaginas);
	}

	/**
	 * Creates a new contenuto pagina with the primary key. Does not add the contenuto pagina to the database.
	 *
	 * @param contenutoPaginaId the primary key for the new contenuto pagina
	 * @return the new contenuto pagina
	 */
	public static ContenutoPagina create(long contenutoPaginaId) {
		return getPersistence().create(contenutoPaginaId);
	}

	/**
	 * Removes the contenuto pagina with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina that was removed
	 * @throws NoSuchContenutoPaginaException if a contenuto pagina with the primary key could not be found
	 */
	public static ContenutoPagina remove(long contenutoPaginaId)
		throws it.smc.indra.amministrazione.trasparente.exception.
			NoSuchContenutoPaginaException {

		return getPersistence().remove(contenutoPaginaId);
	}

	public static ContenutoPagina updateImpl(ContenutoPagina contenutoPagina) {
		return getPersistence().updateImpl(contenutoPagina);
	}

	/**
	 * Returns the contenuto pagina with the primary key or throws a <code>NoSuchContenutoPaginaException</code> if it could not be found.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina
	 * @throws NoSuchContenutoPaginaException if a contenuto pagina with the primary key could not be found
	 */
	public static ContenutoPagina findByPrimaryKey(long contenutoPaginaId)
		throws it.smc.indra.amministrazione.trasparente.exception.
			NoSuchContenutoPaginaException {

		return getPersistence().findByPrimaryKey(contenutoPaginaId);
	}

	/**
	 * Returns the contenuto pagina with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina, or <code>null</code> if a contenuto pagina with the primary key could not be found
	 */
	public static ContenutoPagina fetchByPrimaryKey(long contenutoPaginaId) {
		return getPersistence().fetchByPrimaryKey(contenutoPaginaId);
	}

	/**
	 * Returns all the contenuto paginas.
	 *
	 * @return the contenuto paginas
	 */
	public static List<ContenutoPagina> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @return the range of contenuto paginas
	 */
	public static List<ContenutoPagina> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of contenuto paginas
	 */
	public static List<ContenutoPagina> findAll(
		int start, int end,
		OrderByComparator<ContenutoPagina> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of contenuto paginas
	 */
	public static List<ContenutoPagina> findAll(
		int start, int end,
		OrderByComparator<ContenutoPagina> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Removes all the contenuto paginas from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of contenuto paginas.
	 *
	 * @return the number of contenuto paginas
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ContenutoPaginaPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<ContenutoPaginaPersistence, ContenutoPaginaPersistence>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			ContenutoPaginaPersistence.class);

		ServiceTracker<ContenutoPaginaPersistence, ContenutoPaginaPersistence>
			serviceTracker =
				new ServiceTracker
					<ContenutoPaginaPersistence, ContenutoPaginaPersistence>(
						bundle.getBundleContext(),
						ContenutoPaginaPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}