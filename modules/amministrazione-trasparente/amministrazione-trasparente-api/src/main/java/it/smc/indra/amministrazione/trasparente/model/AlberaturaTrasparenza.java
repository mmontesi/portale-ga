/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the AlberaturaTrasparenza service. Represents a row in the &quot;bk_alberaturatrasparenza&quot; database table, with each column mapped to a property of this class.
 *
 * @author Simone Sorgon
 * @see AlberaturaTrasparenzaModel
 * @generated
 */
@ImplementationClassName(
	"it.smc.indra.amministrazione.trasparente.model.impl.AlberaturaTrasparenzaImpl"
)
@ProviderType
public interface AlberaturaTrasparenza
	extends AlberaturaTrasparenzaModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>it.smc.indra.amministrazione.trasparente.model.impl.AlberaturaTrasparenzaImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<AlberaturaTrasparenza, Long>
		CODICE_PAGINA_ACCESSOR = new Accessor<AlberaturaTrasparenza, Long>() {

			@Override
			public Long get(AlberaturaTrasparenza alberaturaTrasparenza) {
				return alberaturaTrasparenza.getCodicePagina();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<AlberaturaTrasparenza> getTypeClass() {
				return AlberaturaTrasparenza.class;
			}

		};

}