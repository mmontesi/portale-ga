/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the ContenutoPagina service. Represents a row in the &quot;bk_contenutopagina&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation <code>it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaModelImpl</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in <code>it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaImpl</code>.
 * </p>
 *
 * @author Simone Sorgon
 * @see ContenutoPagina
 * @generated
 */
@ProviderType
public interface ContenutoPaginaModel extends BaseModel<ContenutoPagina> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a contenuto pagina model instance should use the {@link ContenutoPagina} interface instead.
	 */

	/**
	 * Returns the primary key of this contenuto pagina.
	 *
	 * @return the primary key of this contenuto pagina
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this contenuto pagina.
	 *
	 * @param primaryKey the primary key of this contenuto pagina
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the contenuto pagina ID of this contenuto pagina.
	 *
	 * @return the contenuto pagina ID of this contenuto pagina
	 */
	public long getContenutoPaginaId();

	/**
	 * Sets the contenuto pagina ID of this contenuto pagina.
	 *
	 * @param contenutoPaginaId the contenuto pagina ID of this contenuto pagina
	 */
	public void setContenutoPaginaId(long contenutoPaginaId);

	/**
	 * Returns the codice pagina of this contenuto pagina.
	 *
	 * @return the codice pagina of this contenuto pagina
	 */
	public long getCodicePagina();

	/**
	 * Sets the codice pagina of this contenuto pagina.
	 *
	 * @param codicePagina the codice pagina of this contenuto pagina
	 */
	public void setCodicePagina(long codicePagina);

	/**
	 * Returns the descrizione of this contenuto pagina.
	 *
	 * @return the descrizione of this contenuto pagina
	 */
	@AutoEscape
	public String getDescrizione();

	/**
	 * Sets the descrizione of this contenuto pagina.
	 *
	 * @param descrizione the descrizione of this contenuto pagina
	 */
	public void setDescrizione(String descrizione);

	/**
	 * Returns the placeholder of this contenuto pagina.
	 *
	 * @return the placeholder of this contenuto pagina
	 */
	@AutoEscape
	public String getPlaceholder();

	/**
	 * Sets the placeholder of this contenuto pagina.
	 *
	 * @param placeholder the placeholder of this contenuto pagina
	 */
	public void setPlaceholder(String placeholder);

	/**
	 * Returns the document_blob of this contenuto pagina.
	 *
	 * @return the document_blob of this contenuto pagina
	 */
	@AutoEscape
	public String getDocument_blob();

	/**
	 * Sets the document_blob of this contenuto pagina.
	 *
	 * @param document_blob the document_blob of this contenuto pagina
	 */
	public void setDocument_blob(String document_blob);

	/**
	 * Returns the descrizione file of this contenuto pagina.
	 *
	 * @return the descrizione file of this contenuto pagina
	 */
	@AutoEscape
	public String getDescrizioneFile();

	/**
	 * Sets the descrizione file of this contenuto pagina.
	 *
	 * @param descrizioneFile the descrizione file of this contenuto pagina
	 */
	public void setDescrizioneFile(String descrizioneFile);

	/**
	 * Returns the tipo file of this contenuto pagina.
	 *
	 * @return the tipo file of this contenuto pagina
	 */
	@AutoEscape
	public String getTipoFile();

	/**
	 * Sets the tipo file of this contenuto pagina.
	 *
	 * @param tipoFile the tipo file of this contenuto pagina
	 */
	public void setTipoFile(String tipoFile);

	/**
	 * Returns the nome file of this contenuto pagina.
	 *
	 * @return the nome file of this contenuto pagina
	 */
	@AutoEscape
	public String getNomeFile();

	/**
	 * Sets the nome file of this contenuto pagina.
	 *
	 * @param nomeFile the nome file of this contenuto pagina
	 */
	public void setNomeFile(String nomeFile);

	/**
	 * Returns the data allegato of this contenuto pagina.
	 *
	 * @return the data allegato of this contenuto pagina
	 */
	public Date getDataAllegato();

	/**
	 * Sets the data allegato of this contenuto pagina.
	 *
	 * @param dataAllegato the data allegato of this contenuto pagina
	 */
	public void setDataAllegato(Date dataAllegato);

	/**
	 * Returns the data etichetta principale of this contenuto pagina.
	 *
	 * @return the data etichetta principale of this contenuto pagina
	 */
	public Date getDataEtichettaPrincipale();

	/**
	 * Sets the data etichetta principale of this contenuto pagina.
	 *
	 * @param dataEtichettaPrincipale the data etichetta principale of this contenuto pagina
	 */
	public void setDataEtichettaPrincipale(Date dataEtichettaPrincipale);

	/**
	 * Returns the data altre prop of this contenuto pagina.
	 *
	 * @return the data altre prop of this contenuto pagina
	 */
	public Date getDataAltreProp();

	/**
	 * Sets the data altre prop of this contenuto pagina.
	 *
	 * @param dataAltreProp the data altre prop of this contenuto pagina
	 */
	public void setDataAltreProp(Date dataAltreProp);

	/**
	 * Returns the etichetta principale of this contenuto pagina.
	 *
	 * @return the etichetta principale of this contenuto pagina
	 */
	@AutoEscape
	public String getEtichettaPrincipale();

	/**
	 * Sets the etichetta principale of this contenuto pagina.
	 *
	 * @param etichettaPrincipale the etichetta principale of this contenuto pagina
	 */
	public void setEtichettaPrincipale(String etichettaPrincipale);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
			contenutoPagina);

	@Override
	public int hashCode();

	@Override
	public CacheModel
		<it.smc.indra.amministrazione.trasparente.model.ContenutoPagina>
			toCacheModel();

	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		toEscapedModel();

	@Override
	public it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();

}