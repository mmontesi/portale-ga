/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the ContenutoPagina service. Represents a row in the &quot;bk_contenutopagina&quot; database table, with each column mapped to a property of this class.
 *
 * @author Simone Sorgon
 * @see ContenutoPaginaModel
 * @generated
 */
@ImplementationClassName(
	"it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaImpl"
)
@ProviderType
public interface ContenutoPagina extends ContenutoPaginaModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<ContenutoPagina, Long>
		CONTENUTO_PAGINA_ID_ACCESSOR = new Accessor<ContenutoPagina, Long>() {

			@Override
			public Long get(ContenutoPagina contenutoPagina) {
				return contenutoPagina.getContenutoPaginaId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<ContenutoPagina> getTypeClass() {
				return ContenutoPagina.class;
			}

		};

}