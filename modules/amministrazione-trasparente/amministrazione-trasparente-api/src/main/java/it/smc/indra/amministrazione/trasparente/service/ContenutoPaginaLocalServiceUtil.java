/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ContenutoPagina. This utility wraps
 * <code>it.smc.indra.amministrazione.trasparente.service.impl.ContenutoPaginaLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Simone Sorgon
 * @see ContenutoPaginaLocalService
 * @generated
 */
@ProviderType
public class ContenutoPaginaLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>it.smc.indra.amministrazione.trasparente.service.impl.ContenutoPaginaLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the contenuto pagina to the database. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPagina the contenuto pagina
	 * @return the contenuto pagina that was added
	 */
	public static it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		addContenutoPagina(
			it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
				contenutoPagina) {

		return getService().addContenutoPagina(contenutoPagina);
	}

	/**
	 * Creates a new contenuto pagina with the primary key. Does not add the contenuto pagina to the database.
	 *
	 * @param contenutoPaginaId the primary key for the new contenuto pagina
	 * @return the new contenuto pagina
	 */
	public static it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		createContenutoPagina(long contenutoPaginaId) {

		return getService().createContenutoPagina(contenutoPaginaId);
	}

	/**
	 * Deletes the contenuto pagina from the database. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPagina the contenuto pagina
	 * @return the contenuto pagina that was removed
	 */
	public static it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		deleteContenutoPagina(
			it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
				contenutoPagina) {

		return getService().deleteContenutoPagina(contenutoPagina);
	}

	/**
	 * Deletes the contenuto pagina with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina that was removed
	 * @throws PortalException if a contenuto pagina with the primary key could not be found
	 */
	public static it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
			deleteContenutoPagina(long contenutoPaginaId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteContenutoPagina(contenutoPaginaId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		fetchContenutoPagina(long contenutoPaginaId) {

		return getService().fetchContenutoPagina(contenutoPaginaId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the contenuto pagina with the primary key.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina
	 * @throws PortalException if a contenuto pagina with the primary key could not be found
	 */
	public static it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
			getContenutoPagina(long contenutoPaginaId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getContenutoPagina(contenutoPaginaId);
	}

	/**
	 * Returns a range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @return the range of contenuto paginas
	 */
	public static java.util.List
		<it.smc.indra.amministrazione.trasparente.model.ContenutoPagina>
			getContenutoPaginas(int start, int end) {

		return getService().getContenutoPaginas(start, end);
	}

	/**
	 * Returns the number of contenuto paginas.
	 *
	 * @return the number of contenuto paginas
	 */
	public static int getContenutoPaginasCount() {
		return getService().getContenutoPaginasCount();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the contenuto pagina in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPagina the contenuto pagina
	 * @return the contenuto pagina that was updated
	 */
	public static it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
		updateContenutoPagina(
			it.smc.indra.amministrazione.trasparente.model.ContenutoPagina
				contenutoPagina) {

		return getService().updateContenutoPagina(contenutoPagina);
	}

	public static ContenutoPaginaLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<ContenutoPaginaLocalService, ContenutoPaginaLocalService>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			ContenutoPaginaLocalService.class);

		ServiceTracker<ContenutoPaginaLocalService, ContenutoPaginaLocalService>
			serviceTracker =
				new ServiceTracker
					<ContenutoPaginaLocalService, ContenutoPaginaLocalService>(
						bundle.getBundleContext(),
						ContenutoPaginaLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}