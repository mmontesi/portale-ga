/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import it.smc.indra.amministrazione.trasparente.exception.NoSuchContenutoPaginaException;
import it.smc.indra.amministrazione.trasparente.model.ContenutoPagina;

import java.io.Serializable;

import java.util.Map;
import java.util.Set;

/**
 * The persistence interface for the contenuto pagina service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Simone Sorgon
 * @see ContenutoPaginaUtil
 * @generated
 */
@ProviderType
public interface ContenutoPaginaPersistence
	extends BasePersistence<ContenutoPagina> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ContenutoPaginaUtil} to access the contenuto pagina persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */
	@Override
	public Map<Serializable, ContenutoPagina> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys);

	/**
	 * Caches the contenuto pagina in the entity cache if it is enabled.
	 *
	 * @param contenutoPagina the contenuto pagina
	 */
	public void cacheResult(ContenutoPagina contenutoPagina);

	/**
	 * Caches the contenuto paginas in the entity cache if it is enabled.
	 *
	 * @param contenutoPaginas the contenuto paginas
	 */
	public void cacheResult(java.util.List<ContenutoPagina> contenutoPaginas);

	/**
	 * Creates a new contenuto pagina with the primary key. Does not add the contenuto pagina to the database.
	 *
	 * @param contenutoPaginaId the primary key for the new contenuto pagina
	 * @return the new contenuto pagina
	 */
	public ContenutoPagina create(long contenutoPaginaId);

	/**
	 * Removes the contenuto pagina with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina that was removed
	 * @throws NoSuchContenutoPaginaException if a contenuto pagina with the primary key could not be found
	 */
	public ContenutoPagina remove(long contenutoPaginaId)
		throws NoSuchContenutoPaginaException;

	public ContenutoPagina updateImpl(ContenutoPagina contenutoPagina);

	/**
	 * Returns the contenuto pagina with the primary key or throws a <code>NoSuchContenutoPaginaException</code> if it could not be found.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina
	 * @throws NoSuchContenutoPaginaException if a contenuto pagina with the primary key could not be found
	 */
	public ContenutoPagina findByPrimaryKey(long contenutoPaginaId)
		throws NoSuchContenutoPaginaException;

	/**
	 * Returns the contenuto pagina with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina, or <code>null</code> if a contenuto pagina with the primary key could not be found
	 */
	public ContenutoPagina fetchByPrimaryKey(long contenutoPaginaId);

	/**
	 * Returns all the contenuto paginas.
	 *
	 * @return the contenuto paginas
	 */
	public java.util.List<ContenutoPagina> findAll();

	/**
	 * Returns a range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @return the range of contenuto paginas
	 */
	public java.util.List<ContenutoPagina> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of contenuto paginas
	 */
	public java.util.List<ContenutoPagina> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContenutoPagina>
			orderByComparator);

	/**
	 * Returns an ordered range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of contenuto paginas
	 */
	public java.util.List<ContenutoPagina> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContenutoPagina>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Removes all the contenuto paginas from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of contenuto paginas.
	 *
	 * @return the number of contenuto paginas
	 */
	public int countAll();

}