/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AlberaturaTrasparenzaLocalService}.
 *
 * @author Simone Sorgon
 * @see AlberaturaTrasparenzaLocalService
 * @generated
 */
@ProviderType
public class AlberaturaTrasparenzaLocalServiceWrapper
	implements AlberaturaTrasparenzaLocalService,
			   ServiceWrapper<AlberaturaTrasparenzaLocalService> {

	public AlberaturaTrasparenzaLocalServiceWrapper(
		AlberaturaTrasparenzaLocalService alberaturaTrasparenzaLocalService) {

		_alberaturaTrasparenzaLocalService = alberaturaTrasparenzaLocalService;
	}

	/**
	 * Adds the alberatura trasparenza to the database. Also notifies the appropriate model listeners.
	 *
	 * @param alberaturaTrasparenza the alberatura trasparenza
	 * @return the alberatura trasparenza that was added
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
		addAlberaturaTrasparenza(
			it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
				alberaturaTrasparenza) {

		return _alberaturaTrasparenzaLocalService.addAlberaturaTrasparenza(
			alberaturaTrasparenza);
	}

	/**
	 * Creates a new alberatura trasparenza with the primary key. Does not add the alberatura trasparenza to the database.
	 *
	 * @param codicePagina the primary key for the new alberatura trasparenza
	 * @return the new alberatura trasparenza
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
		createAlberaturaTrasparenza(long codicePagina) {

		return _alberaturaTrasparenzaLocalService.createAlberaturaTrasparenza(
			codicePagina);
	}

	/**
	 * Deletes the alberatura trasparenza from the database. Also notifies the appropriate model listeners.
	 *
	 * @param alberaturaTrasparenza the alberatura trasparenza
	 * @return the alberatura trasparenza that was removed
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
		deleteAlberaturaTrasparenza(
			it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
				alberaturaTrasparenza) {

		return _alberaturaTrasparenzaLocalService.deleteAlberaturaTrasparenza(
			alberaturaTrasparenza);
	}

	/**
	 * Deletes the alberatura trasparenza with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza that was removed
	 * @throws PortalException if a alberatura trasparenza with the primary key could not be found
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
			deleteAlberaturaTrasparenza(long codicePagina)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _alberaturaTrasparenzaLocalService.deleteAlberaturaTrasparenza(
			codicePagina);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _alberaturaTrasparenzaLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _alberaturaTrasparenzaLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _alberaturaTrasparenzaLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _alberaturaTrasparenzaLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _alberaturaTrasparenzaLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _alberaturaTrasparenzaLocalService.dynamicQueryCount(
			dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _alberaturaTrasparenzaLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
		fetchAlberaturaTrasparenza(long codicePagina) {

		return _alberaturaTrasparenzaLocalService.fetchAlberaturaTrasparenza(
			codicePagina);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _alberaturaTrasparenzaLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the alberatura trasparenza with the primary key.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza
	 * @throws PortalException if a alberatura trasparenza with the primary key could not be found
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
			getAlberaturaTrasparenza(long codicePagina)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _alberaturaTrasparenzaLocalService.getAlberaturaTrasparenza(
			codicePagina);
	}

	/**
	 * Returns a range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @return the range of alberatura trasparenzas
	 */
	@Override
	public java.util.List
		<it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza>
			getAlberaturaTrasparenzas(int start, int end) {

		return _alberaturaTrasparenzaLocalService.getAlberaturaTrasparenzas(
			start, end);
	}

	/**
	 * Returns the number of alberatura trasparenzas.
	 *
	 * @return the number of alberatura trasparenzas
	 */
	@Override
	public int getAlberaturaTrasparenzasCount() {
		return _alberaturaTrasparenzaLocalService.
			getAlberaturaTrasparenzasCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _alberaturaTrasparenzaLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _alberaturaTrasparenzaLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _alberaturaTrasparenzaLocalService.getPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Updates the alberatura trasparenza in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param alberaturaTrasparenza the alberatura trasparenza
	 * @return the alberatura trasparenza that was updated
	 */
	@Override
	public it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
		updateAlberaturaTrasparenza(
			it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
				alberaturaTrasparenza) {

		return _alberaturaTrasparenzaLocalService.updateAlberaturaTrasparenza(
			alberaturaTrasparenza);
	}

	@Override
	public AlberaturaTrasparenzaLocalService getWrappedService() {
		return _alberaturaTrasparenzaLocalService;
	}

	@Override
	public void setWrappedService(
		AlberaturaTrasparenzaLocalService alberaturaTrasparenzaLocalService) {

		_alberaturaTrasparenzaLocalService = alberaturaTrasparenzaLocalService;
	}

	private AlberaturaTrasparenzaLocalService
		_alberaturaTrasparenzaLocalService;

}