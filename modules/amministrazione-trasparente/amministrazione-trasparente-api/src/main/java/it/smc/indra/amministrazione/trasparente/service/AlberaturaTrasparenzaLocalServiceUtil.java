/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for AlberaturaTrasparenza. This utility wraps
 * <code>it.smc.indra.amministrazione.trasparente.service.impl.AlberaturaTrasparenzaLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Simone Sorgon
 * @see AlberaturaTrasparenzaLocalService
 * @generated
 */
@ProviderType
public class AlberaturaTrasparenzaLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>it.smc.indra.amministrazione.trasparente.service.impl.AlberaturaTrasparenzaLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the alberatura trasparenza to the database. Also notifies the appropriate model listeners.
	 *
	 * @param alberaturaTrasparenza the alberatura trasparenza
	 * @return the alberatura trasparenza that was added
	 */
	public static
		it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
			addAlberaturaTrasparenza(
				it.smc.indra.amministrazione.trasparente.model.
					AlberaturaTrasparenza alberaturaTrasparenza) {

		return getService().addAlberaturaTrasparenza(alberaturaTrasparenza);
	}

	/**
	 * Creates a new alberatura trasparenza with the primary key. Does not add the alberatura trasparenza to the database.
	 *
	 * @param codicePagina the primary key for the new alberatura trasparenza
	 * @return the new alberatura trasparenza
	 */
	public static
		it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
			createAlberaturaTrasparenza(long codicePagina) {

		return getService().createAlberaturaTrasparenza(codicePagina);
	}

	/**
	 * Deletes the alberatura trasparenza from the database. Also notifies the appropriate model listeners.
	 *
	 * @param alberaturaTrasparenza the alberatura trasparenza
	 * @return the alberatura trasparenza that was removed
	 */
	public static
		it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
			deleteAlberaturaTrasparenza(
				it.smc.indra.amministrazione.trasparente.model.
					AlberaturaTrasparenza alberaturaTrasparenza) {

		return getService().deleteAlberaturaTrasparenza(alberaturaTrasparenza);
	}

	/**
	 * Deletes the alberatura trasparenza with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza that was removed
	 * @throws PortalException if a alberatura trasparenza with the primary key could not be found
	 */
	public static
		it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
				deleteAlberaturaTrasparenza(long codicePagina)
			throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteAlberaturaTrasparenza(codicePagina);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static
		it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
			fetchAlberaturaTrasparenza(long codicePagina) {

		return getService().fetchAlberaturaTrasparenza(codicePagina);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the alberatura trasparenza with the primary key.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza
	 * @throws PortalException if a alberatura trasparenza with the primary key could not be found
	 */
	public static
		it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
				getAlberaturaTrasparenza(long codicePagina)
			throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getAlberaturaTrasparenza(codicePagina);
	}

	/**
	 * Returns a range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>it.smc.indra.amministrazione.trasparente.model.impl.AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @return the range of alberatura trasparenzas
	 */
	public static java.util.List
		<it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza>
			getAlberaturaTrasparenzas(int start, int end) {

		return getService().getAlberaturaTrasparenzas(start, end);
	}

	/**
	 * Returns the number of alberatura trasparenzas.
	 *
	 * @return the number of alberatura trasparenzas
	 */
	public static int getAlberaturaTrasparenzasCount() {
		return getService().getAlberaturaTrasparenzasCount();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the alberatura trasparenza in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param alberaturaTrasparenza the alberatura trasparenza
	 * @return the alberatura trasparenza that was updated
	 */
	public static
		it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza
			updateAlberaturaTrasparenza(
				it.smc.indra.amministrazione.trasparente.model.
					AlberaturaTrasparenza alberaturaTrasparenza) {

		return getService().updateAlberaturaTrasparenza(alberaturaTrasparenza);
	}

	public static AlberaturaTrasparenzaLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<AlberaturaTrasparenzaLocalService, AlberaturaTrasparenzaLocalService>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			AlberaturaTrasparenzaLocalService.class);

		ServiceTracker
			<AlberaturaTrasparenzaLocalService,
			 AlberaturaTrasparenzaLocalService> serviceTracker =
				new ServiceTracker
					<AlberaturaTrasparenzaLocalService,
					 AlberaturaTrasparenzaLocalService>(
						 bundle.getBundleContext(),
						 AlberaturaTrasparenzaLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}