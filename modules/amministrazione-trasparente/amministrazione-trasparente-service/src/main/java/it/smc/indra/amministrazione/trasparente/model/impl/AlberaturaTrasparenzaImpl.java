/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */

package it.smc.indra.amministrazione.trasparente.model.impl;

import aQute.bnd.annotation.ProviderType;

/**
 * The extended model implementation for the AlberaturaTrasparenza service. Represents a row in the &quot;bk_alberaturatrasparenza&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza<code> interface.
 * </p>
 *
 * @author Simone Sorgon
 */
@ProviderType
public class AlberaturaTrasparenzaImpl extends AlberaturaTrasparenzaBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a alberatura trasparenza model instance should use the {@link it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza} interface instead.
	 */
	public AlberaturaTrasparenzaImpl() {
	}

}