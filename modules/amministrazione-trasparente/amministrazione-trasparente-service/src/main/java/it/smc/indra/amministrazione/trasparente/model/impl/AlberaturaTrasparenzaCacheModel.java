/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing AlberaturaTrasparenza in entity cache.
 *
 * @author Simone Sorgon
 * @generated
 */
@ProviderType
public class AlberaturaTrasparenzaCacheModel
	implements CacheModel<AlberaturaTrasparenza>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AlberaturaTrasparenzaCacheModel)) {
			return false;
		}

		AlberaturaTrasparenzaCacheModel alberaturaTrasparenzaCacheModel =
			(AlberaturaTrasparenzaCacheModel)obj;

		if (codicePagina == alberaturaTrasparenzaCacheModel.codicePagina) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, codicePagina);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{codicePagina=");
		sb.append(codicePagina);
		sb.append(", codicePaginaPadre=");
		sb.append(codicePaginaPadre);
		sb.append(", descrizionePagina=");
		sb.append(descrizionePagina);
		sb.append(", titoloPagina=");
		sb.append(titoloPagina);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AlberaturaTrasparenza toEntityModel() {
		AlberaturaTrasparenzaImpl alberaturaTrasparenzaImpl =
			new AlberaturaTrasparenzaImpl();

		alberaturaTrasparenzaImpl.setCodicePagina(codicePagina);
		alberaturaTrasparenzaImpl.setCodicePaginaPadre(codicePaginaPadre);

		if (descrizionePagina == null) {
			alberaturaTrasparenzaImpl.setDescrizionePagina("");
		}
		else {
			alberaturaTrasparenzaImpl.setDescrizionePagina(descrizionePagina);
		}

		if (titoloPagina == null) {
			alberaturaTrasparenzaImpl.setTitoloPagina("");
		}
		else {
			alberaturaTrasparenzaImpl.setTitoloPagina(titoloPagina);
		}

		alberaturaTrasparenzaImpl.resetOriginalValues();

		return alberaturaTrasparenzaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codicePagina = objectInput.readLong();

		codicePaginaPadre = objectInput.readLong();
		descrizionePagina = objectInput.readUTF();
		titoloPagina = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(codicePagina);

		objectOutput.writeLong(codicePaginaPadre);

		if (descrizionePagina == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(descrizionePagina);
		}

		if (titoloPagina == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(titoloPagina);
		}
	}

	public long codicePagina;
	public long codicePaginaPadre;
	public String descrizionePagina;
	public String titoloPagina;

}