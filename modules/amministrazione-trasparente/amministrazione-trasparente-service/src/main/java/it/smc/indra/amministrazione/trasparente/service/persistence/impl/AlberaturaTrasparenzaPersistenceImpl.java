/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.spring.extender.service.ServiceReference;

import it.smc.indra.amministrazione.trasparente.exception.NoSuchAlberaturaTrasparenzaException;
import it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza;
import it.smc.indra.amministrazione.trasparente.model.impl.AlberaturaTrasparenzaImpl;
import it.smc.indra.amministrazione.trasparente.model.impl.AlberaturaTrasparenzaModelImpl;
import it.smc.indra.amministrazione.trasparente.service.persistence.AlberaturaTrasparenzaPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the alberatura trasparenza service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Simone Sorgon
 * @generated
 */
@ProviderType
public class AlberaturaTrasparenzaPersistenceImpl
	extends BasePersistenceImpl<AlberaturaTrasparenza>
	implements AlberaturaTrasparenzaPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>AlberaturaTrasparenzaUtil</code> to access the alberatura trasparenza persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		AlberaturaTrasparenzaImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;

	public AlberaturaTrasparenzaPersistenceImpl() {
		setModelClass(AlberaturaTrasparenza.class);
	}

	/**
	 * Caches the alberatura trasparenza in the entity cache if it is enabled.
	 *
	 * @param alberaturaTrasparenza the alberatura trasparenza
	 */
	@Override
	public void cacheResult(AlberaturaTrasparenza alberaturaTrasparenza) {
		entityCache.putResult(
			AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
			AlberaturaTrasparenzaImpl.class,
			alberaturaTrasparenza.getPrimaryKey(), alberaturaTrasparenza);

		alberaturaTrasparenza.resetOriginalValues();
	}

	/**
	 * Caches the alberatura trasparenzas in the entity cache if it is enabled.
	 *
	 * @param alberaturaTrasparenzas the alberatura trasparenzas
	 */
	@Override
	public void cacheResult(
		List<AlberaturaTrasparenza> alberaturaTrasparenzas) {

		for (AlberaturaTrasparenza alberaturaTrasparenza :
				alberaturaTrasparenzas) {

			if (entityCache.getResult(
					AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
					AlberaturaTrasparenzaImpl.class,
					alberaturaTrasparenza.getPrimaryKey()) == null) {

				cacheResult(alberaturaTrasparenza);
			}
			else {
				alberaturaTrasparenza.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all alberatura trasparenzas.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(AlberaturaTrasparenzaImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the alberatura trasparenza.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AlberaturaTrasparenza alberaturaTrasparenza) {
		entityCache.removeResult(
			AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
			AlberaturaTrasparenzaImpl.class,
			alberaturaTrasparenza.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AlberaturaTrasparenza> alberaturaTrasparenzas) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AlberaturaTrasparenza alberaturaTrasparenza :
				alberaturaTrasparenzas) {

			entityCache.removeResult(
				AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
				AlberaturaTrasparenzaImpl.class,
				alberaturaTrasparenza.getPrimaryKey());
		}
	}

	/**
	 * Creates a new alberatura trasparenza with the primary key. Does not add the alberatura trasparenza to the database.
	 *
	 * @param codicePagina the primary key for the new alberatura trasparenza
	 * @return the new alberatura trasparenza
	 */
	@Override
	public AlberaturaTrasparenza create(long codicePagina) {
		AlberaturaTrasparenza alberaturaTrasparenza =
			new AlberaturaTrasparenzaImpl();

		alberaturaTrasparenza.setNew(true);
		alberaturaTrasparenza.setPrimaryKey(codicePagina);

		return alberaturaTrasparenza;
	}

	/**
	 * Removes the alberatura trasparenza with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza that was removed
	 * @throws NoSuchAlberaturaTrasparenzaException if a alberatura trasparenza with the primary key could not be found
	 */
	@Override
	public AlberaturaTrasparenza remove(long codicePagina)
		throws NoSuchAlberaturaTrasparenzaException {

		return remove((Serializable)codicePagina);
	}

	/**
	 * Removes the alberatura trasparenza with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza that was removed
	 * @throws NoSuchAlberaturaTrasparenzaException if a alberatura trasparenza with the primary key could not be found
	 */
	@Override
	public AlberaturaTrasparenza remove(Serializable primaryKey)
		throws NoSuchAlberaturaTrasparenzaException {

		Session session = null;

		try {
			session = openSession();

			AlberaturaTrasparenza alberaturaTrasparenza =
				(AlberaturaTrasparenza)session.get(
					AlberaturaTrasparenzaImpl.class, primaryKey);

			if (alberaturaTrasparenza == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAlberaturaTrasparenzaException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(alberaturaTrasparenza);
		}
		catch (NoSuchAlberaturaTrasparenzaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AlberaturaTrasparenza removeImpl(
		AlberaturaTrasparenza alberaturaTrasparenza) {

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(alberaturaTrasparenza)) {
				alberaturaTrasparenza = (AlberaturaTrasparenza)session.get(
					AlberaturaTrasparenzaImpl.class,
					alberaturaTrasparenza.getPrimaryKeyObj());
			}

			if (alberaturaTrasparenza != null) {
				session.delete(alberaturaTrasparenza);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (alberaturaTrasparenza != null) {
			clearCache(alberaturaTrasparenza);
		}

		return alberaturaTrasparenza;
	}

	@Override
	public AlberaturaTrasparenza updateImpl(
		AlberaturaTrasparenza alberaturaTrasparenza) {

		boolean isNew = alberaturaTrasparenza.isNew();

		Session session = null;

		try {
			session = openSession();

			if (alberaturaTrasparenza.isNew()) {
				session.save(alberaturaTrasparenza);

				alberaturaTrasparenza.setNew(false);
			}
			else {
				alberaturaTrasparenza = (AlberaturaTrasparenza)session.merge(
					alberaturaTrasparenza);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(
			AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
			AlberaturaTrasparenzaImpl.class,
			alberaturaTrasparenza.getPrimaryKey(), alberaturaTrasparenza,
			false);

		alberaturaTrasparenza.resetOriginalValues();

		return alberaturaTrasparenza;
	}

	/**
	 * Returns the alberatura trasparenza with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza
	 * @throws NoSuchAlberaturaTrasparenzaException if a alberatura trasparenza with the primary key could not be found
	 */
	@Override
	public AlberaturaTrasparenza findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAlberaturaTrasparenzaException {

		AlberaturaTrasparenza alberaturaTrasparenza = fetchByPrimaryKey(
			primaryKey);

		if (alberaturaTrasparenza == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAlberaturaTrasparenzaException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return alberaturaTrasparenza;
	}

	/**
	 * Returns the alberatura trasparenza with the primary key or throws a <code>NoSuchAlberaturaTrasparenzaException</code> if it could not be found.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza
	 * @throws NoSuchAlberaturaTrasparenzaException if a alberatura trasparenza with the primary key could not be found
	 */
	@Override
	public AlberaturaTrasparenza findByPrimaryKey(long codicePagina)
		throws NoSuchAlberaturaTrasparenzaException {

		return findByPrimaryKey((Serializable)codicePagina);
	}

	/**
	 * Returns the alberatura trasparenza with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza, or <code>null</code> if a alberatura trasparenza with the primary key could not be found
	 */
	@Override
	public AlberaturaTrasparenza fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(
			AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
			AlberaturaTrasparenzaImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		AlberaturaTrasparenza alberaturaTrasparenza =
			(AlberaturaTrasparenza)serializable;

		if (alberaturaTrasparenza == null) {
			Session session = null;

			try {
				session = openSession();

				alberaturaTrasparenza = (AlberaturaTrasparenza)session.get(
					AlberaturaTrasparenzaImpl.class, primaryKey);

				if (alberaturaTrasparenza != null) {
					cacheResult(alberaturaTrasparenza);
				}
				else {
					entityCache.putResult(
						AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
						AlberaturaTrasparenzaImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(
					AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
					AlberaturaTrasparenzaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return alberaturaTrasparenza;
	}

	/**
	 * Returns the alberatura trasparenza with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param codicePagina the primary key of the alberatura trasparenza
	 * @return the alberatura trasparenza, or <code>null</code> if a alberatura trasparenza with the primary key could not be found
	 */
	@Override
	public AlberaturaTrasparenza fetchByPrimaryKey(long codicePagina) {
		return fetchByPrimaryKey((Serializable)codicePagina);
	}

	@Override
	public Map<Serializable, AlberaturaTrasparenza> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, AlberaturaTrasparenza> map =
			new HashMap<Serializable, AlberaturaTrasparenza>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			AlberaturaTrasparenza alberaturaTrasparenza = fetchByPrimaryKey(
				primaryKey);

			if (alberaturaTrasparenza != null) {
				map.put(primaryKey, alberaturaTrasparenza);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(
				AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
				AlberaturaTrasparenzaImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (AlberaturaTrasparenza)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler(
			uncachedPrimaryKeys.size() * 2 + 1);

		query.append(_SQL_SELECT_ALBERATURATRASPARENZA_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (AlberaturaTrasparenza alberaturaTrasparenza :
					(List<AlberaturaTrasparenza>)q.list()) {

				map.put(
					alberaturaTrasparenza.getPrimaryKeyObj(),
					alberaturaTrasparenza);

				cacheResult(alberaturaTrasparenza);

				uncachedPrimaryKeys.remove(
					alberaturaTrasparenza.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(
					AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
					AlberaturaTrasparenzaImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the alberatura trasparenzas.
	 *
	 * @return the alberatura trasparenzas
	 */
	@Override
	public List<AlberaturaTrasparenza> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @return the range of alberatura trasparenzas
	 */
	@Override
	public List<AlberaturaTrasparenza> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of alberatura trasparenzas
	 */
	@Override
	public List<AlberaturaTrasparenza> findAll(
		int start, int end,
		OrderByComparator<AlberaturaTrasparenza> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the alberatura trasparenzas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>AlberaturaTrasparenzaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of alberatura trasparenzas
	 * @param end the upper bound of the range of alberatura trasparenzas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of alberatura trasparenzas
	 */
	@Override
	public List<AlberaturaTrasparenza> findAll(
		int start, int end,
		OrderByComparator<AlberaturaTrasparenza> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<AlberaturaTrasparenza> list = null;

		if (retrieveFromCache) {
			list = (List<AlberaturaTrasparenza>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_ALBERATURATRASPARENZA);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ALBERATURATRASPARENZA;

				if (pagination) {
					sql = sql.concat(
						AlberaturaTrasparenzaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<AlberaturaTrasparenza>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<AlberaturaTrasparenza>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the alberatura trasparenzas from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (AlberaturaTrasparenza alberaturaTrasparenza : findAll()) {
			remove(alberaturaTrasparenza);
		}
	}

	/**
	 * Returns the number of alberatura trasparenzas.
	 *
	 * @return the number of alberatura trasparenzas
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ALBERATURATRASPARENZA);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return AlberaturaTrasparenzaModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the alberatura trasparenza persistence.
	 */
	public void afterPropertiesSet() {
		_finderPathWithPaginationFindAll = new FinderPath(
			AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
			AlberaturaTrasparenzaModelImpl.FINDER_CACHE_ENABLED,
			AlberaturaTrasparenzaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
			AlberaturaTrasparenzaModelImpl.FINDER_CACHE_ENABLED,
			AlberaturaTrasparenzaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			AlberaturaTrasparenzaModelImpl.ENTITY_CACHE_ENABLED,
			AlberaturaTrasparenzaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	}

	public void destroy() {
		entityCache.removeCache(AlberaturaTrasparenzaImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;

	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_ALBERATURATRASPARENZA =
		"SELECT alberaturaTrasparenza FROM AlberaturaTrasparenza alberaturaTrasparenza";

	private static final String _SQL_SELECT_ALBERATURATRASPARENZA_WHERE_PKS_IN =
		"SELECT alberaturaTrasparenza FROM AlberaturaTrasparenza alberaturaTrasparenza WHERE codicePagina IN (";

	private static final String _SQL_COUNT_ALBERATURATRASPARENZA =
		"SELECT COUNT(alberaturaTrasparenza) FROM AlberaturaTrasparenza alberaturaTrasparenza";

	private static final String _ORDER_BY_ENTITY_ALIAS =
		"alberaturaTrasparenza.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No AlberaturaTrasparenza exists with the primary key ";

	private static final Log _log = LogFactoryUtil.getLog(
		AlberaturaTrasparenzaPersistenceImpl.class);

}