/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */
package it.smc.indra.amministrazione.trasparente.gogo.shell.command.activator;

import com.liferay.osgi.util.ServiceTrackerFactory;

import it.smc.indra.amministrazione.trasparente.gogo.shell.command.GogoShellCommand;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 * @author Simone Sorgon
 */
public class GogoShellCommandActivator implements BundleActivator {

	// TODO change System.out.println with logs

	@Override
	public void start(BundleContext bundleContext) throws Exception {
		System.out.println("Starting bundle " + bundleContext.getBundle());

		ServiceTracker<GogoShellCommand, GogoShellCommand> serviceTracker =
			ServiceTrackerFactory.open(GogoShellCommand.class);

		GogoShellCommand gogoShellMessage = serviceTracker.getService();

		serviceTracker.close();
	}

	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		System.out.println("Stopping bundle " + bundleContext.getBundle());
	}

}
