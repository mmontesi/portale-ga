/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */
package it.smc.indra.amministrazione.trasparente.background.task;

import java.io.File;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.exception.NoSuchVocabularyException;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetCategoryConstants;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.document.library.kernel.exception.FileNameException;
import com.liferay.document.library.kernel.exception.NoSuchFileEntryException;
import com.liferay.document.library.kernel.exception.NoSuchFolderException;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.portal.kernel.backgroundtask.BackgroundTask;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskConstants;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskResult;
import com.liferay.portal.kernel.backgroundtask.BaseBackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.display.BackgroundTaskDisplay;
import com.liferay.portal.kernel.dao.jdbc.DataAccess;
import com.liferay.portal.kernel.dao.jdbc.DataSourceFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

import it.smc.indra.amministrazione.trasparente.model.AlberaturaTrasparenza;
import it.smc.indra.amministrazione.trasparente.service.AlberaturaTrasparenzaLocalService;
import it.smc.indra.amministrazione.trasparente.service.ContenutoPaginaLocalService;
import it.smc.indra.amministrazione.trasparente.service.internal.util.PortletPropsValues;


/**
 * @author Simone Sorgon
 * @author Luca Comin
 */
@Component(
	immediate = true,
	property = "background.task.executor.class.name=" +
		"it.smc.indra.amministrazione.trasparente.background.task." +
		"ImportDocumentBackgroundTaskExecutor",
	service = BackgroundTaskExecutor.class
)
public class ImportDocumentBackgroundTaskExecutor
	extends BaseBackgroundTaskExecutor{

	@Override
	public BackgroundTaskExecutor clone() {
		return this;
	}

	public static  String getBackgroundTaskName() {
		return _BACKGROUND_TASK_NAME_PREFIX;
	}

	@Override
	public BackgroundTaskResult execute(
			BackgroundTask backgroundTask)
		throws Exception {

		long userId = backgroundTask.getUserId();
		long companyId = backgroundTask.getCompanyId();
		long groupId = backgroundTask.getGroupId();
		
		Map<String, Serializable> taskContextMap = backgroundTask.getTaskContextMap();
		long start = (long) taskContextMap.get("start");
		long end = (long) taskContextMap.get("end");

		_importDocument(userId,companyId,groupId, start, end);

		int processed = 0;

		BackgroundTaskResult backgroundTaskResult = new BackgroundTaskResult(
			BackgroundTaskConstants.STATUS_SUCCESSFUL);

		backgroundTaskResult.setStatus(
			BackgroundTaskResult.SUCCESS.getStatus());

		_log.info("Import Document terminato");

		if (_log.isDebugEnabled()) {
			_log.debug(
				"Report job completed, processed " + processed +
				" documents");
		}

		return backgroundTaskResult;
	}

	@Override
	public BackgroundTaskDisplay getBackgroundTaskDisplay(
		BackgroundTask backgroundTask) {

		return null;
	}

	private void _importDocument(long userId, long companyId, long groupId,
			long start, long end)
		throws Exception {

		String driverClassName = PropsUtil.get("jdbc.ext.driverClassName");
		String url = PropsUtil.get("jdbc.ext.url");
		String userName = PropsUtil.get("jdbc.ext.username");
		String password = PropsUtil.get("jdbc.ext.password");

		String jndiName = "";

		DataSource dataSource = DataSourceFactoryUtil.initDataSource(
			driverClassName, url, userName, password, jndiName);

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		ServiceContext serviceContext = new ServiceContext();

		serviceContext.setCompanyId(companyId);
		serviceContext.setUserId(userId);
		serviceContext.setScopeGroupId(groupId);
		serviceContext.setAddGuestPermissions(true);

		AssetVocabulary area = _getAssetVocabulary(
			userId, groupId, PortletPropsValues.VOCABOLARY_AREA_NAME,
			serviceContext);

		AssetVocabulary page = _getAssetVocabulary(
			userId, groupId, PortletPropsValues.VOCABOLARY_PAGE_NAME,
			serviceContext);

		AssetVocabulary location = _getAssetVocabulary(
				userId, groupId, PortletPropsValues.VOCABOLARY_LOCATION_NAME,
				serviceContext);
		
		List<String> locations = new ArrayList<String>();
		if (Validator.isNotNull(PortletPropsValues.LOCATIONS)) {
			locations = ListUtil.fromArray(StringUtil.split(PortletPropsValues.LOCATIONS));
		}

		try {
			connection = dataSource.getConnection();
			
			String queryString = "SELECT * FROM bk_contenutopagina WHERE document_blob is not null";

			if (start >= 0 && end >= 0 && end >= start) {
				queryString = 
					"SELECT * FROM bk_contenutopagina where document_blob is not null and contenutoPaginaId >= "
					+ start + " and contenutoPaginaId <= " + end;
			}

			_log.info("Query string: " + queryString);
			preparedStatement = connection.prepareStatement(queryString);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				long codicePagina = resultSet.getLong("codicePagina");
				long contenutoPaginaId = resultSet.getLong("contenutoPaginaId");
				String nomeFile = resultSet.getString("nomeFile");
				String descrizioneFile = resultSet.getString("descrizioneFile");
				String mimeType = resultSet.getString("tipoFile");
				String placeholder = resultSet.getString("placeholder");
				Date dataAllegato = resultSet.getDate("dataAllegato");
				File tempFile;
				_log.info("-------------" + contenutoPaginaId + " - " + nomeFile + "-----------");


				if(Validator.isNull(nomeFile)) {
					String etichettaPrincipale = resultSet.getString(
						"etichettaPrincipale");

					nomeFile = etichettaPrincipale;

				}

				if (nomeFile.contains("/")) {
					nomeFile = nomeFile.replaceAll("/", StringPool.UNDERLINE);
				}

				if (nomeFile.contains("\\")) {
					nomeFile = nomeFile.replaceAll("\\", StringPool.UNDERLINE);
				}

				if (nomeFile.contains("&")) {
					nomeFile = nomeFile.replaceAll("&", StringPool.UNDERLINE);
				}

				if (nomeFile.contains("<")) {
					nomeFile = nomeFile.replaceAll("<", StringPool.UNDERLINE);
				}

				if (nomeFile.contains(">")) {
					nomeFile = nomeFile.replaceAll(">", StringPool.UNDERLINE);
				}

				if (nomeFile.contains("?")) {
					nomeFile = nomeFile.replaceAll("?", StringPool.UNDERLINE);
				}

				if (nomeFile.contains("|")) {
					nomeFile = nomeFile.replaceAll("|", StringPool.UNDERLINE);
				}

				if (Validator.isNull(
					resultSet.getBinaryStream("document_blob"))) {

					tempFile = null;
				}
				else {
					tempFile = FileUtil.createTempFile(
						resultSet.getBinaryStream("document_blob"));
				}

				if (Validator.isNull(nomeFile)) {
					nomeFile = StringPool.BLANK;
				}

				if (Validator.isNull(mimeType)) {
					mimeType = StringPool.BLANK;
				}

				List<AlberaturaTrasparenza> tree = _getAlberatura(codicePagina);

				Folder folder = _createFolderTree(tree, serviceContext);

				_storeDocument(
					tree, folder, mimeType, nomeFile, descrizioneFile,
					dataAllegato, tempFile, area, page, location, locations,
					serviceContext, contenutoPaginaId,placeholder);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally {
			DataAccess.cleanUp(connection, preparedStatement, resultSet);
		}
	}

	private Folder _createFolderTree(
			List<AlberaturaTrasparenza> tree, ServiceContext serviceContext)
		throws PortalException {

		Folder rootFolder = _getFolder(
			serviceContext.getUserId(), serviceContext.getScopeGroupId(),
			DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
			PortletPropsValues.FOLDER_NAME, serviceContext);

		long parentFolderId = rootFolder.getFolderId();
		Folder lastFolder = rootFolder;

		for (AlberaturaTrasparenza nodo : tree) {
			String folderName = nodo.getTitoloPagina();

			if (folderName.contains("/")) {
				folderName = folderName.replaceAll("/", StringPool.UNDERLINE);
			}

			if (folderName.contains("\\")) {
				folderName = folderName.replaceAll("\\", StringPool.UNDERLINE);
			}

			if (folderName.contains("&")) {
				folderName = folderName.replaceAll("&", StringPool.UNDERLINE);
			}

			if (folderName.contains("<")) {
				folderName = folderName.replaceAll("<", StringPool.UNDERLINE);
			}

			if (folderName.contains(">")) {
				folderName = folderName.replaceAll(">", StringPool.UNDERLINE);
			}

			if (folderName.contains("?")) {
				folderName = folderName.replaceAll("?", StringPool.UNDERLINE);
			}

			if (folderName.contains("|")) {
				folderName = folderName.replaceAll("|", StringPool.UNDERLINE);
			}

			if (folderName.contains(".")) {
				folderName = folderName.replaceAll("\\.", StringPool.UNDERLINE);
			}

			folderName = folderName.trim();
			Folder folder = _getFolder(
				serviceContext.getUserId(), serviceContext.getScopeGroupId(),
				parentFolderId, folderName, serviceContext);

			parentFolderId = folder.getFolderId();
			lastFolder = folder;
		}

		return lastFolder;
	}

	private List<AlberaturaTrasparenza> _getAlberatura(long codicePagina)
		throws PortalException {

		List<AlberaturaTrasparenza> tree = new ArrayList<>();

		long currentCode = codicePagina;

		while (currentCode > 0) {
			AlberaturaTrasparenza alberatura =
				_alberaturaTrasparenzaLocalServiceLocalService
					.getAlberaturaTrasparenza(currentCode);

			tree.add(0, alberatura);

			currentCode = alberatura.getCodicePaginaPadre();
		}

		return tree;
	}

	private AssetCategory _getAssetCategory(long userId, long groupId,
			String title, long vocabularyId, ServiceContext serviceContext)
		throws PortalException {
		
		title = title.trim();

		AssetCategory assetCategory = _assetCategoryLocalService.fetchCategory(
			groupId, AssetCategoryConstants.DEFAULT_PARENT_CATEGORY_ID,
			title, vocabularyId);


		if (assetCategory == null) {
			assetCategory = _assetCategoryLocalService.addCategory(
				userId, groupId, title, vocabularyId,
				serviceContext);

			_log.info(
				"Categoria creata con questo nome = " + StringPool.BLANK +
				title);
		}
		else {
			_log.info(
					"Categoria " + StringPool.BLANK + title + StringPool.BLANK +
					" gia' presente");
		}

		return assetCategory;
	}

	private AssetVocabulary _getAssetVocabulary(
			long userId, long groupId, String name,
			ServiceContext serviceContext)
		throws PortalException {

		name = name.trim();

		AssetVocabulary assetVocabulary;

		try {
			assetVocabulary = _assetVocabularyLocalService.getGroupVocabulary(
				groupId, name);

			_log.info(
				"Vocabolario " + StringPool.BLANK + name + StringPool.BLANK +
				" esiste gia'");
		}
		catch (NoSuchVocabularyException e) {
			assetVocabulary = _assetVocabularyLocalService.addVocabulary(
				userId, groupId, name, serviceContext);

			_log.info(
				"Vocabolario creato con questo nome = " + StringPool.BLANK +
				name);
		}

		return assetVocabulary;
	}

	private Folder _getFolder(
			long userId, long groupId, long parentFolderId, String folderName,
			ServiceContext serviceContext)
		throws PortalException {

		Folder folder = null;

		try {
			folder = _dlAppLocalService.getFolder(
				groupId, parentFolderId, folderName);

			_log.info(
				"Cartella " + StringPool.BLANK + folderName + StringPool.BLANK +
				" esiste gia'");
		}
		catch (NoSuchFolderException nsfe) {
			folder = _dlAppLocalService.addFolder(
				userId, groupId, parentFolderId, folderName,
				folderName, serviceContext);

			_log.info(
				"Cartella creata con questo nome = " + StringPool.BLANK +
				folderName);
		}

		return folder;
	}

	private void _storeDocument(
			List<AlberaturaTrasparenza> tree, Folder folder, String mimeType,
			String sourceFileName, String sourceFileDescription,
			Date dataAllegato, File documentFile, AssetVocabulary area,
			AssetVocabulary page, AssetVocabulary location,
			List<String> locations, ServiceContext serviceContext,
			long contenutoPaginaId, String placeholder) throws PortalException{

		long[] categoryIds;

		if (tree != null && tree.size() > 0) {
			AlberaturaTrasparenza primoNodo = tree.get(0);

			AssetCategory acArea = _getAssetCategory(
				serviceContext.getUserId(), serviceContext.getScopeGroupId(),
				primoNodo.getTitoloPagina(), area.getVocabularyId(), serviceContext);
			
			categoryIds = new long[] { acArea.getCategoryId()};
			if (tree.size() > 1) {
				for (int i = 1; i < tree.size(); i++) {
					AlberaturaTrasparenza nodo = tree.get(i);
					
					AssetCategory acPage = _getAssetCategory(
							serviceContext.getUserId(), serviceContext.getScopeGroupId(),
							nodo.getTitoloPagina(), page.getVocabularyId(), serviceContext);
					
					
					categoryIds = ArrayUtil.append(categoryIds, acPage.getCategoryId());
					
				}
				AlberaturaTrasparenza ultimoNodo = tree.get(tree.size() - 1);
				/* Aggiungo la sede */
				if (Validator.isNotNull(locations) && ListUtil.isNotEmpty(locations)) {
					if (locations.contains(ultimoNodo.getTitoloPagina().trim())) {
						AssetCategory acLocation = _getAssetCategory(
								serviceContext.getUserId(), serviceContext.getScopeGroupId(),
								ultimoNodo.getTitoloPagina(), location.getVocabularyId(), serviceContext);
						
						categoryIds = ArrayUtil.append(categoryIds, acLocation.getCategoryId());
					}
				}
			}
		}
		else {
			categoryIds = new long[0];
		}

		String[] assetTagNames = new String[]{"amministrazione_trasparente"};

		serviceContext.setAssetTagNames(assetTagNames);
		serviceContext.setAssetCategoryIds(categoryIds);
		serviceContext.setCreateDate(dataAllegato);

		try {
			boolean found = false;
			FileEntry file = null;
			int i=1;

			while (found == false) {
				if (i == 1) {
					try {
						file = DLAppLocalServiceUtil.getFileEntry(
							serviceContext.getScopeGroupId(),
							folder.getFolderId(), sourceFileName);
					}
					catch (NoSuchFileEntryException e) {
						if (Validator.isNull(file)) {
							DLAppLocalServiceUtil.addFileEntry(
								serviceContext.getUserId(),
								serviceContext.getScopeGroupId(),
								folder.getFolderId(),  sourceFileName, mimeType,
								sourceFileName, sourceFileDescription,
								StringPool.BLANK, documentFile, serviceContext);

							found = true;
						}
					}

					i = i + 1;
				}
				else {
					FileEntry fileCopy = null;
					String nameTest =
						sourceFileName + StringPool.BLANK +
							StringUtil.valueOf(i);

					try {
						fileCopy = DLAppLocalServiceUtil.getFileEntry(
							serviceContext.getScopeGroupId(),
							folder.getFolderId(), nameTest);
					}
					catch (NoSuchFileEntryException e) {
						if (Validator.isNull(fileCopy)) {
							DLAppLocalServiceUtil.addFileEntry(
								serviceContext.getUserId(),
								serviceContext.getScopeGroupId(),
								folder.getFolderId(), sourceFileName, mimeType,
								nameTest, sourceFileDescription, StringPool.BLANK,
								documentFile, serviceContext);

							found = true;
						}
					}

					i = i + 1;
				}
			}

			_log.info(
				"Documento " + StringPool.BLANK + sourceFileName +
				StringPool.BLANK + " creato con successo");

		}
		catch(FileNameException e1) {

			String nameTest =
				placeholder + StringPool.BLANK +
					StringUtil.valueOf(contenutoPaginaId);

			DLAppLocalServiceUtil.addFileEntry(
					serviceContext.getUserId(),
					serviceContext.getScopeGroupId(),
					folder.getFolderId(), nameTest, mimeType,
					nameTest, StringPool.BLANK, StringPool.BLANK,
					documentFile, serviceContext);
		}
		catch (PortalException e) {
			_log.info(
				"Il documento non è stato creato causa " + StringPool.BLANK +
				e);
		}
	}

	private static final Log _log = LogFactoryUtil.getLog(
		ImportDocumentBackgroundTaskExecutor.class);

	private static final String _BACKGROUND_TASK_NAME_PREFIX =
		"ImportDocumentTaskExecutor-";

	@Reference
	private AssetCategoryLocalService _assetCategoryLocalService;

	@Reference
	private AssetEntryLocalService _assetEntryLocalService;

	@Reference
	private AssetVocabularyLocalService _assetVocabularyLocalService;

	@Reference
	private ContenutoPaginaLocalService _contenutoPaginaLocalService;

	@Reference
	private AlberaturaTrasparenzaLocalService
		_alberaturaTrasparenzaLocalServiceLocalService;

	@Reference
	private DLAppLocalService _dlAppLocalService;

	@Reference
	private DLFolderLocalService _dlFolderLocalService;

	@Reference
	private DLFileEntryLocalService _dlFileEntryLocalService;

}