/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.spring.extender.service.ServiceReference;

import it.smc.indra.amministrazione.trasparente.exception.NoSuchContenutoPaginaException;
import it.smc.indra.amministrazione.trasparente.model.ContenutoPagina;
import it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaImpl;
import it.smc.indra.amministrazione.trasparente.model.impl.ContenutoPaginaModelImpl;
import it.smc.indra.amministrazione.trasparente.service.persistence.ContenutoPaginaPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the contenuto pagina service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Simone Sorgon
 * @generated
 */
@ProviderType
public class ContenutoPaginaPersistenceImpl
	extends BasePersistenceImpl<ContenutoPagina>
	implements ContenutoPaginaPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>ContenutoPaginaUtil</code> to access the contenuto pagina persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		ContenutoPaginaImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;

	public ContenutoPaginaPersistenceImpl() {
		setModelClass(ContenutoPagina.class);
	}

	/**
	 * Caches the contenuto pagina in the entity cache if it is enabled.
	 *
	 * @param contenutoPagina the contenuto pagina
	 */
	@Override
	public void cacheResult(ContenutoPagina contenutoPagina) {
		entityCache.putResult(
			ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
			ContenutoPaginaImpl.class, contenutoPagina.getPrimaryKey(),
			contenutoPagina);

		contenutoPagina.resetOriginalValues();
	}

	/**
	 * Caches the contenuto paginas in the entity cache if it is enabled.
	 *
	 * @param contenutoPaginas the contenuto paginas
	 */
	@Override
	public void cacheResult(List<ContenutoPagina> contenutoPaginas) {
		for (ContenutoPagina contenutoPagina : contenutoPaginas) {
			if (entityCache.getResult(
					ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
					ContenutoPaginaImpl.class,
					contenutoPagina.getPrimaryKey()) == null) {

				cacheResult(contenutoPagina);
			}
			else {
				contenutoPagina.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all contenuto paginas.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ContenutoPaginaImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the contenuto pagina.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ContenutoPagina contenutoPagina) {
		entityCache.removeResult(
			ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
			ContenutoPaginaImpl.class, contenutoPagina.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ContenutoPagina> contenutoPaginas) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ContenutoPagina contenutoPagina : contenutoPaginas) {
			entityCache.removeResult(
				ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
				ContenutoPaginaImpl.class, contenutoPagina.getPrimaryKey());
		}
	}

	/**
	 * Creates a new contenuto pagina with the primary key. Does not add the contenuto pagina to the database.
	 *
	 * @param contenutoPaginaId the primary key for the new contenuto pagina
	 * @return the new contenuto pagina
	 */
	@Override
	public ContenutoPagina create(long contenutoPaginaId) {
		ContenutoPagina contenutoPagina = new ContenutoPaginaImpl();

		contenutoPagina.setNew(true);
		contenutoPagina.setPrimaryKey(contenutoPaginaId);

		return contenutoPagina;
	}

	/**
	 * Removes the contenuto pagina with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina that was removed
	 * @throws NoSuchContenutoPaginaException if a contenuto pagina with the primary key could not be found
	 */
	@Override
	public ContenutoPagina remove(long contenutoPaginaId)
		throws NoSuchContenutoPaginaException {

		return remove((Serializable)contenutoPaginaId);
	}

	/**
	 * Removes the contenuto pagina with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the contenuto pagina
	 * @return the contenuto pagina that was removed
	 * @throws NoSuchContenutoPaginaException if a contenuto pagina with the primary key could not be found
	 */
	@Override
	public ContenutoPagina remove(Serializable primaryKey)
		throws NoSuchContenutoPaginaException {

		Session session = null;

		try {
			session = openSession();

			ContenutoPagina contenutoPagina = (ContenutoPagina)session.get(
				ContenutoPaginaImpl.class, primaryKey);

			if (contenutoPagina == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchContenutoPaginaException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(contenutoPagina);
		}
		catch (NoSuchContenutoPaginaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ContenutoPagina removeImpl(ContenutoPagina contenutoPagina) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(contenutoPagina)) {
				contenutoPagina = (ContenutoPagina)session.get(
					ContenutoPaginaImpl.class,
					contenutoPagina.getPrimaryKeyObj());
			}

			if (contenutoPagina != null) {
				session.delete(contenutoPagina);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (contenutoPagina != null) {
			clearCache(contenutoPagina);
		}

		return contenutoPagina;
	}

	@Override
	public ContenutoPagina updateImpl(ContenutoPagina contenutoPagina) {
		boolean isNew = contenutoPagina.isNew();

		Session session = null;

		try {
			session = openSession();

			if (contenutoPagina.isNew()) {
				session.save(contenutoPagina);

				contenutoPagina.setNew(false);
			}
			else {
				contenutoPagina = (ContenutoPagina)session.merge(
					contenutoPagina);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(
			ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
			ContenutoPaginaImpl.class, contenutoPagina.getPrimaryKey(),
			contenutoPagina, false);

		contenutoPagina.resetOriginalValues();

		return contenutoPagina;
	}

	/**
	 * Returns the contenuto pagina with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the contenuto pagina
	 * @return the contenuto pagina
	 * @throws NoSuchContenutoPaginaException if a contenuto pagina with the primary key could not be found
	 */
	@Override
	public ContenutoPagina findByPrimaryKey(Serializable primaryKey)
		throws NoSuchContenutoPaginaException {

		ContenutoPagina contenutoPagina = fetchByPrimaryKey(primaryKey);

		if (contenutoPagina == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchContenutoPaginaException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return contenutoPagina;
	}

	/**
	 * Returns the contenuto pagina with the primary key or throws a <code>NoSuchContenutoPaginaException</code> if it could not be found.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina
	 * @throws NoSuchContenutoPaginaException if a contenuto pagina with the primary key could not be found
	 */
	@Override
	public ContenutoPagina findByPrimaryKey(long contenutoPaginaId)
		throws NoSuchContenutoPaginaException {

		return findByPrimaryKey((Serializable)contenutoPaginaId);
	}

	/**
	 * Returns the contenuto pagina with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the contenuto pagina
	 * @return the contenuto pagina, or <code>null</code> if a contenuto pagina with the primary key could not be found
	 */
	@Override
	public ContenutoPagina fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(
			ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
			ContenutoPaginaImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ContenutoPagina contenutoPagina = (ContenutoPagina)serializable;

		if (contenutoPagina == null) {
			Session session = null;

			try {
				session = openSession();

				contenutoPagina = (ContenutoPagina)session.get(
					ContenutoPaginaImpl.class, primaryKey);

				if (contenutoPagina != null) {
					cacheResult(contenutoPagina);
				}
				else {
					entityCache.putResult(
						ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
						ContenutoPaginaImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(
					ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
					ContenutoPaginaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return contenutoPagina;
	}

	/**
	 * Returns the contenuto pagina with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param contenutoPaginaId the primary key of the contenuto pagina
	 * @return the contenuto pagina, or <code>null</code> if a contenuto pagina with the primary key could not be found
	 */
	@Override
	public ContenutoPagina fetchByPrimaryKey(long contenutoPaginaId) {
		return fetchByPrimaryKey((Serializable)contenutoPaginaId);
	}

	@Override
	public Map<Serializable, ContenutoPagina> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ContenutoPagina> map =
			new HashMap<Serializable, ContenutoPagina>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ContenutoPagina contenutoPagina = fetchByPrimaryKey(primaryKey);

			if (contenutoPagina != null) {
				map.put(primaryKey, contenutoPagina);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(
				ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
				ContenutoPaginaImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ContenutoPagina)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler(
			uncachedPrimaryKeys.size() * 2 + 1);

		query.append(_SQL_SELECT_CONTENUTOPAGINA_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ContenutoPagina contenutoPagina :
					(List<ContenutoPagina>)q.list()) {

				map.put(contenutoPagina.getPrimaryKeyObj(), contenutoPagina);

				cacheResult(contenutoPagina);

				uncachedPrimaryKeys.remove(contenutoPagina.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(
					ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
					ContenutoPaginaImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the contenuto paginas.
	 *
	 * @return the contenuto paginas
	 */
	@Override
	public List<ContenutoPagina> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @return the range of contenuto paginas
	 */
	@Override
	public List<ContenutoPagina> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of contenuto paginas
	 */
	@Override
	public List<ContenutoPagina> findAll(
		int start, int end,
		OrderByComparator<ContenutoPagina> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the contenuto paginas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ContenutoPaginaModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of contenuto paginas
	 * @param end the upper bound of the range of contenuto paginas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of contenuto paginas
	 */
	@Override
	public List<ContenutoPagina> findAll(
		int start, int end,
		OrderByComparator<ContenutoPagina> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<ContenutoPagina> list = null;

		if (retrieveFromCache) {
			list = (List<ContenutoPagina>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CONTENUTOPAGINA);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CONTENUTOPAGINA;

				if (pagination) {
					sql = sql.concat(ContenutoPaginaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ContenutoPagina>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ContenutoPagina>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the contenuto paginas from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ContenutoPagina contenutoPagina : findAll()) {
			remove(contenutoPagina);
		}
	}

	/**
	 * Returns the number of contenuto paginas.
	 *
	 * @return the number of contenuto paginas
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CONTENUTOPAGINA);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ContenutoPaginaModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the contenuto pagina persistence.
	 */
	public void afterPropertiesSet() {
		_finderPathWithPaginationFindAll = new FinderPath(
			ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
			ContenutoPaginaModelImpl.FINDER_CACHE_ENABLED,
			ContenutoPaginaImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
			ContenutoPaginaModelImpl.FINDER_CACHE_ENABLED,
			ContenutoPaginaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			ContenutoPaginaModelImpl.ENTITY_CACHE_ENABLED,
			ContenutoPaginaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	}

	public void destroy() {
		entityCache.removeCache(ContenutoPaginaImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;

	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_CONTENUTOPAGINA =
		"SELECT contenutoPagina FROM ContenutoPagina contenutoPagina";

	private static final String _SQL_SELECT_CONTENUTOPAGINA_WHERE_PKS_IN =
		"SELECT contenutoPagina FROM ContenutoPagina contenutoPagina WHERE contenutoPaginaId IN (";

	private static final String _SQL_COUNT_CONTENUTOPAGINA =
		"SELECT COUNT(contenutoPagina) FROM ContenutoPagina contenutoPagina";

	private static final String _ORDER_BY_ENTITY_ALIAS = "contenutoPagina.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No ContenutoPagina exists with the primary key ";

	private static final Log _log = LogFactoryUtil.getLog(
		ContenutoPaginaPersistenceImpl.class);

}