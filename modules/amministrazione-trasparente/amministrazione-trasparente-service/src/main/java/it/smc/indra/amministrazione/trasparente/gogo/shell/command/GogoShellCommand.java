/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */
package it.smc.indra.amministrazione.trasparente.gogo.shell.command;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.backgroundtask.BackgroundTaskManager;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

import it.smc.indra.amministrazione.trasparente.background.task.ImportDocumentBackgroundTaskExecutor;
import it.smc.indra.amministrazione.trasparente.service.internal.util.PortletPropsValues;

/**
 * @author Simone Sorgon
 * @author Luca Comin
 */
@Component(
	immediate = true,
	property = {
		"osgi.command.function=importDocuments",
		"osgi.command.scope=indra",
	},
	service = Object.class
)
public class GogoShellCommand {
	
	public void importDocuments() throws PortalException {
		importDocuments(-1, -1);
	}

	public void importDocuments(long start, long end) throws PortalException {

		Map<String, Serializable> taskContextMap = new HashMap<>();

		taskContextMap.put("start", start);
		taskContextMap.put("end", end);

		ServiceContext serviceContext = new ServiceContext();

		String backgroundTaskName =
			ImportDocumentBackgroundTaskExecutor.getBackgroundTaskName();

		Group group = GroupLocalServiceUtil.getGroup(
			PortletPropsValues.COMPANY_ID, PortletPropsValues.SITE_NAME);

		long groupId = group.getGroupId();
		long userId = 0;

		Role role = RoleLocalServiceUtil.getRole(
			PortletPropsValues.COMPANY_ID,RoleConstants.ADMINISTRATOR);

		for (final User admin : UserLocalServiceUtil.getRoleUsers(
			role.getRoleId())) {

			userId = admin.getUserId();
		}

		backgroundTaskmanager.addBackgroundTask(
			userId, groupId, backgroundTaskName,
				ImportDocumentBackgroundTaskExecutor.class.getName(),
					taskContextMap, serviceContext);
	}

	@Reference
	protected BackgroundTaskManager backgroundTaskmanager;

}