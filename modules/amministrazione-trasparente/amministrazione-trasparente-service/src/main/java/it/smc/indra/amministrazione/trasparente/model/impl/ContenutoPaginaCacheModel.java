/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.smc.indra.amministrazione.trasparente.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import it.smc.indra.amministrazione.trasparente.model.ContenutoPagina;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ContenutoPagina in entity cache.
 *
 * @author Simone Sorgon
 * @generated
 */
@ProviderType
public class ContenutoPaginaCacheModel
	implements CacheModel<ContenutoPagina>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ContenutoPaginaCacheModel)) {
			return false;
		}

		ContenutoPaginaCacheModel contenutoPaginaCacheModel =
			(ContenutoPaginaCacheModel)obj;

		if (contenutoPaginaId == contenutoPaginaCacheModel.contenutoPaginaId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, contenutoPaginaId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{contenutoPaginaId=");
		sb.append(contenutoPaginaId);
		sb.append(", codicePagina=");
		sb.append(codicePagina);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", placeholder=");
		sb.append(placeholder);
		sb.append(", document_blob=");
		sb.append(document_blob);
		sb.append(", descrizioneFile=");
		sb.append(descrizioneFile);
		sb.append(", tipoFile=");
		sb.append(tipoFile);
		sb.append(", nomeFile=");
		sb.append(nomeFile);
		sb.append(", dataAllegato=");
		sb.append(dataAllegato);
		sb.append(", dataEtichettaPrincipale=");
		sb.append(dataEtichettaPrincipale);
		sb.append(", dataAltreProp=");
		sb.append(dataAltreProp);
		sb.append(", etichettaPrincipale=");
		sb.append(etichettaPrincipale);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ContenutoPagina toEntityModel() {
		ContenutoPaginaImpl contenutoPaginaImpl = new ContenutoPaginaImpl();

		contenutoPaginaImpl.setContenutoPaginaId(contenutoPaginaId);
		contenutoPaginaImpl.setCodicePagina(codicePagina);

		if (descrizione == null) {
			contenutoPaginaImpl.setDescrizione("");
		}
		else {
			contenutoPaginaImpl.setDescrizione(descrizione);
		}

		if (placeholder == null) {
			contenutoPaginaImpl.setPlaceholder("");
		}
		else {
			contenutoPaginaImpl.setPlaceholder(placeholder);
		}

		if (document_blob == null) {
			contenutoPaginaImpl.setDocument_blob("");
		}
		else {
			contenutoPaginaImpl.setDocument_blob(document_blob);
		}

		if (descrizioneFile == null) {
			contenutoPaginaImpl.setDescrizioneFile("");
		}
		else {
			contenutoPaginaImpl.setDescrizioneFile(descrizioneFile);
		}

		if (tipoFile == null) {
			contenutoPaginaImpl.setTipoFile("");
		}
		else {
			contenutoPaginaImpl.setTipoFile(tipoFile);
		}

		if (nomeFile == null) {
			contenutoPaginaImpl.setNomeFile("");
		}
		else {
			contenutoPaginaImpl.setNomeFile(nomeFile);
		}

		if (dataAllegato == Long.MIN_VALUE) {
			contenutoPaginaImpl.setDataAllegato(null);
		}
		else {
			contenutoPaginaImpl.setDataAllegato(new Date(dataAllegato));
		}

		if (dataEtichettaPrincipale == Long.MIN_VALUE) {
			contenutoPaginaImpl.setDataEtichettaPrincipale(null);
		}
		else {
			contenutoPaginaImpl.setDataEtichettaPrincipale(
				new Date(dataEtichettaPrincipale));
		}

		if (dataAltreProp == Long.MIN_VALUE) {
			contenutoPaginaImpl.setDataAltreProp(null);
		}
		else {
			contenutoPaginaImpl.setDataAltreProp(new Date(dataAltreProp));
		}

		if (etichettaPrincipale == null) {
			contenutoPaginaImpl.setEtichettaPrincipale("");
		}
		else {
			contenutoPaginaImpl.setEtichettaPrincipale(etichettaPrincipale);
		}

		contenutoPaginaImpl.resetOriginalValues();

		return contenutoPaginaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		contenutoPaginaId = objectInput.readLong();

		codicePagina = objectInput.readLong();
		descrizione = objectInput.readUTF();
		placeholder = objectInput.readUTF();
		document_blob = objectInput.readUTF();
		descrizioneFile = objectInput.readUTF();
		tipoFile = objectInput.readUTF();
		nomeFile = objectInput.readUTF();
		dataAllegato = objectInput.readLong();
		dataEtichettaPrincipale = objectInput.readLong();
		dataAltreProp = objectInput.readLong();
		etichettaPrincipale = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(contenutoPaginaId);

		objectOutput.writeLong(codicePagina);

		if (descrizione == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		if (placeholder == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(placeholder);
		}

		if (document_blob == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(document_blob);
		}

		if (descrizioneFile == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(descrizioneFile);
		}

		if (tipoFile == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(tipoFile);
		}

		if (nomeFile == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(nomeFile);
		}

		objectOutput.writeLong(dataAllegato);
		objectOutput.writeLong(dataEtichettaPrincipale);
		objectOutput.writeLong(dataAltreProp);

		if (etichettaPrincipale == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(etichettaPrincipale);
		}
	}

	public long contenutoPaginaId;
	public long codicePagina;
	public String descrizione;
	public String placeholder;
	public String document_blob;
	public String descrizioneFile;
	public String tipoFile;
	public String nomeFile;
	public long dataAllegato;
	public long dataEtichettaPrincipale;
	public long dataAltreProp;
	public String etichettaPrincipale;

}