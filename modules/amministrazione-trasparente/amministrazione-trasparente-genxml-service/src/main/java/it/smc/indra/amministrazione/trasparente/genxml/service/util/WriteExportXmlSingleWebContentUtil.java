/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */

package it.smc.indra.amministrazione.trasparente.genxml.service.util;

import com.liferay.document.library.kernel.exception.NoSuchFileEntryException;
import com.liferay.document.library.kernel.exception.NoSuchFolderException;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * @author Simone Sorgon
 */
public class WriteExportXmlSingleWebContentUtil {

	public static void addContentInXml(
			String oggetto, String cig,
			List<com.liferay.portal.kernel.xml.Node> strutturaProponente,
			String proceduraSceltaContraente,
			List<com.liferay.portal.kernel.xml.Node>
				elencoOperatoriPartecipanti,
			List<com.liferay.portal.kernel.xml.Node> aggiudicatari,
			String importoDiAggiudicazioneAlNetto,
			String importoDelleSommeLiquidateAlNettoIVA,
			List<com.liferay.portal.kernel.xml.Node> date, String name,
			String nameCategory,
			Document document, File file, int number, long folderId,
			ServiceContext serviceContext)
		throws IOException, PortalException, TransformerException {

		Node root = document.getFirstChild();
		String destination = PortletPropsValues.PATH_DESTINATION;

		if (Validator.isNull(root)) {
			Element legge = XmlUtil.getTestata(
				document, PortletPropsValues.URL_TAG, name, false,nameCategory);

			Element data = document.createElement(Constants.DATA);

			legge.appendChild(data);

			Element lotto = document.createElement(Constants.LOTTO);

			data.appendChild(lotto);

			_insertInXml(
				document, lotto, true, oggetto, cig, strutturaProponente,
				elencoOperatoriPartecipanti, aggiudicatari,
				proceduraSceltaContraente, date, importoDiAggiudicazioneAlNetto,
				importoDelleSommeLiquidateAlNettoIVA, name, file, folderId,
				serviceContext);

			FileEntry fileCopy = null;

			if (number == 1) {
				try {
					fileCopy = DLAppLocalServiceUtil.getFileEntry(
						serviceContext.getScopeGroupId(), folderId, name);
				}
				catch (NoSuchFileEntryException | NoSuchFolderException e) {
					fileCopy = null;

					_log.info("No file esistente= "+e+ ".Lo creo");
				}

				if (Validator.isNull(fileCopy)) {
					DLAppLocalServiceUtil.addFileEntry(
						serviceContext.getUserId(),
						serviceContext.getScopeGroupId(), folderId, name,
						Constants.XML, name, StringPool.BLANK, StringPool.BLANK,
						file, serviceContext);

				}
				else {
					_log.info("XML =" + name + ", gia' presente.Lo aggiorno!");

					DLAppLocalServiceUtil.updateFileEntry(
						serviceContext.getUserId(), fileCopy.getFileEntryId(),
						name, Constants.XML, name, StringPool.BLANK,
						StringPool.BLANK, false, file, serviceContext);

				}
				
				XmlUtil.createOnFS(file, name);
			}
		}
		else {
			Node data = root.getLastChild();

			Element lotto = document.createElement(Constants.LOTTO);

			data.appendChild(lotto);

			_insertInXml(
				document, lotto, false, oggetto, cig, strutturaProponente,
				elencoOperatoriPartecipanti, aggiudicatari,
				proceduraSceltaContraente, date, importoDiAggiudicazioneAlNetto,
				importoDelleSommeLiquidateAlNettoIVA, name, file, folderId,
				serviceContext);
		}
	}

	private static void _insertInXml(
			Document document, Element lotto, boolean step, String oggetto,
			String cig,
			List<com.liferay.portal.kernel.xml.Node> strutturaProponente,
			List<com.liferay.portal.kernel.xml.Node>
				elencoOperatoriPartecipanti,
			List<com.liferay.portal.kernel.xml.Node> aggiudicatariList,
			String proceduraSceltaContraente,
			List<com.liferay.portal.kernel.xml.Node> date,
			String importoDiAggiudicazioneAlNetto,
			String importoDelleSommeLiquidateAlNettoIVA, String name, File file,
			long folderId, ServiceContext serviceContext)
		throws PortalException, TransformerException {

		com.liferay.portal.kernel.xml.Node currentNode = null;
		List<String> ragioniSociali = new ArrayList<>();
		List<String> codiciFiscali = new ArrayList<>();

		for (com.liferay.portal.kernel.xml.Node node :
				elencoOperatoriPartecipanti) {

			currentNode = node.selectSingleNode(
				node.getUniquePath() + Constants.NAME_RAGIONESOCIALE);

			ragioniSociali.add(currentNode.getStringValue());

			currentNode = node.selectSingleNode(
				node.getUniquePath() + Constants.NAME_CODICEFISCALE);

			codiciFiscali.add(currentNode.getStringValue());
		}

		List<String> ragioniSocialiAgg = new ArrayList<>();
		List<String> codiciFiscaliAgg = new ArrayList<>();

		for (com.liferay.portal.kernel.xml.Node node : aggiudicatariList) {
			currentNode = node.selectSingleNode(
				node.getUniquePath() + Constants.NAME_AGGIUDICATARIO_RAGIONE);

			ragioniSocialiAgg.add(currentNode.getStringValue());

			currentNode = node.selectSingleNode(
				node.getUniquePath() + Constants.NAME_AGGIUDICATARIO_CODICE);

			codiciFiscaliAgg.add(currentNode.getStringValue());
		}

		Element cigElement = document.createElement(Constants.CIG);

		cigElement.appendChild(document.createTextNode(cig));

		lotto.appendChild(cigElement);

		List<String> strutturaProponenteList = new ArrayList<>();

		currentNode = strutturaProponente.get(0);
		strutturaProponenteList.add(currentNode.getStringValue());

		currentNode = strutturaProponente.get(1);
		strutturaProponenteList.add(currentNode.getStringValue());

		Element strutturaProponenteElement = document.createElement(
			Constants.STRUTTURA_PROPONENTE);

		lotto.appendChild(strutturaProponenteElement);

		Element denominazione = document.createElement(Constants.DENOMINAZIONE);

		denominazione.appendChild(
			document.createTextNode(strutturaProponenteList.get(0)));

		strutturaProponenteElement.appendChild(denominazione);
		Element codiceFiscaleProp = document.createElement(
			Constants.CODICE_FISCALEPROP);

		codiceFiscaleProp.appendChild(
			document.createTextNode(strutturaProponenteList.get(1)));

		strutturaProponenteElement.appendChild(codiceFiscaleProp);

		lotto.appendChild(strutturaProponenteElement);

		Element oggettoElement = document.createElement(Constants.OGGETTO);

		oggettoElement.appendChild(document.createTextNode(oggetto));
		lotto.appendChild(oggettoElement);

		Element partecipanti = document.createElement(Constants.PARTECIPANTI);

		lotto.appendChild(partecipanti);

		int x = 0;

		for (String i : ragioniSociali) {
			for (int j = x; j < codiciFiscali.size(); ++j) {
				Element partecipante = document.createElement(
					Constants.PARTECIPANTE);

				partecipanti.appendChild(partecipante);

				Element codiceFiscale = document.createElement(
					Constants.CODICE_FISCALE);

				codiceFiscale.appendChild(
					document.createTextNode(codiciFiscali.get(j)));

				partecipante.appendChild(codiceFiscale);

				Element ragioneSociale = document.createElement(
					Constants.RAGIONE_SOCIALE);

				ragioneSociale.appendChild(document.createTextNode(i));

				partecipante.appendChild(ragioneSociale);

				partecipanti.appendChild(partecipante);

				lotto.appendChild(partecipanti);
				x = x + 1;

				break;
			}
		}

		Element aggiudicatari = document.createElement(Constants.AGGIUDICATARI);

		lotto.appendChild(aggiudicatari);

		int z = 0;

		for (String i : ragioniSocialiAgg) {
			for (int y = z; y < codiciFiscaliAgg.size(); ++y) {
				Element aggiudicatario = document.createElement(
					Constants.AGGIUDICATARIO);

				aggiudicatari.appendChild(aggiudicatario);

				Element codiceFiscale = document.createElement(
					Constants.CODICE_FISCALE);

				codiceFiscale.appendChild(
					document.createTextNode(codiciFiscaliAgg.get(y)));

				aggiudicatario.appendChild(codiceFiscale);

				Element ragioneSociale = document.createElement(
					Constants.RAGIONE_SOCIALE);

				ragioneSociale.appendChild(document.createTextNode(i));
				aggiudicatario.appendChild(ragioneSociale);

				aggiudicatari.appendChild(aggiudicatario);

				lotto.appendChild(aggiudicatari);
				z = z + 1;

				break;
			}
		}

		List<String> dateToString = new ArrayList<>();

		currentNode = date.get(
			0
		).selectSingleNode(
			date.get(
				0
			).getUniquePath() + Constants.DATA_INIZIO_NAME
		);

		dateToString.add(currentNode.getStringValue());

		currentNode = date.get(
			1
		).selectSingleNode(
			date.get(
				1
			).getUniquePath() + Constants.DATA_FINE_NAME
		);

		dateToString.add(currentNode.getStringValue());

		Element proceduraSceltaContraenteElement = document.createElement(
			Constants.SCELTA_CONTRAENTE);

		proceduraSceltaContraenteElement.appendChild(
			document.createTextNode(proceduraSceltaContraente));

		lotto.appendChild(proceduraSceltaContraenteElement);

		Element importoAggiudicazioneNettoElement = document.createElement(
			Constants.IMPORTO_AGG);

		importoAggiudicazioneNettoElement.appendChild(
			document.createTextNode(importoDiAggiudicazioneAlNetto));

		lotto.appendChild(importoAggiudicazioneNettoElement);

		Element tempiCompletamento = document.createElement(
			Constants.TEMPI_COMPLETAMENTO);

		lotto.appendChild(tempiCompletamento);

		Element dataInizio = document.createElement(Constants.DATA_IN);

		dataInizio.appendChild(document.createTextNode(dateToString.get(0)));
		tempiCompletamento.appendChild(dataInizio);
		Element dataCompletamento = document.createElement(Constants.DATA_COM);

		dataCompletamento.appendChild(
			document.createTextNode(dateToString.get(1)));
		tempiCompletamento.appendChild(dataCompletamento);

		lotto.appendChild(tempiCompletamento);

		Element importoSommeLiquidateNettoIVA = document.createElement(
			Constants.IMPORTO_SOMME_LIQ);

		importoSommeLiquidateNettoIVA.appendChild(
			document.createTextNode(importoDelleSommeLiquidateAlNettoIVA));
		lotto.appendChild(importoSommeLiquidateNettoIVA);

		TransformerFactory transformerFactory =
			TransformerFactory.newInstance();

		Transformer transformer = transformerFactory.newTransformer();

		DOMSource domSource = new DOMSource(document);

		StreamResult streamResult = new StreamResult(file);

		transformer.transform(domSource, streamResult);
		FileEntry fileCopy = null;

		try {
			fileCopy = DLAppLocalServiceUtil.getFileEntry(
				serviceContext.getScopeGroupId(), folderId, name);
		}
		catch (NoSuchFileEntryException | NoSuchFolderException e) {
			fileCopy = null;

			_log.info("No file esistente= "+e+ ".Lo creo");
		}

		if (!step) {
			if (Validator.isNull(fileCopy)) {
				DLAppLocalServiceUtil.addFileEntry(
					serviceContext.getUserId(),
					serviceContext.getScopeGroupId(), folderId, name,
					Constants.XML, name, StringPool.BLANK, StringPool.BLANK,
					file, serviceContext);

			}
			else {
				DLAppLocalServiceUtil.updateFileEntry(
					serviceContext.getUserId(), fileCopy.getFileEntryId(), name,
					Constants.XML, name, StringPool.BLANK, StringPool.BLANK,
					false, file, serviceContext);

				_log.info(
					"XML=" + name + ",gia' presente.Lo aggiorno sulla DML!");

			}
			try {
				XmlUtil.createOnFS(file, name);
			}
			catch(Exception ex) {
				_log.error(ex);
			}
		}
	}


	private static final Log _log = LogFactoryUtil.getLog(
		WriteExportXmlGenericWebContentUtil.class);

}