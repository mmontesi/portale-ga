/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */

package it.smc.indra.amministrazione.trasparente.genxml.service.util;

import com.liferay.portal.kernel.util.GetterUtil;

/**
 * @author Simone Sorgon
 */
public class PortletPropsValues {

	public static final long COMPANY_ID = GetterUtil.getLong(
		PortletPropsUtil.get(PortletPropsKeys.COMPANY_ID));

	public static final String FOLDER_NAME = GetterUtil.getString(
		PortletPropsUtil.get(PortletPropsKeys.FOLDER_NAME));

	public static final String PATH_DESTINATION = GetterUtil.getString(
		PortletPropsUtil.get(PortletPropsKeys.PATH_DESTINATION));

	public static final String SITE_NAME = GetterUtil.getString(
		PortletPropsUtil.get(PortletPropsKeys.SITE_NAME));

	public static final String URL_TAG = GetterUtil.getString(
		PortletPropsUtil.get(PortletPropsKeys.URL_TAG));

	public static final String VOCABOLARY_NAME = GetterUtil.getString(
		PortletPropsUtil.get(PortletPropsKeys.VOCABOLARY_NAME));

}