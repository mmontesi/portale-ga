/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */

package it.smc.indra.amministrazione.trasparente.genxml.service.util;

public class Constants {

	// ogni giorno all'1

	public static final String ABSTRACT = "abstract";

	// ogni 2 gennaio alle 3

	public static final String AGGIUDICATARI = "aggiudicatari";

	public static final String AGGIUDICATARIO = "aggiudicatario";

	public static final String ANNO = "anno";

	public static final String ANNO_RIF = "annoRiferimento";

	public static final String CDSCOMPLETO = "CdsCompleto";

	public static final String CHRONJOBDAY = "0 0 1 * * ? *";

	public static final String CHRONJOBYEAR = "0 0 3 2 1 ? *";

	public static final String CIG = "cig";

	public static final String CODICE_CIG = "codiceCIG";

	public static final String CODICE_FISCALE = "codiceFiscale";

	public static final String CODICE_FISCALEPROP = "codiceFiscaleProp";

	public static final String DATA = "data";

	public static final String DATA_COM = "dataCompletamento";

	public static final String DATA_FINE_NAME =
		"[@name='" +
			"tempiDiCompletamentoDiServizioOFornitura_dataFine']/dynamic-content";

	public static final String DATA_IN = "dataInizio";

	public static final String DATA_INIZIO_NAME =
		"[@name='" +
			"tempiDiCompletamentoDiServizioOFornitura_dataInizio']/dynamic-content";

	public static final String DATA_PUBB_SET = "dataPubblicazioneDataset";

	public static final String DATA_ULTIMO_AGGI =
		"dataUltimoAggiornamentoDataset";

	public static final String DENOMINAZIONE = "denominazione";

	public static final String ENTE = "entePubblicatore";

	public static final String IMPORTO_AGG = "importoAggiudicazione";

	public static final String IMPORTO_AGGIUDICAZIONE =
		"importoDiAggiudicazioneAlNetto";

	public static final String IMPORTO_SOMME =
		"importoDelleSommeLiquidateAlNettoIVA";

	public static final String IMPORTO_SOMME_LIQ = "importoSommeLiquidate";

	public static final String IODL = "IODL";

	public static final String LEGGE190 = "legge190:pubblicazione";

	public static final String LEGGE190_XMLSN = "legge190_1_0";

	public static final String LEGGE190_XMLSN_TAG = "xmlns:legge190";

	public static final String LICENZA = "licenza";

	public static final String LOTTO = "lotto";

	public static final String METADATA = "metadata";

	public static final String NAME_AGGIUDICATARIO_CODICE =
		"/dynamic-element" + "[@name='" +
			"aggiudicatario_ragioneSociale_codiceFiscale']/dynamic-content";

	public static final String NAME_AGGIUDICATARIO_RAGIONE =
		"[@name='" + "aggiudicatario_ragioneSociale']/dynamic-content";

	public static final String NAME_CODICEFISCALE =
		"/dynamic-element" + "[@name='" +
			"elencoOperatoriPartecipanti_ragioneSociale_codiceFiscale']/dynamic-content";

	public static final String NAME_RAGIONESOCIALE =
		"[@name='" +
			"elencoOperatoriPartecipanti_ragioneSociale']/dynamic-content";

	public static final String OGGETTO = "oggetto";

	public static final String OPERATORI = "elencoOperatoriPartecipanti";

	public static final String PARTECIPANTE = "partecipante";

	public static final String PARTECIPANTI = "partecipanti";

	public static final String PROCEDURA = "proceduraSceltaContraente";

	public static final String PUBBLICAZIONE_LEGGE = "Pubblicazione legge 190";

	public static final String PUBBLICAZIONE_LEGGE_ANNO =
		"Pubblicazione legge 190 anno rif. 2017";

	public static final String RAGIONE_SOCIALE = "ragioneSociale";

	public static final String SCELTA_CONTRAENTE = "sceltaContraente";

	public static final String STRUTTURA = "strutturaProponenteDenominazione";

	public static final String STRUTTURA_PROPONENTE = "strutturaProponente";

	public static final String TEMPI =
		"tempiDiCompletamentoDiServizioOFornitura";

	public static final String TEMPI_COMPLETAMENTO = "tempiCompletamento";

	public static final String TITOLO = "titolo";

	public static final String UFFICIO_UNICO =
		"Ufficio unico contratti e risorse";

	public static final String URL_FILE = "urlFile";

	public static final String XML = "xml";

	public static final String XML_EXTENSION = ".xml";

	public static final String XMLNS_HTTP =
		"http://www.w3.org/2001/XMLSchema-instance";

	public static final String XMLNS_HTTP_TAG = "xmlns:xsi";

	public static final String XSI = "legge190_1_0 datasetAppaltiL190.xsd";

	public static final String XSI_TAG = "xsi:schemaLocation";

	public static final String test2 = "0 00 18 26 * ? *";
	public static final String test3 = "30 00 18 26 * ? *";

}