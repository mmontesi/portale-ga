/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */

package it.smc.indra.amministrazione.trasparente.genxml.service.scheduler;

import com.liferay.announcements.kernel.service.AnnouncementsEntryLocalService;
import com.liferay.asset.kernel.exception.NoSuchVocabularyException;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.AssetEntryService;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.document.library.kernel.exception.NoSuchFolderException;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleResource;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.journal.service.JournalArticleResourceLocalService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.scheduler.SchedulerEngineHelper;
import com.liferay.portal.kernel.scheduler.SchedulerEntry;
import com.liferay.portal.kernel.scheduler.SchedulerEntryImpl;
import com.liferay.portal.kernel.scheduler.Trigger;
import com.liferay.portal.kernel.scheduler.TriggerFactory;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import it.smc.indra.amministrazione.trasparente.genxml.service.util.Constants;
import it.smc.indra.amministrazione.trasparente.genxml.service.util.PortletPropsValues;
import it.smc.indra.amministrazione.trasparente.genxml.service.util.WriteExportXmlGenericWebContentUtil;
import it.smc.indra.amministrazione.trasparente.genxml.service.util.XmlUtil;

import java.io.File;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 *
 * @author Simone Sorgon
 *
 */
@Component(immediate = true, service = XmlExportSchedulerEveryYears.class)
public class XmlExportSchedulerEveryYears extends BaseMessageListener {

	@Activate
	protected void activate() {
		Class<?> clazz = getClass();
		String className = clazz.getName();

		Trigger trigger = _triggerFactory.createTrigger(
			className, className, null, null, Constants.test3);

		SchedulerEntry schedulerEntry = new SchedulerEntryImpl(
			className, trigger);

		_schedulerEngineHelper.register(
			this, schedulerEntry, DestinationNames.SCHEDULER_DISPATCH);
	}

	@Deactivate
	protected void deactivate() {
		_schedulerEngineHelper.unregister(this);
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		_exportDocumentMultiple();
	}

	@Reference(unbind = "-")
	protected void setAnnouncementsEntryLocalService(
		AnnouncementsEntryLocalService announcementsEntryLocalService) {

		_announcementsEntryLocalService = announcementsEntryLocalService;
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(
		ModuleServiceLifecycle moduleServiceLifecycle) {
	}

	@Reference(unbind = "-")
	protected void setSchedulerEngineHelper(
		SchedulerEngineHelper schedulerEngineHelper) {

		_schedulerEngineHelper = schedulerEngineHelper;
	}

	private void _exportDocumentMultiple() throws Exception {
		ServiceContext serviceContext = new ServiceContext();

		Group group = _groupLocalService.getGroup(
			PortletPropsValues.COMPANY_ID, PortletPropsValues.SITE_NAME);

		long groupId = group.getGroupId();
		long userId = 0;

		Role role = _roleLocalService.getRole(
			PortletPropsValues.COMPANY_ID, RoleConstants.ADMINISTRATOR);

		for (final User admin :
				_userLocalService.getRoleUsers(role.getRoleId())) {

			userId = admin.getUserId();
		}

		serviceContext.setScopeGroupId(groupId);
		serviceContext.setUserId(userId);

		//
		long classNameIdJournalArticle = _portal.getClassNameId(
			JournalArticle.class.getName());

		long[] classNameIdJA = new long[1];
		classNameIdJA[0] = classNameIdJournalArticle;

		List<AssetCategory> categories = null;

		AssetVocabulary assetVocabulary = _getAssetVocabulary(
			serviceContext.getUserId(), serviceContext.getScopeGroupId(),
			PortletPropsValues.VOCABOLARY_NAME, serviceContext);

		categories = assetVocabulary.getCategories();

		AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
		List<AssetEntry> results = null;
		List<String> webContentGeneral = new ArrayList<>();
		File file = FileUtil.createTempFile(Constants.XML);
		org.w3c.dom.Document d = XmlUtil.createXml();
		String name = StringPool.BLANK;

		if (Validator.isNotNull(categories)) {
			for (AssetCategory assetCategory : categories) {
				long[] catIds = new long[1];

				catIds[0] = assetCategory.getCategoryId();
				assetEntryQuery.setAnyCategoryIds(catIds);
				assetEntryQuery.setClassNameIds(classNameIdJA);
				assetEntryQuery.setExcludeZeroViewCount(false);
				assetEntryQuery.setOrderByCol1("publishDate");
				assetEntryQuery.setVisible(Boolean.TRUE);
				assetEntryQuery.setEnd(QueryUtil.ALL_POS);
				assetEntryQuery.setStart(QueryUtil.ALL_POS);
				results = _assetEntryLocalService.getEntries(assetEntryQuery);

				String nameCategory = assetCategory.getName();
				Date today = new Date();
				Calendar calendar = Calendar.getInstance();

				calendar.setTime(today);

				boolean testName = XmlUtil.controllName(nameCategory);

				int annoTitle = calendar.get(Calendar.YEAR) - 1;

				if (testName) {
					name =
						Constants.CDSCOMPLETO + StringPool.UNDERLINE +
							String.valueOf(annoTitle) + Constants.XML_EXTENSION;

					if (Validator.isNotNull(results)) {
						for (AssetEntry assetEntry : results) {
							JournalArticleResource journalArticleResource =
								_journalArticleResourceLocalService.
									getJournalArticleResource(
										assetEntry.getClassPK());

							JournalArticle journalArticle =
								_journalArticleLocalService.getLatestArticle(
									journalArticleResource.
										getResourcePrimKey());

							String content = journalArticle.getContent();

							com.liferay.portal.kernel.xml.Document doc =
								SAXReaderUtil.read(content);

							String anno = XmlUtil.getName(doc, Constants.ANNO);

							if (anno.equals(
									String.valueOf(
										calendar.get(Calendar.YEAR) - 1))) {

								webContentGeneral.add(content);
							}
						}
					}
				}
			}
		}

		Folder folder = _getFolder(
			serviceContext.getUserId(), serviceContext.getScopeGroupId(),
			DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
			PortletPropsValues.FOLDER_NAME, serviceContext);

		WriteExportXmlGenericWebContentUtil.addContentInXml(
			webContentGeneral, file, d, name,folder.getFolderId(),
			serviceContext);

		_log.info("Creato XML unico");
	}

	private AssetVocabulary _getAssetVocabulary(
			long userId, long groupId, String name,
			ServiceContext serviceContext)
		throws PortalException {

		AssetVocabulary assetVocabulary;

		try {
			assetVocabulary = _assetVocabularyLocalService.getGroupVocabulary(
				groupId, name);
		}
		catch (NoSuchVocabularyException nsve) {
			/*assetVocabulary = _assetVocabularyLocalService.addVocabulary(
				userId, groupId, name, serviceContext);
*/
			assetVocabulary = null;
			_log.info(
				"Vocabolario non trovato con questo nome = " + StringPool.BLANK +
					name);
		}

		return assetVocabulary;
	}

	private Folder _getFolder(
			long userId, long groupId, long parentFolderId, String folderName,
			ServiceContext serviceContext)
		throws PortalException {

		Folder folder = null;

		try {
			folder = _dlAppLocalService.getFolder(
				groupId, parentFolderId, folderName);
		}
		catch (NoSuchFolderException nsfe) {
			folder = _dlAppLocalService.addFolder(
				userId, groupId, parentFolderId, folderName, folderName,
				serviceContext);

			_log.info(
				"Cartella creata con questo nome = " + StringPool.BLANK +
					folderName);
		}

		return folder;
	}

	private static final Log _log = LogFactoryUtil.getLog(
		XmlExportSchedulerEveryYears.class);

	private AnnouncementsEntryLocalService _announcementsEntryLocalService;

	@Reference
	private AssetCategoryLocalService _assetCategoryLocalService;

	@Reference
	private AssetEntryLocalService _assetEntryLocalService;

	@Reference
	private AssetEntryService _assetEntryService;

	@Reference
	private AssetVocabularyLocalService _assetVocabularyLocalService;

	@Reference
	private DLAppLocalService _dlAppLocalService;

	@Reference
	private GroupLocalService _groupLocalService;

	@Reference
	private JournalArticleLocalService _journalArticleLocalService;

	@Reference
	private JournalArticleResourceLocalService
		_journalArticleResourceLocalService;

	@Reference
	private Portal _portal;

	@Reference
	private RoleLocalService _roleLocalService;

	private SchedulerEngineHelper _schedulerEngineHelper;

	@Reference
	private TriggerFactory _triggerFactory;

	@Reference
	private UserLocalService _userLocalService;

}