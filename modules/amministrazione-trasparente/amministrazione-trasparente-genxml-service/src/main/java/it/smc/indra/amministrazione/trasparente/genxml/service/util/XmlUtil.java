/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 *
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */

package it.smc.indra.amministrazione.trasparente.genxml.service.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;

import it.smc.indra.amministrazione.trasparente.genxml.service.scheduler.XmlExportSchedulerEveryYears;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Element;

/**
 * @author Simone Sorgon
 */
public class XmlUtil {

	public static boolean controllName(String name) {
		boolean test = false;
		String category1 = "Ufficio unico contratti e risorse";
		String category2 =
			"Ufficio gestione del bilancio e del trattamento economico";
		String category3 = "Segretariato generale";
		String category4 =
			"Servizio centrale per l'informatica e le tecnologie di comunicazione";
		String category5 =
			"Ufficio per la gestione delle risorse materiali e servizi generali";

		if (name.equalsIgnoreCase(category1) || name.equalsIgnoreCase(category2) ||
			name.equalsIgnoreCase(category3) || name.equalsIgnoreCase(category4) ||
			name.equalsIgnoreCase(category5)) {

			test = true;
		}
		else {
			test = false;
		}

		return test;
	}

	public static org.w3c.dom.Document createXml()
		throws ParserConfigurationException {

		DocumentBuilderFactory documentFactory =
			DocumentBuilderFactory.newInstance();

		DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

		org.w3c.dom.Document document = documentBuilder.newDocument();

		return document;
	}

	public static List<Node> getElenco(Document doc, String name) {
		String startNode = "/root/dynamic-element[@name='";
		String endNode = "']/dynamic-element";
		Node nodeRoot = null;

		nodeRoot = doc.selectSingleNode(startNode + name + endNode);

		List<Node> nodes = nodeRoot.selectNodes(startNode + name + endNode);

		return nodes;
	}

	public static String getName(Document doc, String name) {
		String startNode = "/root/dynamic-element[@name='";
		String endNode = "']/dynamic-content";
		Node node = null;

		node = doc.selectSingleNode(startNode + name + endNode);

		return node.getText();
	}

	public static String getNameFile(String name, String nameCategory,
		Calendar calendar) {

		_log.info(
			"Nome categoria = " + StringPool.BLANK +
				nameCategory);

		String category1 = "Ufficio unico contratti e risorse";
		String category2 =
			"Ufficio gestione del bilancio e del trattamento economico";
		String category3 = "Segretariato generale";
		String category4 =
			"Servizio centrale per l'informatica e le tecnologie di comunicazione";
		String category5 =
			"Ufficio per la gestione delle risorse materiali e servizi generali";
		String category6 =
				"Consiglio di Giustizia Amministrativa per la Regione Siciliana";

		String category7 =
				"Tar Emilia Romagna - Bologna";

		String category8 =
				"Tar Friuli Venezia Giulia - Trieste";

		String category9 =
				"Tar Emilia Romagna - Parma";

		String category10 =
				"Tar Valle D'Aosta - Aosta";

		if(nameCategory.equalsIgnoreCase(category7)){

			name =
				"TARERBO" + StringPool.UNDERLINE +
					String.valueOf(calendar.get(Calendar.YEAR)) + Constants.XML_EXTENSION;

		}else if(nameCategory.equalsIgnoreCase(category8)){

			name =
				"TARFVGTR" + StringPool.UNDERLINE +
					String.valueOf(calendar.get(Calendar.YEAR)) + Constants.XML_EXTENSION;

		}else if(nameCategory.equalsIgnoreCase(category9)){

			name =
				"TARERPA" + StringPool.UNDERLINE +
					String.valueOf(calendar.get(Calendar.YEAR)) + Constants.XML_EXTENSION;

		}else if(nameCategory.equalsIgnoreCase(category10)){

			name =
				"TARVAAO" + StringPool.UNDERLINE +
					String.valueOf(calendar.get(Calendar.YEAR)) + Constants.XML_EXTENSION;

		}
		else if(nameCategory.substring(0,3).equalsIgnoreCase("Tar")) {

			String[] array = nameCategory.split(" ");

			for (int i = 0; i < array.length; i++) {
				if (i == 0) {
					name = array[i];
				}
				else if (i != 2) {
					if (array[i].contains(StringPool.APOSTROPHE)) {
						name = name + array[i].substring(2, 4);
					}
					else {
						name = name + array[i].substring(0, 2);
					}
				}
			}

			name =
				name + StringPool.UNDERLINE +
					String.valueOf(calendar.get(Calendar.YEAR));

			name = name.toUpperCase() + Constants.XML_EXTENSION;
		}else {
			if(nameCategory.equalsIgnoreCase(category1)) {

				name =
					"TARUFCO" + StringPool.UNDERLINE +
						String.valueOf(calendar.get(Calendar.YEAR)) + Constants.XML_EXTENSION;

			}else if(nameCategory.equalsIgnoreCase(category2)){

				name =
					"CDSUGB" + StringPool.UNDERLINE +
						String.valueOf(calendar.get(Calendar.YEAR)) + Constants.XML_EXTENSION;

			}else if(nameCategory.equalsIgnoreCase(category3)){

				name =
					"CDSSGE" + StringPool.UNDERLINE +
						String.valueOf(calendar.get(Calendar.YEAR)) + Constants.XML_EXTENSION;

			}else if(nameCategory.equalsIgnoreCase(category4)){

				name =
					"CDSSEI" + StringPool.UNDERLINE +
						String.valueOf(calendar.get(Calendar.YEAR)) + Constants.XML_EXTENSION;

			}else if(nameCategory.equalsIgnoreCase(category5)){

				name =
					"TARGERIMA" + StringPool.UNDERLINE +
						String.valueOf(calendar.get(Calendar.YEAR)) + Constants.XML_EXTENSION;

			}else if(nameCategory.equalsIgnoreCase(category6)){

			name =
				"COGIAMSI" + StringPool.UNDERLINE +
					String.valueOf(calendar.get(Calendar.YEAR)) + Constants.XML_EXTENSION;

			}
		}

		return name;
	}

	public static List<Node> getStruttura(Document doc, String name) {
		String startNode = "/root/dynamic-element[@name='";
		String endNode = "']/dynamic-content";
		String endNode2 = "']/dynamic-element";
		Node nodeDenominazione = null;
		Node nodeCodiceFiscale = null;
		List<Node> nodes = new ArrayList<>();

		nodeDenominazione = doc.selectSingleNode(startNode + name + endNode);
		nodes.add(nodeDenominazione);

		nodeCodiceFiscale = doc.selectSingleNode(startNode + name + endNode2);

		nodes.add(nodeCodiceFiscale);

		return nodes;
	}

	public static Element getTestata(
		org.w3c.dom.Document document, String url, String nomeFile,
		boolean test,String nameCategory) {

		Date today = new Date();
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(today);
		DateTimeFormatter formmat1 = DateTimeFormatter.ofPattern(
			"yyyy-MM-dd", Locale.ITALIAN);
		LocalDateTime ldt = LocalDateTime.now();

		String formatter = formmat1.format(ldt);

		Element legge = document.createElement(Constants.LEGGE190);

		legge.setAttribute(
			Constants.LEGGE190_XMLSN_TAG, Constants.LEGGE190_XMLSN);
		legge.setAttribute(Constants.XMLNS_HTTP_TAG, Constants.XMLNS_HTTP);
		legge.setAttribute(Constants.XSI_TAG, Constants.XSI);

		document.appendChild(legge);

		Element metadata = document.createElement(Constants.METADATA);

		legge.appendChild(metadata);

		Element titolo = document.createElement(Constants.TITOLO);

		titolo.appendChild(
			document.createTextNode(Constants.PUBBLICAZIONE_LEGGE));

		metadata.appendChild(titolo);

		Element abstract1 = document.createElement(Constants.ABSTRACT);

		abstract1.appendChild(
			document.createTextNode(Constants.PUBBLICAZIONE_LEGGE_ANNO));

		metadata.appendChild(abstract1);

		Element dataPubblicazioneDataset = document.createElement(
			Constants.DATA_PUBB_SET);

		dataPubblicazioneDataset.appendChild(
			document.createTextNode(formatter));

		metadata.appendChild(dataPubblicazioneDataset);

		Element entePubblicatore = document.createElement(Constants.ENTE);

		if(test) {
			entePubblicatore.appendChild(
					document.createTextNode(Constants.UFFICIO_UNICO));
		}else {
			entePubblicatore.appendChild(
					document.createTextNode(nameCategory));
		}

		metadata.appendChild(entePubblicatore);

		Element dataUltimoAggiornamentoDataset = document.createElement(
			Constants.DATA_ULTIMO_AGGI);

		dataUltimoAggiornamentoDataset.appendChild(
			document.createTextNode(formatter));

		metadata.appendChild(dataUltimoAggiornamentoDataset);

		Element annoRiferimento = document.createElement(Constants.ANNO_RIF);

		int anno = calendar.get(Calendar.YEAR) - 1;

		if (test) {
			annoRiferimento.appendChild(
				document.createTextNode(String.valueOf(anno)));
		}
		else {
			annoRiferimento.appendChild(
				document.createTextNode(
					String.valueOf(calendar.get(Calendar.YEAR))));
		}

		metadata.appendChild(annoRiferimento);

		Element urlFile = document.createElement(Constants.URL_FILE);

		urlFile.appendChild(
			document.createTextNode(url + StringPool.BACK_SLASH + nomeFile));

		metadata.appendChild(urlFile);

		Element licenza = document.createElement(Constants.LICENZA);

		licenza.appendChild(document.createTextNode(Constants.IODL));

		metadata.appendChild(licenza);

		return legge;
	}
	
	public static void createOnFS(File file, String name) throws IOException {
		String destination = PortletPropsValues.PATH_DESTINATION;
		File dest = new File(
			destination + File.separator + name);
		
		FileOutputStream fos = new FileOutputStream(dest);
		
		fos.write(readFileToByteArray(file));
		fos.flush();
		fos.close();
		
		_log.info("******* File creato");

//		if (dest.exists() && !dest.delete()) {
//
//			_log.info("Non sono riuscito a eliminare il file sul FS!");
//		}
//
//		if (file.renameTo(dest)) {
//			if(!file.delete()) {
//				_log.info("Non sono riuscito a eliminare il file sul FS!");
//			}
//			_log.info("XML aggiunto con successo in FS = " + name);
//		}
//		else {
//			_log.info("XML non aggiunto su FS");
//		}
	}
	
	public static byte[] readFileToByteArray(File file) throws IOException{
		FileInputStream fis = null;
		
		byte[] bArray = new byte[(int) file.length()];
		fis = new FileInputStream(file);
		fis.read(bArray);
		fis.close();
		return bArray;
	}

	private static final Log _log = LogFactoryUtil.getLog(
		XmlUtil.class);

}