# portal-fe-puglia

Portal Fe for Puglia pubblica amministrazione

# template statico per Portale Istituzionale - Regione Puglia.

## Per avviare node-sass, per la compilazione di scss รจ necessario effettuare il lancio da linea di comando della seguenti istruzioni:
## npm install
## npm run scss

## Nella cartella public sono presenti le pagine statiche con il css compilato.
## Nella cartella src sono presenti i componenti scss.

# E' stato utilizzato il framework Bootstrap v4.3.1