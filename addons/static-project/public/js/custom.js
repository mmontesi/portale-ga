$(document).ready(function () {
	var table = $('#table-custom').DataTable({
		responsive: true,
		fixedHeader: {
			header: false,
			footer: false
		}
	});
	new $.fn.dataTable.FixedHeader(table);
}); 

/*$(window).on('resize', function(event){
					if ($( window ).width() <= 720) {
					$.fn.DataTable.ext.pager.numbers_length = 3;
					}
					else {
					$.fn.DataTable.ext.pager.numbers_length = 5;
					}
});*/

$('[aria-controls="collapseTwo"]').on('click', function (event) {
	if ($(event.target).attr('aria-expanded') == 'true') {
		$.each($(event.target).closest('.panel').find('button'), function (index, element) {
			if ($(element).attr('aria-expanded') == 'true') {
				$(element).click();
			}
		});
	}
});

$('.collapseCustom').on('click', function (event) {
	event.preventDefault();
	$(event.target).closest(".collapseCustom").toggleClass("active");
});




$(window).on("load resize", function (e) {
	if ($(this).width() < 575.98) {
		$(".logo-puglia-footer").after($(".login"));
	} else {
		$(".social").before($(".login"));
	}
});

$(function () {
	var lastScrollTop = 0;
	$(window).scroll(function () {
		var x = $(window).scrollTop();
		if (x > 0) {
			$('#wrapper').addClass('scroll-header');
			//$("#menuHorizzontal").show();
		} else if (x < 0) {
			$('#wrapper').removeClass('scroll-header');
		}
		if (x <= lastScrollTop) {
			$('#wrapper').removeClass('scroll-header');
		}
		lastScrollTop = x;
	});
});

function myFunction() {
	var input, filter, ul, li, a, i, txtValue;
	input = document.getElementById("myInput");
	filter = input.value.toUpperCase();
	ul = document.getElementById("myUL");
	li = ul.querySelectorAll("li.searchable");
	for (i = 0; i < li.length; i++) {
		a = li[i].getElementsByTagName("a")[0];
		txtValue = a.textContent || a.innerText;
		if (txtValue.toUpperCase().indexOf(filter) > -1) {
			li[i].style.display = "";
		} else {
			li[i].style.display = "none";
		}
	}
}
