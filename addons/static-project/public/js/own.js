$(document).ready(function () {
    $('#owl-one').owlCarousel({
        loop: true,
        margin: 30,
        center: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                autoWidth: true,
                margin: 20,
                nav: false,
                autoplay: false,
                autoplayTimeout: 4000,
                autoplayHoverPause: false
            },
            600: {
                items: 2,
                nav: false,
                autoplay: false,
                navText: ["<span style='font-size:23px' class='fas fa-chevron-left'></span>", "<span style='font-size:23px' class='fas fa-chevron-right'></span>"]
            },
            1000: {
                items: 3,
                nav: true,
                loop: false,
                autoplay: false,
                center: false,
                navText: ["<span style='font-size:23px' class='fas fa-chevron-left'></span>", "<span style='font-size:23px' class='fas fa-chevron-right'></span>"]
            }
        }
    });
    $('#owl-two').owlCarousel({
        loop: true,
        margin: 30,
        center: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                autoWidth: true,
                margin: 20,
                nav: false,
                autoplay: false,
                autoplayTimeout: 4000,
                autoplayHoverPause: false
            },
            600: {
                items: 2,
                nav: false,
                autoplay: false,
                navText: ["<span style='font-size:23px' class='fas fa-chevron-left'></span>", "<span style='font-size:23px' class='fas fa-chevron-right'></span>"]
            },
            1000: {
                items: 3,
                nav: true,
                loop: false,
                autoplay: false,
                center: false,
                navText: ["<span style='font-size:23px' class='fas fa-chevron-left'></span>", "<span style='font-size:23px' class='fas fa-chevron-right'></span>"]
            }
        }
    });
});
