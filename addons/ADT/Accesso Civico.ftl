<#-- Accesso Civico -->

<div class="civic-access-adt">

	<#assign journalArticleLocalService=serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
	<#if entries?has_content>
		<div class="section-accordion-custom">
			<ul class="myUL">
				<#list entries as entry>
					<#if (entry.className="com.liferay.journal.model.JournalArticle" )>
						<#assign entry=entry assetRenderer=entry.getAssetRenderer() entryTitle=htmlUtil.escape(assetRenderer.getTitle(locale)) viewURL=assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, entry, !stringUtil.equals(assetLinkBehavior, "showFullContent" )) journalArticle=assetRenderer.getAssetObject() document=saxReaderUtil.read(journalArticle.getContent()) rootElement=document.getRootElement()>
						<#assign accessoDetails=journalArticleLocalService.getArticleContent(journalArticle, journalArticle.getDDMTemplateKey(), "VIEW" , locale, themeDisplay)>
						<#assign articleID="article-" + journalArticle.getArticleId() collpaseID="collpase-" + journalArticle.getArticleId()>

						<li class="searchable">
							<h4 class="main-title">${entryTitle}</h4>
							<span>${accessoDetails}</span>
						</li>
					</#if>
				</#list>
			</ul>
		</div>
	</#if>
</div>