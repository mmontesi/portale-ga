<#-- Tabella Lista -->

<div class="executives-table">
	<#if entries?has_content>
		<#assign journalArticleLocalService=serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
		<#if entries?has_content>
			<div class="section-accordion-custom">
				<ul class="myUL">
					<#list entries as entry>
						<#if (entry.className="com.liferay.journal.model.JournalArticle" )>
							<#assign entry=entry assetRenderer=entry.getAssetRenderer() entryTitle=htmlUtil.escape(assetRenderer.getTitle(locale)) viewURL=assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, entry, !stringUtil.equals(assetLinkBehavior, "showFullContent" )) journalArticle=assetRenderer.getAssetObject() document=saxReaderUtil.read(journalArticle.getContent()) rootElement=document.getRootElement()>
							<#assign tabellaDirDetails=journalArticleLocalService.getArticleContent(journalArticle, journalArticle.getDDMTemplateKey(), "VIEW" , locale, themeDisplay)>
							<#assign articleID="article-" + journalArticle.getArticleId() collpaseID="collpase-" + journalArticle.getArticleId()>

							<li class="searchable">
								<div class="panel-group accordion" id="accordion-custom" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="${articleID}">
											<h4 class="panel-title">
												<a class="collapseCustom collapse-button" role="button" data-toggle="collapse" data-parent="#accordion-custom" href="#${collpaseID}" aria-expanded="true" aria-controls="${collpaseID}">
													<span class="title">${entryTitle} <br /> </span>
													<span class="expand"></span>
												</a>
											</h4>
										</div>
										<div id="${collpaseID}" class="panel-collapse collapse collapseCustom" role="tabpanel" aria-labelledby="${articleID}">
											<div class="panel-body">
												${tabellaDirDetails}
											</div>
										</div>
									</div>
								</div>
							</li>
						</#if>
					</#list>
				</ul>
			</div>
		</#if>
	</#if>
</div>