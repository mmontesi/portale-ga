<#-- Tassi Assenza -->
<script type="text/javascript">
    define.amd = false;
    exports = '';

$(document).ready(function () {
	$.fn.DataTable.ext.pager.numbers_length = 3;
	var table = $('#table-custom').DataTable({
		responsive: true,
		fixedHeader: {
			header: false,
			footer: false
		},
		"language": {
			"sProcessing": "Caricamento...",
			"sLengthMenu": "Mostra _MENU_ elementi",
			"sZeroRecords": "La ricerca non ha prodotto risultati",
			"sInfo": "Elementi da _START_ a _END_ di _TOTAL_",
			"sInfoEmpty": "Da 0 a 0 di 0 elementi",
			"sInfoPostFix": "",
			"sSearch": "Cerca ",
			"sUrl": "",
			"oPaginate": {
				"sFirst": "Primo",
				"sPrevious": "Indietro",
				"sNext": "Avanti",
				"sLast": "Ultimo"
			}
		},
		"order": [[ 0, 'desc' ]],
		"ordering": false
	});

});
</script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<#if entries?has_content>
	<div class="absence-rates-adt">

		<#assign journalArticleLocalService=serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
		<table id="table-custom" class="table table-striped table-bordered" style="width:100%">
			<thead>
			<tr role="row">
				<th>Anno</th>
				<th>Mese</th>
				<th>Nome Ufficio</th>
				<th>N dipendenti di servizio</th>
				<th>Assenze giornaliere</th>
				<th>Presenze giornaliere</th>
				<th >Percentuale assenze</th>
				<th>Percentuale presenze</th>
				<th>Documento</th>
			</thead>
			<tbody>
			<#list entries as curEntry>
				<#if (curEntry.className="com.liferay.journal.model.JournalArticle" )>
					<#assign entry=curEntry assetRenderer=curEntry.getAssetRenderer() entryTitle=htmlUtil.escape(assetRenderer.getTitle(locale)) viewURL=assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, curEntry, !stringUtil.equals(assetLinkBehavior, "showFullContent" )) journalArticle=assetRenderer.getAssetObject() document=saxReaderUtil.read(journalArticle.getContent()) rootElement=document.getRootElement()>
					<#assign tassiAssenzaDetails=journalArticleLocalService.getArticleContent(journalArticle, journalArticle.getDDMTemplateKey(), "VIEW" , locale, themeDisplay)>


					<#assign data = document.valueOf("//dynamic-element[@name='data']/dynamic-content/text()") />
					<#if validator.isNotNull(data)>
						<#assign data_DateObj = dateUtil.parseDate("yyyy-MM-dd", data, locale)>
						<#assign anno = dateUtil.getDate(data_DateObj, "yyyy", locale) />
						<#assign mese = dateUtil.getDate(data_DateObj, "MMMM", locale) />
					</#if>

					<#assign category = curEntry.getCategories() />
					<#assign cat = category[0].name />

					<#assign nDipendentiDiServizio = document.valueOf("//dynamic-element[@name='nDipendentiDiServizio']/dynamic-content/text()") />
					<#assign assenzeGiornaliere = document.valueOf("//dynamic-element[@name='assenzeGiornaliere']/dynamic-content/text()") />
					<#assign presenzeGiornaliere = document.valueOf("//dynamic-element[@name='presenzeGiornaliere']/dynamic-content/text()") />
					<#assign percentualeAssenze = document.valueOf("//dynamic-element[@name='percentualeAssenze']/dynamic-content/text()") />
					<#assign percentualePresenze = document.valueOf("//dynamic-element[@name='percentualePresenze']/dynamic-content/text()") />
					<#assign percentualePresenze = document.valueOf("//dynamic-element[@name='percentualePresenze']/dynamic-content/text()") />

					<#assign documento = document.valueOf("//dynamic-element[@name='documento']/dynamic-content/text()") />
					<#if documento?has_content && documento?length gt 0>
						<#assign doc = documento?split("/") />
						<#assign groupId = doc[2]?number />
						<#assign uuId = doc[5]?keep_before('?') />

						<#assign dlfileentry = serviceLocator.findService("com.liferay.document.library.kernel.service.DLFileEntryLocalService") />
						<#assign file = dlfileentry.getDLFileEntryByUuidAndGroupId(uuId, groupId) />
						<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService")>
						<#assign assetEntry=assetEntryLocalService.getEntry("com.liferay.document.library.kernel.model.DLFileEntry", file.fileEntryId) />
						<#assign assetRenderer=assetEntry.assetRenderer />
						<#assign docUrl=assetRenderer.getURLDownload(themeDisplay) />

						<#assign file_description='' />

						<#if file.getDescription()?? &&  file.getDescription()!=''>
							<#assign file_description=file.getDescription() />
						<#else>
							<#assign file_description=file.getFileName() />
						</#if>

					</#if>

					<tr>
						<td><#if anno?has_content>${anno}</#if></td>
						<td><#if mese?has_content>${mese}</#if></td>
						<td><#if cat?has_content>${cat}</#if></td>
						<td><#if nDipendentiDiServizio?has_content>${nDipendentiDiServizio}</#if></td>
						<td><#if assenzeGiornaliere?has_content>${assenzeGiornaliere}</#if></td>
						<td><#if presenzeGiornaliere?has_content>${presenzeGiornaliere}</#if></td>
						<td><#if percentualeAssenze?has_content>${percentualeAssenze}%</#if></td>
						<td><#if percentualePresenze?has_content>${percentualePresenze}%</#if></td>
						<td><#if docUrl?has_content && docUrl?length gt 0>
							<a download href="${docUrl}"><i class="far fa-arrow-alt-circle-down"></i> ${file_description}</a></#if>
						</td>
					</tr>


				</#if>
			</#list>
			</tbody>
		</table>
	</div>
</#if>