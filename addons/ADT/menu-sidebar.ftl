<#-- Menu Sidebar -->

<div class="menu-sidebar">
	<#if entries?has_content>
		<#list entries as navigationEntry>
			<#assign
			navHasChildren = navigationEntry.hasChildren()
			nav_item_css_class = ""
			nav_item_expanded = ""
			/>
			<#if navigationEntry.isSelected() && !navigationEntry.isChildSelected() >
				<#assign nav_item_css_class = "${nav_item_css_class} selected active" />
			</#if>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
							<a class="${nav_item_css_class}" href="${navigationEntry.getURL()}">
								<span></span>${navigationEntry.getName()}
							</a>
							<#if navHasChildren>
								<div class="button-parent collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-${navigationEntry.getLayoutId()}" aria-expanded="false" aria-controls="collapseOne">
									<span></span>
								</div>
							</#if>
						</h4>
					</div>
					<#if navHasChildren>
						<#if navigationEntry.isSelected() && navigationEntry.isChildSelected() >
							<#assign nav_item_expanded = "in" />
						</#if>
						<div id="collapse-${navigationEntry.getLayoutId()}" class="panel-collapse collapse ${nav_item_expanded}" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<ul class="parent-menu">
									<#list navigationEntry.getChildren() as nav_child>
										<#assign nav_item_css_class = "" />
										<#if nav_child.isSelected() && !nav_child.isChildSelected()>
											<#assign nav_item_css_class = "${nav_item_css_class} selected active" />
										</#if>
										<li>
											<div class="panel-title">
												<a class="${nav_item_css_class}" href="${nav_child.getURL()}">${nav_child.getName()}</a>
												<#if nav_child.hasChildren()>
												<#assign nav_item_expanded = "" />
												<#if nav_child.isChildSelected() >
													<#assign nav_item_expanded = "in" />
												</#if>
												<div class="button-child collapsed" type="button" data-toggle="collapse" data-target="#collapseChild-${nav_child.getLayoutId()}" aria-expanded="false" aria-controls="collapseExample">
													<span></span>
												</div>
											</div>
											<div class="collapse collapse-child ${nav_item_expanded}" id="collapseChild-${nav_child.getLayoutId()}">
												<ul class="siblings-menu">
													<#list nav_child.getChildren() as nav_nephew>
														<#assign nav_item_css_class = "" />
														<#if nav_nephew.isSelected() >
															<#assign nav_item_css_class = "${nav_item_css_class} selected active" />
														</#if>
														<li>
															<a class="${nav_item_css_class}" href="${nav_nephew.getURL()}">${nav_nephew.getName()}</a>
														</li>
													</#list>
												</ul>
											</div>
											</#if>
										</li>
									</#list>
								</ul>
							</div>
						</div>
					</#if>
				</div>
			</div>
		</#list>
	</#if>
</div>