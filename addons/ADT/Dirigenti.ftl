<#-- Dirigenti -->

<#if entries?has_content>
	<div class="executives">
		<#assign journalArticleLocalService=serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
		<#assign portletID = themeDisplay.getPortletDisplay().getInstanceId()  />
		<#assign collapseID = "collapseID-" + portletID />

		<div class="section-accordion-custom">
			<ul class="myUL">
				<li class="searchable">
					<div class="panel-group accordion" id="accordion-custom" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="${portletID}">
								<h4 class="panel-title">
									<a class="collapseCustom collapse-button" role="button" data-toggle="collapse" data-parent="#accordion-custom" href="#${collapseID}" aria-expanded="true" aria-controls="${collapseID}">
										<span class="title">${themeDisplay.getPortletDisplay().getTitle()} <br /> </span>
										<span class="expand"></span>
									</a>
								</h4>
							</div>
							<div id="${collapseID}" class="panel-collapse collapse collapseCustom" role="tabpanel" aria-labelledby="${portletID}">
								<div class="panel-body">
									<ul class="list">
										<#list entries as curEntry>
											<#assign uuid = curEntry.getClassUuid()>
											<#assign groupid = curEntry.getGroupId()>
											<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService")>
											<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />
											<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuid, groupid?number) />
											<#assign assetEntry=assetEntryLocalService.getEntry("com.liferay.document.library.kernel.model.DLFileEntry", file.fileEntryId) />
											<#assign assetRenderer=assetEntry.assetRenderer />
											<#assign docUrl=assetRenderer.getURLDownload(themeDisplay) />


											<#assign file_description='' />
											<#if file.getDescription()?? &&  file.getDescription()!=''>
												<#assign file_description=file.getDescription() />
											<#else>
												<#assign file_description=file.getTitle() />
											</#if>
											<li>
												<a href="${docUrl}">
													<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
												</a>
											</li>
										</#list>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</#if>