<#-- Ufficio -->
<script type="text/javascript">
    define.amd = false;
    exports = '';

$(document).ready(function () {
	var table = $('#table-custom2').DataTable({
		responsive: true,
		fixedHeader: {
			header: false,
			footer: false
		},
		"language": {
			"sProcessing": "Caricamento...",
			"sLengthMenu": "Mostra _MENU_ elementi",
			"sZeroRecords": "La ricerca non ha prodotto risultati",
			"sInfo": "Elementi da _START_ a _END_ di _TOTAL_",
			"sInfoEmpty": "Da 0 a 0 di 0 elementi",
			"sInfoPostFix": "",
			"sSearch": "Cerca ",
			"sUrl": "",
			"oPaginate": {
				"sFirst": "Primo",
				"sPrevious": "Indietro",
				"sNext": "Avanti",
				"sLast": "Ultimo"
			}
		},
		"order": [[ 0, 'desc' ]],
		"ordering": false
	});
	// table.columns.adjust().draw();
	//new $.fn.dataTable.FixedHeader(table);
});
</script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<#if entries?has_content>
	<div class="office-adt">
		<#assign journalArticleLocalService=serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
		<table id="table-custom2" class="table table-striped table-bordered" style="width:100%">
			<thead>
			<tr role="row">
				<th>Ufficio</th>
				<th>Dettaglio</th>
				<th>Telefono</th>
				<th>PEC</th>
			</tr>
			</thead>
			<tbody>
			<#list entries as curEntry>
				<#if (curEntry.className="com.liferay.journal.model.JournalArticle" )>
					<#assign entry=curEntry assetRenderer=curEntry.getAssetRenderer() entryTitle=htmlUtil.escape(assetRenderer.getTitle(locale)) viewURL=assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, curEntry, !stringUtil.equals(assetLinkBehavior, "showFullContent" )) journalArticle=assetRenderer.getAssetObject() document=saxReaderUtil.read(journalArticle.getContent()) rootElement=document.getRootElement()>
					<#assign tassiAssenzaDetails=journalArticleLocalService.getArticleContent(journalArticle, journalArticle.getDDMTemplateKey(), "VIEW" , locale, themeDisplay)>


					<#assign ufficio = document.valueOf("//dynamic-element[@name='ufficio']/dynamic-content/text()") />
					<#assign dettaglio = document.valueOf("//dynamic-element[@name='dettaglio']/dynamic-content/text()") />
					<#assign telefono = document.valueOf("//dynamic-element[@name='telefono']/dynamic-content/text()") />
					<#assign pec = document.valueOf("//dynamic-element[@name='pec']/dynamic-content/text()") />


					<tr>
						<td><#if ufficio?has_content>${ufficio}</#if></td>
						<td><#if dettaglio?has_content>${dettaglio}</#if></td>
						<td><#if telefono?has_content>${telefono}</#if></td>
						<td><#if pec?has_content>${pec}</#if></td>
					</tr>
				</#if>
			</#list>
			</tbody>
		</table>
	</div>
</#if>