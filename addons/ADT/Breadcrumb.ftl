<#-- Breadcrumb -->

<#if entries?has_content>
	<div class="bg-breadcrumb">
		<ol class="breadcrumb">

			<li><a href="/amministrazione-trasparente" title="Amministrazione Trasparente"><i style="color: #777" class="fa fa-home"></i></a></li>

			<#list entries as curEntry>
				<#assign cssClass = "" />
				<#if curEntry?is_last>
					<#assign cssClass = "active" />
				</#if>

				<li class="${cssClass}">
					<#if curEntry?has_next>
					<a
							<#if curEntry.isBrowsable()>
								href="${curEntry.getURL()!""}"
							</#if>

					>
						</#if>

						${htmlUtil.escape(curEntry.getTitle())}

						<#if curEntry?has_next>
					</a>
					</#if>
				</li>
			</#list>
		</ol>
	</div>
</#if>