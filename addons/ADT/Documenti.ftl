<#-- Documenti -->

<#if entries?has_content>
	<div class="documents">
		<h4><i class="fas fa-download"></i>&nbsp;&nbsp;${themeDisplay.getPortletDisplay().getTitle()}</h4>
		<ul class="list">
			<#list entries as curEntry>
				<#assign uuid = curEntry.getClassUuid()>
				<#assign groupid = curEntry.getGroupId()>
				<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService")>
				<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />
				<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuid, groupid?number) />
				<#assign assetEntry=assetEntryLocalService.getEntry("com.liferay.document.library.kernel.model.DLFileEntry", file.fileEntryId) />
				<#assign assetRenderer=assetEntry.assetRenderer />
				<#assign docUrl=assetRenderer.getURLDownload(themeDisplay) />
				<#assign extension = file.getExtension() />

				<li>
					<#if extension == "doc" || extension == "txt" || extension == "docx">
						<i class="far fa-file-word"></i>
					<#elseif extension == "pdf">
						<i class="far fa-file-pdf"></i>
					<#elseif extension == "xls" || extension == "xlsx">
						<i class="far fa-file-excel"></i>
					<#else>
						<i class="far fa-file-alt"></i>
					</#if>

					<#assign file_description='' />
					<#if file.getDescription()?? &&  file.getDescription()!=''>
						<#assign file_description=file.getDescription() />
					<#else>
						<#assign file_description=curEntry.getTitle(locale) />
					</#if>

					<a href="${docUrl}">${file_description}</a>
				</li>
			</#list>
		</ul>
	</div>
</#if>