<#-- Serie Documenti Lista -->

<div class="documents-series-list">
	<h3 class="main-title">${.vars['reserved-article-title'].data} </h3>

	<#assign journalArticle = serviceLocator.findService('com.liferay.journal.service.JournalArticleLocalService') />
	<#assign article = journalArticle.getArticle(groupId, .vars['reserved-article-id'].data)/>
	<#assign dataAggiornamento_Data=getterUtil.getString(dataAggiornamento.getData())>
	<#if validator.isNotNull(dataAggiornamento_Data)>
		<#assign dataAggiornamento_DateObj=dateUtil.parseDate("yyyy-MM-dd", dataAggiornamento_Data, locale) />
		<#assign data=dateUtil.getDate(dataAggiornamento_DateObj, "dd/MM/yyyy" , locale) />
		<#if data?has_content>
			<p class="update-date">Ultimo aggiornamento della pagina: ${data}</p>
		</#if>
	</#if>
	<#if contenuto.getData()?has_content>
		<p>${contenuto.getData()}</p>
	</#if>

	<#if titoloSezione.getSiblings()?has_content>
		<#list titoloSezione.getSiblings() as cur_titoloSezione>
			<#if cur_titoloSezione.getData()?has_content && cur_titoloSezione.getData()?length gt 0>
				<#if cur_titoloSezione.documento.getSiblings()?has_content>
					<h4 style="padding-top: 2rem;">${cur_titoloSezione.getData()}</h4>

					<ul id="myUL">
						<#list cur_titoloSezione.documento.getSiblings() as cur_titoloSezioneDoc>
							<#if cur_titoloSezioneDoc.getData()?has_content && cur_titoloSezioneDoc.getData()?length gt 0>
								<#assign cur_Allegato_data = cur_titoloSezioneDoc.getData()?split("/") />
								<#assign groupId = cur_Allegato_data[2]?number />
								<#assign uuId = cur_Allegato_data[5]?keep_before('?') />
								<#assign fileName = cur_Allegato_data[4] />
								<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />
								<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
								<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService")>
								<#assign assetEntry=assetEntryLocalService.getEntry("com.liferay.document.library.kernel.model.DLFileEntry", file.fileEntryId) />
								<#assign assetRenderer=assetEntry.assetRenderer />
								<#assign docUrl=assetRenderer.getURLDownload(themeDisplay) />

								<#assign file_description='' />

								<#if file.getDescription()?? &&  file.getDescription()!=''>
									<#assign file_description=file.getDescription() />
								<#else>
									<#assign file_description=file.getTitle() />
								</#if>


								<p class="searchable">
									<a download href="${docUrl}">
										<i class="far fa-arrow-alt-circle-down"></i>&nbsp;&nbsp;${file_description}
									</a>
								</p>
							</#if>
						</#list>
					</ul>
				</#if>
			</#if>
		</#list>
	</#if>
</div>