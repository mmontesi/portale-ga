<#-- Personale -->

<div class="personale">
	<ul>
		<#if nominativo.getData()?has_content>
			<li>
				<span class="before-text">Nominativo:</span>
				<span class="after-text">${nominativo.getData()}</span>
			</li>
		</#if>

		<#if ruolo.getData()?has_content>
			<li>
				<span class="before-text">Ruolo:</span>
				<span class="after-text">${ruolo.getData()}</span>
			</li>
		</#if>

		<#if provvedimentoDiNomina.getSiblings()?has_content>
			<#assign countProvvedimenti = 0>
			<#list provvedimentoDiNomina.getSiblings() as cur_provvedimentoDiNomina>
				<#if cur_provvedimentoDiNomina.getData()?has_content && cur_provvedimentoDiNomina.getData()?length gt 0>
					<#assign countProvvedimenti = countProvvedimenti + 1>
				</#if>
			</#list>
		</#if>
		<#if countProvvedimenti gt 0>
			<li>
				<span class="before-text">Provvedimenti di nomina:</span>
				<ul class="menu-download">
					<#if provvedimentoDiNomina.getSiblings()?has_content>
						<#list provvedimentoDiNomina.getSiblings() as cur_provvedimentoDiNomina>
							<#if cur_provvedimentoDiNomina.getData()?has_content && cur_provvedimentoDiNomina.getData()?length gt 0>
								<#assign cur_Provv_data = cur_provvedimentoDiNomina.getData()?split("/") />
								<#assign groupId = cur_Provv_data[2]?number />
								<#assign uuId = cur_Provv_data[5]?keep_before('?') />
								<#assign fileName = cur_Provv_data[4] />
								<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

								<#attempt>
									<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
									<#assign file_description='' />
									<#if file.getDescription()?? &&  file.getDescription()!=''>
										<#assign file_description=file.getDescription() />
									<#else>
										<#assign file_description=file.getTitle() />
									</#if>
									<p class="searchable">
										<a download href="${cur_provvedimentoDiNomina.getData()}">
											<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
										</a>
									</p>
									<#recover>
								</#attempt>
							</#if>
						</#list>
					</#if>
				</ul>
			</li>
		</#if>

		<#if cv.getSiblings()?has_content>
			<#assign countCV = 0>
			<#list cv.getSiblings() as cur_cv>
				<#if cur_cv.getData()?has_content && cur_cv.getData()?length gt 0>
					<#assign countCV = countCV + 1 >
				</#if>
			</#list>
		</#if>

		<#if countCV gt 0>
			<li>
				<span class="before-text">CV:</span>
				<ul class="menu-download">
					<#if cv.getSiblings()?has_content>
						<#list cv.getSiblings() as cur_cv>
							<#if cur_cv.getData()?has_content && cur_cv.getData()?length gt 0>
								<#assign cur_CV_data = cur_cv.getData()?split("/") />
								<#assign groupId = cur_CV_data[2]?number />
								<#assign uuId = cur_CV_data[5]?keep_before('?') />
								<#assign fileName = cur_CV_data[4] />
								<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

								<#attempt>
									<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
									<#assign file_description='' />
									<#if file.getDescription()?? &&  file.getDescription()!=''>
										<#assign file_description=file.getDescription() />
									<#else>
										<#assign file_description=file.getTitle() />
									</#if>
									<p class="searchable">
										<a download href="${cur_cv.getData()}">
											<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
										</a>
									</p>
									<#recover>
								</#attempt>
							</#if>
						</#list>
					</#if>
				</ul>
			</li>
		</#if>

		<#if posizionePatrimonialeEReddituale.getSiblings()?has_content>
			<#assign countPosizione = 0>
			<#list posizionePatrimonialeEReddituale.getSiblings() as cur_posizionePatrimonialeEReddituale>
				<#if cur_posizionePatrimonialeEReddituale.getData()?has_content && cur_posizionePatrimonialeEReddituale.getData()?length gt 0>
					<#assign countPosizione = countPosizione + 1 >
				</#if>
			</#list>
		</#if>

		<#if countPosizione gt 0>
			<li>
				<span class="before-text">Posizione patrimoniale e Reddituale:</span>
				<ul class="menu-download">
					<#if posizionePatrimonialeEReddituale.getSiblings()?has_content>
						<#list posizionePatrimonialeEReddituale.getSiblings() as cur_posizionePatrimonialeEReddituale>
							<#if cur_posizionePatrimonialeEReddituale.getData()?has_content && cur_posizionePatrimonialeEReddituale.getData()?length gt 0>
								<#assign cur_Posiz_data = cur_posizionePatrimonialeEReddituale.getData()?split("/") />
								<#assign groupId = cur_Posiz_data[2]?number />
								<#assign uuId = cur_Posiz_data[5]?keep_before('?') />
								<#assign fileName = cur_Posiz_data[4] />
								<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

								<#attempt>
									<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
									<#assign file_description='' />
									<#if file.getDescription()?? &&  file.getDescription()!=''>
										<#assign file_description=file.getDescription() />
									<#else>
										<#assign file_description=file.getTitle() />
									</#if>
									<p class="searchable">
										<a download href="${cur_posizionePatrimonialeEReddituale.getData()}">
											<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
										</a>
									</p>
									<#recover>
								</#attempt>
							</#if>
						</#list>
					</#if>
				</ul>
			</li>
		</#if>


		<#if emolumentiSpettanti.getSiblings()?has_content>
			<#assign countEmol = 0>
			<#list emolumentiSpettanti.getSiblings() as cur_emolumentiSpettanti>
				<#if cur_emolumentiSpettanti.getData()?has_content && cur_emolumentiSpettanti.getData()?length gt 0>
					<#assign countEmol = countEmol + 1 />
				</#if>
			</#list>
		</#if>

		<#if countEmol gt 0>
			<li>
				<span class="before-text">Emolumenti Spettanti:</span>
				<ul class="menu-download">
					<#if emolumentiSpettanti.getSiblings()?has_content>
						<#list emolumentiSpettanti.getSiblings() as cur_emolumentiSpettanti>
							<#if cur_emolumentiSpettanti.getData()?has_content && cur_emolumentiSpettanti.getData()?length gt 0>
								<#assign cur_Emol_data = cur_emolumentiSpettanti.getData()?split("/") />
								<#assign groupId =  cur_Emol_data[2]?number />
								<#assign uuId =  cur_Emol_data[5]?keep_before('?') />
								<#assign fileName =  cur_Emol_data[4] />
								<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

								<#attempt>
									<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
									<#assign file_description='' />
									<#if file.getDescription()?? &&  file.getDescription()!=''>
										<#assign file_description=file.getDescription() />
									<#else>
										<#assign file_description=file.getTitle() />
									</#if>
									<p class="searchable">
										<a download href="${cur_emolumentiSpettanti.getData()}">
											<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
										</a>
									</p>
									<#recover>
								</#attempt>
							</#if>
						</#list>
					</#if>
				</ul>
			</li>
		</#if>

		<#if rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici.getSiblings()?has_content>
			<#assign countRimborso = 0>
			<#list rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici.getSiblings() as cur_rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici>
				<#if cur_rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici.getData()?has_content && cur_rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici.getData()?length gt 0>
					<#assign countRimborso = countRimborso + 1 >
				</#if>
			</#list>
		</#if>

		<#if countRimborso gt 0>
			<li>
				<span class="before-text">Rimborso di viaggi di servizio e missioni pagati con fondi pubblici:</span>
				<ul class="menu-download">
					<#if rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici.getSiblings()?has_content>
						<#list rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici.getSiblings() as cur_rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici>
							<#if cur_rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici.getData()?has_content && cur_rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici.getData()?length gt 0>
								<#assign cur_Rimborso_data = cur_rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici.getData()?split("/") />
								<#assign groupId = cur_Rimborso_data[2]?number />
								<#assign uuId = cur_Rimborso_data[5]?keep_before('?') />
								<#assign fileName = cur_Rimborso_data[4] />
								<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

								<#attempt>
									<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
									<#assign file_description='' />
									<#if file.getDescription()?? &&  file.getDescription()!=''>
										<#assign file_description=file.getDescription() />
									<#else>
										<#assign file_description=file.getTitle() />
									</#if>
									<p class="searchable">
										<a download href="${cur_rimborsoDiViaggiDiServizioEMissioniPagatiConFondiPubblici.getData()}">
											<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
										</a>
									</p>
									<#recover>
								</#attempt>
							</#if>
						</#list>
					</#if>
				</ul>
			</li>
		</#if>

		<#if dichiarazioneExArt20DLgsN392013.getSiblings()?has_content>
			<#assign countDic1 = 0>
			<#list dichiarazioneExArt20DLgsN392013.getSiblings() as cur_dichiarazioneExArt20DLgsN392013>
				<#if cur_dichiarazioneExArt20DLgsN392013.getData()?has_content && cur_dichiarazioneExArt20DLgsN392013.getData()?length gt 0>
					<#assign countDic1 = countDic1 + 1 >
				</#if>
			</#list>
		</#if>

		<#if countDic1 gt 0>
			<li>
				<span class="before-text">Dichiarazione ex art 20 d lgs n 39/2013:</span>
				<ul class="menu-download">
					<#if dichiarazioneExArt20DLgsN392013.getSiblings()?has_content>
						<#list dichiarazioneExArt20DLgsN392013.getSiblings() as cur_dichiarazioneExArt20DLgsN392013>
							<#if cur_dichiarazioneExArt20DLgsN392013.getData()?has_content && cur_dichiarazioneExArt20DLgsN392013.getData()?length gt 0>
								<#assign cur_Dic1_data = cur_dichiarazioneExArt20DLgsN392013.getData()?split("/") />
								<#assign groupId = cur_Dic1_data[2]?number />
								<#assign uuId = cur_Dic1_data[5]?keep_before('?') />
								<#assign fileName = cur_Dic1_data[4] />
								<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

								<#attempt>
									<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
									<#assign file_description='' />
									<#if file.getDescription()?? &&  file.getDescription()!=''>
										<#assign file_description=file.getDescription() />
									<#else>
										<#assign file_description=file.getTitle() />
									</#if>
									<p class="searchable">
										<a download href="${cur_dichiarazioneExArt20DLgsN392013.getData()}">
											<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
										</a>
									</p>
									<#recover>
								</#attempt>
							</#if>
						</#list>
					</#if>
				</ul>
			</li>
		</#if>

		<#if dichiarazioneExArt14Comma1terDLgsN332013.getSiblings()?has_content>
			<#assign countDic2 = 0>
			<#list dichiarazioneExArt14Comma1terDLgsN332013.getSiblings() as cur_dichiarazioneExArt14Comma1terDLgsN332013>
				<#if cur_dichiarazioneExArt14Comma1terDLgsN332013.getData()?has_content && cur_dichiarazioneExArt14Comma1terDLgsN332013.getData()?length gt 0>
					<#assign countDic2 = countDic2 + 1 >
				</#if>
			</#list>
		</#if>

		<#if countDic2 gt 0>
			<li>
				<span class="before-text">Dichiarazione ex art 14, comma 1ter, d lgs n 33/2013:</span>
				<ul class="menu-download">
					<#if dichiarazioneExArt14Comma1terDLgsN332013.getSiblings()?has_content>
						<#list dichiarazioneExArt14Comma1terDLgsN332013.getSiblings() as cur_dichiarazioneExArt14Comma1terDLgsN332013>
							<#if cur_dichiarazioneExArt14Comma1terDLgsN332013.getData()?has_content && cur_dichiarazioneExArt14Comma1terDLgsN332013.getData()?length gt 0>
								<#assign cur_Dic2_data = cur_dichiarazioneExArt14Comma1terDLgsN332013.getData()?split("/") />
								<#assign groupId = cur_Dic2_data[2]?number />
								<#assign uuId = cur_Dic2_data[5]?keep_before('?') />
								<#assign fileName = cur_Dic2_data[4] />
								<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

								<#attempt>
									<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
									<#assign file_description='' />
									<#if file.getDescription()?? &&  file.getDescription()!=''>
										<#assign file_description=file.getDescription() />
									<#else>
										<#assign file_description=file.getTitle() />
									</#if>
									<p class="searchable">
										<a download href="${cur_dichiarazioneExArt14Comma1terDLgsN332013.getData()}">
											<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
										</a>
									</p>
									<#recover>
								</#attempt>
							</#if>
						</#list>
					</#if>
				</ul>
			</li>
		</#if>
	</ul>
</div>