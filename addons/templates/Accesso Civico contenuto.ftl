<#-- Accesso Civico -->

<div class="civic-access">
	<ul class="list">
		<#if nominativo.getData()?has_content>
			<li>
				<span class="before-text">Nominativo:</span>
				<span class="after-text">${nominativo.getData()}</span>
			</li>
		</#if>

		<#if ruolo.getData()?has_content>
			<li>
				<span class="before-text">Ruolo:</span>
				<span class="after-text">${ruolo.getData()}</span>
			</li>
		</#if>

		<#if indirizzoDiPostaElettronica.getData()?has_content>
			<li>
				<span class="before-text">Indirizzo di posta elettronica:</span>
				<span class="after-text">${indirizzoDiPostaElettronica.getData()}</span>
			</li>
		</#if>


		<#if moduloAccessoCivico.getSiblings()?has_content>
			<#assign countMod = 0 />
			<#list moduloAccessoCivico.getSiblings() as cur_moduloAccessoCivico>
				<#if cur_moduloAccessoCivico.getData()?has_content && cur_moduloAccessoCivico.getData()?length gt 0>
					<#assign countMod = countMod + 1 />
				</#if>
			</#list>
		</#if>

		<#if countMod gt 0>
			<#list moduloAccessoCivico.getSiblings() as cur_moduloAccessoCivico>
				<#assign cur_mod = cur_moduloAccessoCivico.getData()?split("/") />
				<#assign groupId = cur_mod[2]?number />
				<#assign uuId = cur_mod[5]?keep_before('?') />
				<#assign fileName = cur_mod[4] />
				<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />
				<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
				<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService")>
				<#assign assetEntry=assetEntryLocalService.getEntry("com.liferay.document.library.kernel.model.DLFileEntry", file.fileEntryId) />
				<#assign assetRenderer=assetEntry.assetRenderer />
				<#assign docUrl=assetRenderer.getURLDownload(themeDisplay) />

				<#assign file_description='' />

				<#if file.getDescription()?? &&  file.getDescription()!=''>
					<#assign file_description=file.getDescription() />
				<#else>
					<#assign file_description=fileName />
				</#if>


				<p class="searchable">
					<a download href="${docUrl}">
						<i class="far fa-arrow-alt-circle-down"></i>&nbsp;&nbsp;${file_description}
					</a>
				</p>
			</#list>
		</#if>
	</ul>
</div>