<#-- GA Base -->
<div class="ga-base">
	<h3 class="main-title">${.vars['reserved-article-title'].data} </h3>

	<#assign dataAggiornamento_Data=getterUtil.getString(dataAggiornamento.getData())>
	<#if validator.isNotNull(dataAggiornamento_Data)>
		<#assign dataAggiornamento_DateObj=dateUtil.parseDate("yyyy-MM-dd", dataAggiornamento_Data, locale) />
		<#assign data = dateUtil.getDate(dataAggiornamento_DateObj, "dd/MM/yyyy", locale) />

		<#if data?has_content>
			<p class="update-date">Ultimo aggiornamento della pagina: ${data}</p>
		</#if>
	</#if>


	<#if contenuto.getData()?has_content>
		<p>${contenuto.getData()}</p>
	</#if>
</div>