<#-- Tabella Link -->
<div class="link-table">
	<#if labelLink.getSiblings()?has_content>
		<#assign countLink = 0>
		<#list labelLink.getSiblings() as cur_labelLink>
			<#if cur_labelLink.getData()?has_content && cur_labelLink.getData()?length gt 0>
				<#if cur_labelLink.urlLink.getData()?has_content && cur_labelLink.urlLink.getData()?length gt 0>
					<#assign countLink = countLink + 1>
				</#if>
			</#if>
		</#list>

		<#if countLink gt 0>
			<#list labelLink.getSiblings() as cur_labelLink>
				<#if cur_labelLink.getData()?has_content && cur_labelLink.getData()?length gt 0 && cur_labelLink.urlLink.getData()?has_content && cur_labelLink.urlLink.getData()?length gt 0>
					<p class="searchable">
						<a download target="_blank" href="${cur_labelLink.urlLink.getData()}">
							<#if getterUtil.getBoolean(cur_labelLink.documento.getData())>
								<i class="far fa-arrow-alt-circle-down"></i>
							<#else>
								<i class="fas fa-external-link-alt"></i>
							</#if>

							${cur_labelLink.getData()}
						</a>
					</p>
				</#if>
			</#list>
		</#if>
	</#if>
</div>