<#-- Consulenti e Collaboratori -->

<div class="consultants">
	<ul>
		<#if nominativo.getData()?has_content>
			<li>
				<span class="before-text">Nominativo:</span>
				<span class="after-text">${nominativo.getData()}</span>
			</li>
		</#if>

		<#if incarico.getData()?has_content>
			<li>
				<span class="before-text">Incarico:</span>
				<span class="after-text">${incarico.getData()}</span>
			</li>
		</#if>

		<#if periodo.getData()?has_content>
			<li>
				<span class="before-text">Periodo:</span>
				<span class="after-text">${periodo.getData()}</span>
			</li>
		</#if>

		<#if CV.getData()?has_content && CV.getData()?length gt 0>
			<li>
				<span class="before-text">CV: </span>
				<ul class="menu-download">
					<#assign cur_CV = CV.getData()?split("/") />
					<#assign groupId = cur_CV[2]?number />
					<#assign uuId = cur_CV[5]?keep_before('?') />
					<#assign fileName = cur_CV[4] />
					<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

					<#attempt>
						<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
						<#assign file_description='' />
						<#if file.getDescription()?? &&  file.getDescription()!=''>
							<#assign file_description=file.getDescription() />
						<#else>
							<#assign file_description=file.getTitle() />
						</#if>
						<p class="searchable">
							<a download href="${CV.getData()}">
								<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
							</a>
						</p>
						<#recover>
					</#attempt>
				</ul>
			</li>
		</#if>


		<#if dichiarazioneExArt15C1LettCDLgsN332013.getSiblings()?has_content>
			<#assign countDic = 0>
			<#list dichiarazioneExArt15C1LettCDLgsN332013.getSiblings() as cur_dic>
				<#if cur_dic?has_content && cur_dic.getData()?has_content && cur_dic.getData()?length gt 0>
					<#assign countDic = countDic + 1 >
				</#if>
			</#list>
		</#if>

		<#if countDic gt 0>
			<li>
				<span class="before-text">Dichiarazione ex Art 15,c 1,lett c) d lgs n 33/2013:</span>
				<ul class="menu-download">
					<#if dichiarazioneExArt15C1LettCDLgsN332013.getSiblings()?has_content>
						<#list dichiarazioneExArt15C1LettCDLgsN332013.getSiblings() as cur_dic>
							<#if cur_dic?has_content && cur_dic.getData()?has_content>
								<#assign cur_Allegato_data = cur_dic.getData()?split("/") />
								<#assign groupId = cur_Allegato_data[2]?number />
								<#assign uuId = cur_Allegato_data[5]?keep_before('?') />
								<#assign fileName = cur_Allegato_data[4] />
								<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

								<#attempt>
									<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
									<#assign file_description='' />
									<#if file.getDescription()?? &&  file.getDescription()!=''>
										<#assign file_description=file.getDescription() />
									<#else>
										<#assign file_description=file.getTitle() />
									</#if>
									<p class="searchable">
										<a download href="${cur_dic.getData()}">
											<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
										</a>
									</p>
									<#recover>
								</#attempt>
							</#if>
						</#list>
					</#if>
				</ul>
			</li>
		</#if>


		<#if attestazioneDellAvvenutaVerificaDellInsussistenzaDiSituazioniAnchePotenzialiDiConflittoDiInteresse.getSiblings()?has_content>
			<#assign countAtt = 0>
			<#list attestazioneDellAvvenutaVerificaDellInsussistenzaDiSituazioniAnchePotenzialiDiConflittoDiInteresse.getSiblings() as cur_att>
				<#if cur_att?has_content && cur_att.getData()?has_content && cur_att.getData()?length gt 0>
					<#assign countAtt = countAtt + 1 >
				</#if>
			</#list>
		</#if>

		<#if countAtt gt 0>
			<li>
				<span class="before-text">Attestazione dell'avvenuta verifica dell'insussistenza di situazioni, anche potenziali,di conflitto di interesse:</span>
				<ul class="menu-download">
					<#if attestazioneDellAvvenutaVerificaDellInsussistenzaDiSituazioniAnchePotenzialiDiConflittoDiInteresse.getSiblings()?has_content>
						<#list attestazioneDellAvvenutaVerificaDellInsussistenzaDiSituazioniAnchePotenzialiDiConflittoDiInteresse.getSiblings() as cur_att>
							<#if cur_att?has_content && cur_att.getData()?has_content>
								<#assign cur_Allegato_data = cur_att.getData()?split("/") />
								<#assign groupId = cur_Allegato_data[2]?number />
								<#assign uuId = cur_Allegato_data[5]?keep_before('?') />
								<#assign fileName = cur_Allegato_data[4] />
								<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

								<#attempt>
									<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
									<#assign file_description='' />
									<#if file.getDescription()?? &&  file.getDescription()!=''>
										<#assign file_description=file.getDescription() />
									<#else>
										<#assign file_description=file.getTitle() />
									</#if>
									<p class="searchable">
										<a download href="${cur_att.getData()}">
											<i class="far fa-arrow-alt-circle-down"></i> ${file_description}
										</a>
									</p>
									<#recover>
								</#attempt>
							</#if>
						</#list>
					</#if>
				</ul>
			</li>
		</#if>
	</ul>
</div>