<#-- Tabella -->

<div class="tabella">

	<#if documento.getSiblings()?has_content>
		<#assign countDoc = 0>
		<#list documento.getSiblings() as cur_documento>
			<#if cur_documento?has_content && cur_documento.getData()?has_content && cur_documento.getData()?length gt 0>
				<#assign countDoc = countDoc + 1 >
			</#if>
		</#list>
	</#if>

	<#if countDoc gt 0>
		<#if documento.getSiblings()?has_content>
			<#list documento.getSiblings() as cur_documento>
				<#if cur_documento?has_content && cur_documento.getData()?has_content && cur_documento.getData()?length gt 0>
					<#assign cur_Allegato_data = cur_documento.getData()?split("/") />
					<#assign groupId = cur_Allegato_data[2]?number />
					<#assign uuId = cur_Allegato_data[5]?keep_before('?') />
					<#assign fileName = cur_Allegato_data[4] />
					<#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />
					<#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
					<#assign file_description='' />
					<#if file.getDescription()?? &&  file.getDescription()!=''>
						<#assign file_description=file.getDescription() />
					<#else>
						<#assign file_description=file.getTitle() />
					</#if>
					<p class="searchable">
						<a download href="${cur_documento.getData()}">
							<i class="far fa-arrow-alt-circle-down"></i>&nbsp;&nbsp;${file_description}
						</a>
					</p>
				</#if>
			</#list>
		</#if>
	</#if>
</div>