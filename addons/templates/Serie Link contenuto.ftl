<#-- Serie Link -->

<div class="link-series">
	<h3 class="main-title">${.vars['reserved-article-title'].data} </h3>
	<#assign dataAggiornamento_Data = getterUtil.getString(dataAggiornamento.getData())>

	<#assign journalArticle = serviceLocator.findService('com.liferay.journal.service.JournalArticleLocalService') />
	<#assign article = journalArticle.getArticle(groupId, .vars['reserved-article-id'].data)/>
	<#assign articleID=article.getArticleId() />
	<#assign dataAggiornamento_Data=getterUtil.getString(dataAggiornamento.getData())>
	<#if validator.isNotNull(dataAggiornamento_Data)>
		<#assign dataAggiornamento_DateObj=dateUtil.parseDate("yyyy-MM-dd", dataAggiornamento_Data, locale) />
		<#assign data=dateUtil.getDate(dataAggiornamento_DateObj, "dd/MM/yyyy" , locale) />
		<#if data?has_content>
			<p class="update-date">Ultimo aggiornamento della pagina: ${data}</p>
		</#if>
	</#if>

	<#if titoloSezione.getSiblings()?has_content>
		<div class="section-accordion-custom">
			<ul class="myUL">
				<#assign collapseID = 0 />
				<#list titoloSezione.getSiblings() as cur_titoloSezione>
					<li class="searchable">
						<#assign count = 0 />
						<#if cur_titoloSezione.getData()?has_content && cur_titoloSezione.getData()?length gt 0>
							<#if cur_titoloSezione.labelLink.getSiblings()?has_content>
								<#list cur_titoloSezione.labelLink.getSiblings() as cur_titoloSezioneLabel>
									<#if cur_titoloSezioneLabel.getData()?has_content && cur_titoloSezioneLabel.getData()?length gt 0 && cur_titoloSezioneLabel.urlLink.getData()?has_content && cur_titoloSezioneLabel.urlLink.getData()?length gt 0>
										<#assign count = count + 1 />
									</#if>
								</#list>
							</#if><#-- cur_titoloSezione.labelLink.getSiblings()?size-->

							<#if count != 0>
								<div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="${articleID}">
											<h4 class="panel-title">
												<a class="collapseCustom collapse-button" role="button" data-toggle="collapse" data-parent="#accordion2" href="#${collapseID}" aria-expanded="true" aria-controls="collapseOne">
													<span class="title">${cur_titoloSezione.getData()}</span>
													<span class="expand"></span>
												</a>
											</h4>
										</div>


										<div id="${collapseID}" class="panel-collapse collapse collapseCustom" role="tabpanel" aria-labelledby="${articleID}">
											<div class="panel-body">
												<ul class="list">
													<#if cur_titoloSezione.labelLink.getSiblings()?has_content>
														<#list cur_titoloSezione.labelLink.getSiblings() as cur_titoloSezioneLabel>
															<#if cur_titoloSezioneLabel.getData()?has_content && cur_titoloSezioneLabel.getData()?length gt 0 && cur_titoloSezioneLabel.urlLink.getData()?has_content && cur_titoloSezioneLabel.urlLink.getData()?length gt 0>

																<p class="searchable">
																	<a download href="${cur_titoloSezioneLabel.urlLink.getData()}">
																		<i class="far fa-arrow-alt-circle-down"></i>&nbsp;&nbsp;${cur_titoloSezioneLabel.getData()}
																	</a>
																</p>
															</#if>
														</#list>
													</#if>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<#assign collapseID = collapseID + 1 />
							</#if>
						</#if>
					</li>
				</#list>
			</ul>
		</div>
	</#if>

</div>