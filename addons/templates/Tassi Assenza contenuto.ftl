<#-- Tassi Assenza -->

<#assign data_Data = getterUtil.getString(data.getData())>

<#if validator.isNotNull(data_Data)>
	<#assign data_DateObj = dateUtil.parseDate("yyyy-MM-dd", data_Data, locale)>
	<#assign anno = dateUtil.getDate(data_DateObj, "yyyy", locale) />
	<#assign mese = dateUtil.getDate(data_DateObj, "MMMM", locale) />
</#if>

<#if documento.getData()?has_content && documento.getData()?length gt 0>
	<#assign doc = documento.getData()?split("/") />
	<#assign groupId = doc[2]?number />
	<#assign uuId = doc[5]?keep_before('?') />
	<#assign fileName = doc[4] />
</#if>

<#assign journalArticle = serviceLocator.findService('com.liferay.journal.service.JournalArticleLocalService') />
<#assign article = journalArticle.getArticle(groupId, .vars['reserved-article-id'].data)/>
<#assign assetEntry = serviceLocator.findService('com.liferay.asset.kernel.service.AssetEntryLocalService') />

<#assign entry = assetEntry.getEntry("com.liferay.journal.model.JournalArticle", article.resourcePrimKey) />
<#assign category = entry.getCategories() />
<#assign cat = category[0].name />


<tr>
	<td><#if anno?has_content>${anno}</#if></td>
	<td><#if mese?has_content>${mese}</#if></td>
	<td><#if cat?has_content>${cat}</#if></td>
	<td><#if nDipendentiDiServizio.getData()?has_content>${nDipendentiDiServizio.getData()}</#if></td>
	<td><#if assenzeGiornaliere.getData()?has_content>${assenzeGiornaliere.getData()}</#if></td>
	<td><#if presenzeGiornaliere.getData()?has_content>${presenzeGiornaliere.getData()}</#if></td>
	<td><#if percentualeAssenze.getData()?has_content>${percentualeAssenze.getData()}%</#if></td>
	<td><#if percentualePresenze.getData()?has_content>${percentualePresenze.getData()}%</#if></td>
	<td><#if documento.getData()?has_content && documento.getData()?length gt 0><a download href="${documento.getData()}"><i class="far fa-arrow-alt-circle-down"></i> ${fileName}</a></#if></td>
</tr>