<#-- Bando di Gara -->

<div class="bando-gara">
  <ul>
    <#if strutturaProponenteDenominazione.getData()?has_content>
      <li>
        <span class="before-text">Struttura proponente:</span>
        <span class="after-text">${strutturaProponenteDenominazione.getData()}</span>
        <#if strutturaProponenteDenominazione.strutturaProponenteCodFiscale.getData()?has_content && strutturaProponenteDenominazione.strutturaProponenteCodFiscale.getData()?length gt 0>
          <span class="after-text"> - Codice Fiscale: ${strutturaProponenteDenominazione.strutturaProponenteCodFiscale.getData()}</span>
        </#if>
      </li>
    </#if>

    <#if proceduraSceltaContraente.getData()?has_content>
      <li>
        <span class="before-text">Procedura scelta contraente:</span>
        <span class="after-text">${proceduraSceltaContraente.getData()}</span>
      </li>
    </#if>

    <#if elencoOperatoriPartecipanti.elencoOperatoriPartecipanti_ragioneSociale.getSiblings()?has_content>
      <#assign countOperatori = 0>
      <#list elencoOperatoriPartecipanti.elencoOperatoriPartecipanti_ragioneSociale.getSiblings() as cur_ragioneSociale>
        <#if (cur_ragioneSociale.getData()?has_content && cur_ragioneSociale.getData()?length gt 0) || ( cur_ragioneSociale.elencoOperatoriPartecipanti_ragioneSociale_codiceFiscale.getData()?has_content && cur_ragioneSociale.elencoOperatoriPartecipanti_ragioneSociale_codiceFiscale.getData()?length gt 0)>
          <#assign countOperatori = countOperatori + 1>
        </#if>
      </#list>
    </#if>
    <#if countOperatori gt 0>
      <li>
        <span class="before-text">Elenco operatori partecipanti:</span>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>Ragione Sociale</th>
              <th>Codice fiscale / P.Iva</th>
            </tr>
            </thead>
            <tbody>
            <#if elencoOperatoriPartecipanti.elencoOperatoriPartecipanti_ragioneSociale.getSiblings()?has_content>
              <#list elencoOperatoriPartecipanti.elencoOperatoriPartecipanti_ragioneSociale.getSiblings() as cur_ragioneSociale>
                <#if (cur_ragioneSociale.getData()?has_content && cur_ragioneSociale.getData()?length gt 0) || ( cur_ragioneSociale.elencoOperatoriPartecipanti_ragioneSociale_codiceFiscale.getData()?has_content && cur_ragioneSociale.elencoOperatoriPartecipanti_ragioneSociale_codiceFiscale.getData()?length gt 0)>
                  <tr>
                    <td>
                      <#if cur_ragioneSociale.getData()?has_content && cur_ragioneSociale.getData()?length gt 0>
                        ${cur_ragioneSociale.getData()}
                      </#if>
                    </td>
                    <td>
                      <#if cur_ragioneSociale.elencoOperatoriPartecipanti_ragioneSociale_codiceFiscale.getData()?has_content && cur_ragioneSociale.elencoOperatoriPartecipanti_ragioneSociale_codiceFiscale.getData()?length gt 0>
                        ${cur_ragioneSociale.elencoOperatoriPartecipanti_ragioneSociale_codiceFiscale.getData()}
                      </#if>
                    </td>
                  </tr>
                </#if>
              </#list>
            </#if>
            </tbody>
          </table>
        </div>
      </li>
    </#if>

    <#if aggiudicatario.aggiudicatario_ragioneSociale.getSiblings()?has_content>
      <#assign countAggiudicatari = 0>
      <#list aggiudicatario.aggiudicatario_ragioneSociale.getSiblings() as cur_ragioneSociale>
        <#if (cur_ragioneSociale.getData()?has_content && cur_ragioneSociale.getData()?length gt 0) || ( cur_ragioneSociale.aggiudicatario_ragioneSociale_codiceFiscale.getData()?has_content && cur_ragioneSociale.aggiudicatario_ragioneSociale_codiceFiscale.getData()?length gt 0)>
          <#assign countAggiudicatari = countAggiudicatari + 1>
        </#if>
      </#list>
    </#if>

    <#if countAggiudicatari gt 0>
      <li>
        <span class="before-text">Aggiudicatario:</span>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>Ragione Sociale</th>
              <th>Codice fiscale / P.Iva</th>
            </tr>
            </thead>
            <tbody>
            <#if aggiudicatario.aggiudicatario_ragioneSociale.getSiblings()?has_content>
              <#list aggiudicatario.aggiudicatario_ragioneSociale.getSiblings() as cur_ragioneSociale>
                <#if (cur_ragioneSociale.getData()?has_content && cur_ragioneSociale.getData()?length gt 0) || ( cur_ragioneSociale.aggiudicatario_ragioneSociale_codiceFiscale.getData()?has_content && cur_ragioneSociale.aggiudicatario_ragioneSociale_codiceFiscale.getData()?length gt 0)>
                  <tr>
                    <#if cur_ragioneSociale.getData()?has_content && cur_ragioneSociale.getData()?length gt 0>
                      <td>${cur_ragioneSociale.getData()}</td>
                    </#if>

                    <#if cur_ragioneSociale.aggiudicatario_ragioneSociale_codiceFiscale.getData()?has_content && cur_ragioneSociale.aggiudicatario_ragioneSociale_codiceFiscale.getData()?length gt 0>
                      <td>${cur_ragioneSociale.aggiudicatario_ragioneSociale_codiceFiscale.getData()}</td>
                    </#if>
                  </tr>
                </#if>
              </#list>
            </#if>
            </tbody>
          </table>
        </div>
      </li>
    </#if>

    <#assign dataInizio_Data = getterUtil.getString(tempiDiCompletamentoDiServizioOFornitura.tempiDiCompletamentoDiServizioOFornitura_dataInizio.getData())>
    <#if validator.isNotNull(dataInizio_Data)>
      <#assign dataInizio_DateObj = dateUtil.parseDate("yyyy-MM-dd", dataInizio_Data, locale)>
      <#assign dataInizioString = "">
      <#if dataInizio_DateObj?has_content>
        <#assign dataInizioString = dateUtil.getDate(dataInizio_DateObj, "dd MMM yyyy", locale)>
      </#if>
    </#if>
    <#assign dataFine_Data = getterUtil.getString(tempiDiCompletamentoDiServizioOFornitura.tempiDiCompletamentoDiServizioOFornitura_dataFine.getData())>
    <#if validator.isNotNull(dataFine_Data)>
      <#assign dataFine_DateObj = dateUtil.parseDate("yyyy-MM-dd", dataFine_Data, locale)>
      <#assign dataFineString = "">
      <#if dataFine_DateObj?has_content>
        <#assign dataFineString = dateUtil.getDate(dataFine_DateObj, "dd MMM yyyy", locale)>
      </#if>
    </#if>


    <#if importoDiAggiudicazioneAlNetto.getData()?has_content && importoDiAggiudicazioneAlNetto.getData()?length gt 0>
      <li>
        <span class="before-text">Importo di aggiudicazione al netto:</span>
        <span class="after-text">${importoDiAggiudicazioneAlNetto.getData()}<span>€</span></span>
        <br />
      </li>
    </#if>

    <#if dataInizioString?has_content || dataFineString?has_content>
      <li>
        <span class="before-text">Tempi di completamento di servizio o fornitura:</span>
        <span class="after-text">

                    <#if dataInizioString?has_content>
                      Data inizio: ${dataInizioString}
                    </#if>
          <#if dataInizioString?has_content && dataFineString?has_content>
            -
          </#if>
          <#if dataFineString?has_content>
            Data fine: ${dataFineString}
          </#if>

                </span>
      </li>
    </#if>

    <#if importoDelleSommeLiquidateAlNettoIVA.getData()?has_content>
      <li>
        <span class="before-text">Importo delle somme liquidate al netto IVA:</span>
        <span class="after-text">${importoDelleSommeLiquidateAlNettoIVA.getData()}<span>€</span></span>
      </li>
    </#if>


    <#if documentiAllegati.getSiblings()?has_content>
      <#assign countDoc = 0>
      <#list documentiAllegati.getSiblings() as cur_documentiAllegati>
        <#if cur_documentiAllegati?has_content && cur_documentiAllegati.getData()?has_content && cur_documentiAllegati.getData()?length gt 0>
          <#assign countDoc = countDoc + 1 >
        </#if>
      </#list>
    </#if>

    <#if countDoc gt 0>
      <li>
        <span class="before-text">Documenti allegati:</span>
        <ul class="menu-download">
          <#if documentiAllegati.getSiblings()?has_content>
            <#list documentiAllegati.getSiblings() as cur_documentiAllegati>
              <#if cur_documentiAllegati?has_content && cur_documentiAllegati.getData()?has_content>
                <#assign cur_Allegato_data = cur_documentiAllegati.getData()?split("/") />
                <#assign groupId = cur_Allegato_data[2]?number />
                <#assign uuId = cur_Allegato_data[5]?keep_before('?') />
                <#assign fileName = cur_Allegato_data[4] />
                <#assign dlFileEntryService = serviceLocator.findService('com.liferay.document.library.kernel.service.DLFileEntryService') />

                <#attempt>
                  <#assign file = dlFileEntryService.getFileEntryByUuidAndGroupId(uuId,groupId) />
                  <#assign file_description='' />
                  <#if file.getDescription()?? &&  file.getDescription()!=''>
                    <#assign file_description=file.getDescription() />
                  <#else>
                    <#assign file_description=file.getTitle() />
                  </#if>
                  <p class="searchable">
                    <a download href="${cur_documentiAllegati.getData()}">
                      <i class="far fa-arrow-alt-circle-down"></i> ${file_description}
                    </a>
                  </p>
                  <#recover>
                </#attempt>
              </#if>
            </#list>
          </#if>
        </ul>
      </li>
    </#if>
  </ul>
</div>