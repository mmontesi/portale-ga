<#-- Footer -->

<#if (ufficioRelazioniConIlPubblico1.getData()?has_content && ufficioRelazioniConIlPubblico1.getData()?length gt 0) || (amministrazioneTrasparente1.getData()?has_content && amministrazioneTrasparente1.getData()?length gt 0)>
	<div class="col-12 col-xs-12 col-md-12 col-lg-4">
		<#if ufficioRelazioniConIlPubblico1.getData()?has_content && ufficioRelazioniConIlPubblico1.getData()?length gt 0>
			<h3>${ufficioRelazioniConIlPubblico1.getData()}</h3>
			<ul>
				<#if ufficioRelazioniConIlPubblico1.pagina11.getData()?has_content && ufficioRelazioniConIlPubblico1.pagina11.getData()?length gt 0 && ufficioRelazioniConIlPubblico1.pagina11.url11.getData()?has_content && ufficioRelazioniConIlPubblico1.pagina11.url11.getData()?length gt 0>
					<li>
						<a class="link" href="${ufficioRelazioniConIlPubblico1.pagina11.url11.getData()}">${ufficioRelazioniConIlPubblico1.pagina11.getData()}</a>
					</li>
				</#if>

				<#if ufficioRelazioniConIlPubblico1.pagina12.getData()?has_content && ufficioRelazioniConIlPubblico1.pagina12.getData()?length gt 0 && ufficioRelazioniConIlPubblico1.pagina12.url12.getData()?has_content && ufficioRelazioniConIlPubblico1.pagina12.url12.getData()?length gt 0>
					<li>
						<a class="link" href="${ufficioRelazioniConIlPubblico1.pagina12.url12.getData()}">${ufficioRelazioniConIlPubblico1.pagina12.getData()}</a>
					</li>
				</#if>
				<#if ufficioRelazioniConIlPubblico1.pagina13.getData()?has_content && ufficioRelazioniConIlPubblico1.pagina13.getData()?length gt 0 && ufficioRelazioniConIlPubblico1.pagina13.url13.getData()?has_content && ufficioRelazioniConIlPubblico1.pagina13.url13.getData()?length gt 0>
					<li>
						<a class="link" href="ufficioRelazioniConIlPubblico1.pagina13.url13.getData()">${ufficioRelazioniConIlPubblico1.pagina13.getData()}</a>
					</li>
				</#if>
			</ul>
		</#if>
		<#if amministrazioneTrasparente1.getData()?has_content && amministrazioneTrasparente1.getData()?length gt 0>
			<h3>${amministrazioneTrasparente1.getData()}</h3>
			<#if amministrazioneTrasparente1.titolo1.getData()?has_content &&amministrazioneTrasparente1.titolo1.getData()?length gt 0 && amministrazioneTrasparente1.titolo1.url1.getData()?has_content && amministrazioneTrasparente1.titolo1.url1.getData()?length gt 0>
				<ul>
					<li>
						<a class="link" href="${amministrazioneTrasparente1.titolo1.url1.getData()}">${amministrazioneTrasparente1.titolo1.getData()}</a>
					</li>
				</ul>
			</#if>
		</#if>
	</div>
</#if>
<#if (ufficioRelazioniConIlPubblico2.getData()?has_content && ufficioRelazioniConIlPubblico2.getData()?length gt 0) || (pubblicitaLegale.getData()?has_content && pubblicitaLegale.getData()?length gt 0)>
	<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-4">
		<#if ufficioRelazioniConIlPubblico2.getData()?has_content && ufficioRelazioniConIlPubblico2.getData()?length gt 0>
			<h3>${ufficioRelazioniConIlPubblico2.getData()}</h3>
			<ul>
				<#if ufficioRelazioniConIlPubblico2.pagina21.getData()?has_content && ufficioRelazioniConIlPubblico2.pagina21.getData()?length gt 0 && ufficioRelazioniConIlPubblico2.pagina21.url21.getData()?has_content && ufficioRelazioniConIlPubblico2.pagina21.url21.getData()?length gt 0>
					<li>
						<a class="link" href="${ufficioRelazioniConIlPubblico2.pagina21.url21.getData()}">${ufficioRelazioniConIlPubblico2.pagina21.getData()}</a>
					</li>
				</#if>
				<#if ufficioRelazioniConIlPubblico2.pagina22.getData()?has_content && ufficioRelazioniConIlPubblico2.pagina22.getData()?length gt 0 && ufficioRelazioniConIlPubblico2.pagina22.url22.getData()?has_content && ufficioRelazioniConIlPubblico2.pagina22.url22.getData()?length gt 0>
					<li>
						<a class="link" href="${ufficioRelazioniConIlPubblico2.pagina22.url22.getData()}">${ufficioRelazioniConIlPubblico2.pagina22.getData()}</a>
					</li>
				</#if>
				<#if ufficioRelazioniConIlPubblico2.pagina23.getData()?has_content && ufficioRelazioniConIlPubblico2.pagina23.getData()?length gt 0 && ufficioRelazioniConIlPubblico2.pagina23.url23.getData()?has_content && ufficioRelazioniConIlPubblico2.pagina23.url23.getData()?length gt 0>
					<li>
						<a class="link" href="${ufficioRelazioniConIlPubblico2.pagina23.url23.getData()}">${ufficioRelazioniConIlPubblico2.pagina23.getData()}</a>
					</li>
				</#if>
			</ul>
		</#if>

		<#if pubblicitaLegale.getData()?has_content && pubblicitaLegale.getData()?length gt 0>
			<h3>${pubblicitaLegale.getData()}</h3>

			<#if pubblicitaLegale.titolo2.getData()?has_content && pubblicitaLegale.titolo2.getData()?length gt 0 && pubblicitaLegale.titolo2.url2.getData()?has_content && pubblicitaLegale.titolo2.url2.getData()?length gt 0>
				<ul>
					<li>
						<a class="link" href="${pubblicitaLegale.titolo2.url2.getData()}">${pubblicitaLegale.titolo2.getData()}</a>
					</li>
				</ul>
			</#if>
		</#if>
	</div>
</#if>

<#if (ufficioRelazioniConIlPubblico3.getData()?has_content && ufficioRelazioniConIlPubblico3.getData()?length gt 0) || (seguiciSu.getData()?has_content && seguiciSu.getData()?length gt 0)>
	<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-4">
		<#if ufficioRelazioniConIlPubblico3.getData()?has_content && ufficioRelazioniConIlPubblico3.getData()?length gt 0>
			<h3>${ufficioRelazioniConIlPubblico3.getData()}</h3>
			<ul>
				<#if ufficioRelazioniConIlPubblico3.pagina31.getData()?has_content && ufficioRelazioniConIlPubblico3.pagina31.getData()?length gt 0 && ufficioRelazioniConIlPubblico3.pagina31.url31.getData()?has_content && ufficioRelazioniConIlPubblico3.pagina31.url31.getData()?length gt 0>
					<li>
						<a class="link" href="${ufficioRelazioniConIlPubblico3.pagina31.url31.getData()}">${ufficioRelazioniConIlPubblico3.pagina31.getData()}</a>
					</li>
				</#if>
				<#if ufficioRelazioniConIlPubblico3.pagina32.getData()?has_content && ufficioRelazioniConIlPubblico3.pagina32.getData()?length gt 0 && ufficioRelazioniConIlPubblico3.pagina32.url32.getData()?has_content && ufficioRelazioniConIlPubblico3.pagina32.url32.getData()?length gt 0>
					<li>
						<a class="link" href="${ufficioRelazioniConIlPubblico3.pagina32.url32.getData()}">${ufficioRelazioniConIlPubblico3.pagina32.getData()}</a>
					</li>
				</#if>
				<li>
					<a class="link" href="">TAR</a>
				</li>
			</ul>
		</#if>
		<#if seguiciSu.getData()?has_content && seguiciSu.getData()?length gt 0>
			<h3>${seguiciSu.getData()}</h3>
			<#if seguiciSu.urlYouTube.getData()?has_content && seguiciSu.urlYouTube.getData()?length gt 0>
				<a href="${seguiciSu.urlYouTube.getData()}" target="_blank">
					<img class="youtube" src="/documents/20142/39443/youtube.png">
					<span class="sr-only">You Tube</span>
				</a>
			</#if>
			<#if seguiciSu.urlTwitter.getData()?has_content && seguiciSu.urlTwitter.getData()?length gt 0>
				<a href="${seguiciSu.urlTwitter.getData()}" target="_blank">
					<img src="/documents/20142/39443/twitter.png">
					<span class="sr-only">Twitter</span>
				</a>
			</#if>
		</#if>
	</div>
</#if>

<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<ul class="end-menu">
		<#if mappaDelSito.getData()?has_content && mappaDelSito.getData()?length gt 0 && mappaDelSito.urlMappa.getData()?has_content && mappaDelSito.urlMappa.getData()?length gt 0>
			<li>
				<a class="link" href="${mappaDelSito.urlMappa.getData()}">${mappaDelSito.getData()}</a>
			</li>
		</#if>

		<#if guidaAlSito.getData()?has_content && guidaAlSito.getData()?length gt 0 && guidaAlSito.urlGuida.getData()?has_content && guidaAlSito.urlGuida.getData()?length gt 0>
			<li>
				<a class="link" href="${guidaAlSito.urlGuida.getData()}">${guidaAlSito.getData()}</a>
			</li>
		</#if>

		<#if accessibilita.getData()?has_content && accessibilita.getData()?length gt 0 && accessibilita.urlAccessibilita.getData()?has_content && accessibilita.urlAccessibilita.getData()?length gt 0>
			<li>
				<a class="link" href="${accessibilita.urlAccessibilita.getData()}">${accessibilita.getData()}</a>
			</li>
		</#if>

		<#if condizioniDiUtilizzo.getData()?has_content && condizioniDiUtilizzo.getData()?length gt 0 && condizioniDiUtilizzo.urlCondizioni.getData()?has_content && condizioniDiUtilizzo.urlCondizioni.getData()?length gt 0>
			<li>
				<a class="link" href="${condizioniDiUtilizzo.urlCondizioni.getData()}">condizioni di utilizzo</a>
			</li>
		</#if>

		<#if privacy.getData()?has_content && privacy.getData()?length gt 0 && privacy.urlPrivacy.getData()?has_content && privacy.urlPrivacy.getData()?length gt 0>
			<li>
				<a class="link" href="${privacy.urlPrivacy.getData()}">${privacy.getData()}</a>
			</li>
		</#if>

		<#if regoleDiAccesso.getData()?has_content && regoleDiAccesso.getData()?length gt 0 && regoleDiAccesso.urlRegole.getData()?has_content && regoleDiAccesso.urlRegole.getData()?length gt 0>
			<li>
				<a class="link" href="${regoleDiAccesso.urlRegole.getData()}">${regoleDiAccesso.getData()}</a>
			</li>
		</#if>

		<#if webmail.getData()?has_content && webmail.getData()?length gt 0 && webmail.urlWebmail.getData()?has_content && webmail.urlWebmail.getData()?length gt 0>
			<li>
				<a class="link" href="">${webmail.getData()}</a>
			</li>
		</#if>
	</ul>
</div>