<#-- Allegati esterni -->

<#if labelLink.getSiblings()?has_content>
	<div class="external-attachments">
		<h4><i class="fas fa-download"></i>&nbsp;&nbsp;${.vars['reserved-article-title'].data} </h4>
		<ul class="list">
			<#list labelLink.getSiblings() as cur_label>
				<#assign doc = cur_label.urlLink.getData() />
				<#if doc?has_content && doc?length gt 0>
					<#assign tipoFile = cur_label.tipoFile.getData() />
					<li>
						<#if tipoFile == "doc">
							<i class="far fa-file-word"></i>
						<#elseif tipoFile == "pdf">
							<i class="far fa-file-pdf"></i>
						<#elseif tipoFile == "excel">
							<i class="far fa-file-excel"></i>
						<#elseif tipoFile == "xml">
							<i class="far fa-file-code"></i>
						<#else>
							<i class="far fa-file-alt"></i>
						</#if>

						<#assign titolo = '' />
						<#if cur_label.getData()?? &&  cur_label.getData()!='' && cur_label.getData()?length gt 0>
							<#assign titolo = cur_label.getData() />
						<#else>
							<#assign titolo = doc />
						</#if>

						<a download href="${doc}">&nbsp;&nbsp;${titolo}</a>
					</li>
				</#if>
			</#list>
		</ul>
	</div>
</#if>