<#assign
	journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")
	footerTitle = getterUtil.getString(theme_settings["footer-title"])
	groupid = themeDisplay.getScopeGroupId()
	footerArticle = ""
/>
​
<#if footerTitle?? && footerTitle?has_content>
    <#attempt>
        <#assign article = journalArticleLocalService.getArticleByUrlTitle(groupid, footerTitle)>
        <#assign footerArticle = journalArticleLocalService.getArticleContent(article, article.getDDMTemplateKey(), "VIEW", locale, themeDisplay)>
    <#recover>
        <#assign footerArticle = "">
    </#attempt>
</#if>