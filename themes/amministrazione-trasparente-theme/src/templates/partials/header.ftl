<header id="header-custom">
    <div class="container">
        <!--img src="img/png/logo.png"-->
        <a href="/">
            <#assign imagesPath = themeDisplay.getPathThemeImages() />
            <img class="desktop img-responsive" src="${imagesPath}/logo.png" />
            <img class="scroll img-responsive" src="${imagesPath}/logo_scroller.png" />
        </a>
        <#--<div class="dropdown text-right">-->
            <#--<button class="btn btn-default dropdown-toggle " type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">-->
                <#--Ita<i class="fas fa-angle-down"></i>-->
            <#--</button>-->
            <#--<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">-->
                <#--<li><a href="#">Action</a></li>-->
                <#--<li><a href="#">Another action</a></li>-->
                <#--<li><a href="#">Something else here</a></li>-->
                <#--<li role="separator" class="divider"></li>-->
                <#--<li><a href="#">Separated link</a></li>-->
            <#--</ul>-->
        <#--</div>-->
        <!-- div class="input-group search-field__input-group" role="search"> 
        <label for="searchField" class="sr-only"> Cerca... </label> 
            <input id="searchField" name="searchGenericaTheme" type="text" class="form-control search-field__input" placeholder="Cerca...">
            <span class="input-group-btn">
                <button type="submit" class="search-field__btn btn"><span class="fa fa-search"></span></button>
            </span>
        </div -->
        <#--<div class="input-search-bar">-->
            <#--<@liferay.search default_preferences=-->
            <#--freeMarkerPortletPreferences.getPreferences(-->
                <#--"portletSetupPortletDecoratorId", "barebone"-->
            <#--)-->
            <#--/>-->
        <#--</div>-->
		<div class="flag">
		</div>
	</div>
</header>