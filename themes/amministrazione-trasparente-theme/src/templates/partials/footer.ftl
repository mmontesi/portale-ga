<#--${footerArticle}-->
<!-- footer -->
<a href="#" class="pre-footer">
	<span><i class="fas fa-check"></i>Valuta questo sito</span>
</a>
<footer id="footer">
	<div class="container">
		<div class="row footer-template">
			<div class="col-12 col-xs-12 col-sm-12 col-sm-12 logo">
				<img class="img-responsive" src="${imagesPath}/logo_footer.png">
			</div>
			<@liferay_portlet["runtime"]
			instanceId="VTVLLDauQ5r0"
			portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet"/>
		</div>
	</div>
</footer>
<#--<footer id="footer" role="contentinfo">-->
	<#--<@liferay_portlet["runtime"]-->
	<#--instanceId="VTVLLDauQ5r0"-->
	<#--portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet"-->
	<#--defaultPreferences=freeMarkerPortletPreferences.getPreferences(footerLinkJCPortletPreferencesMap)/>-->

<#--</footer>-->